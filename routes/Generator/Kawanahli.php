<?php
/**
 * Kawan Ahli
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Kawanahli'], function () {
        Route::resource('kawanahlis', 'KawanahlisController');
        //For Datatable
        Route::post('kawanahlis/get', 'KawanahlisTableController')->name('kawanahlis.get');
    });
    
});