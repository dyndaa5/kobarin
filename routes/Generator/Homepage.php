<?php
/**
 * Homepage
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Homepage'], function () {
        Route::resource('homepages', 'HomepagesController');
        //For Datatable
        Route::post('homepages/get', 'HomepagesTableController')->name('homepages.get');
    });
    
});