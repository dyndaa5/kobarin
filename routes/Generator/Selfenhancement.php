<?php
/**
 * Selfenhancement
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Selfenhancement'], function () {
        Route::resource('selfenhancements', 'SelfenhancementsController');
        //For Datatable
        Route::post('selfenhancements/get', 'SelfenhancementsTableController')->name('selfenhancements.get');
    });
    
});