<?php
/**
 * Newsletter
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Newsletter'], function () {
        Route::resource('newsletters', 'NewslettersController');
        //For Datatable
        Route::post('newsletters/get', 'NewslettersTableController')->name('newsletters.get');
    });
    
});