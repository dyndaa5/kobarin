<?php
/**
 * Agenda
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Agenda'], function () {
        Route::resource('agendas', 'AgendasController');
        //For Datatable
        Route::post('agendas/get', 'AgendasTableController')->name('agendas.get');
    });
    
});