<?php
/**
 * Selfdevelopment
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Selfdevelopment'], function () {
        Route::resource('selfdevelopments', 'SelfdevelopmentsController');
        //For Datatable
        Route::post('selfdevelopments/get', 'SelfdevelopmentsTableController')->name('selfdevelopments.get');
    });
    
});