<?php
/**
 * Networks
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Network'], function () {
        Route::resource('networks', 'NetworksController');
        //For Datatable
        Route::post('networks/get', 'NetworksTableController')->name('networks.get');
    });
    
});