<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'FrontendController@index')->name('index');

// Self development
Route::get('/self-development', 'SDController@index')->name('sd.index');
Route::get('/self-development/{slug}', 'SDController@show')->name('sd.show');

// Self Enhancement
Route::get('/self-enhancement', 'SEController@index')->name('se.index');
Route::get('/self-enhancement/{slug}', 'SEController@show')->name('se.show');

// Daftar Ahli
Route::get('/daftar-ahli', 'AuthorController@index')->name('author.index');
Route::get('/author/{slug}', 'AuthorController@show')->name('author.show');

// Agenda
Route::get('/agenda', 'AgendaController@index')->name('author.index');
Route::get('/agenda/{slug}', 'AgendaController@show')->name('author.show');

Route::get('/videos', 'VideoController@index')->name('videos.index');
Route::get('/videos/{slug}', 'VideoController@show')->name('videos.show');

// Tentang Kami
Route::get('/tentang-kami', 'AboutController@index')->name('about.index');
Route::get('/tentang-kami/sejarah-kobarin', 'AboutController@sejarah')->name('about.sejarah');
Route::get('/tentang-kami/panduan-logo', 'AboutController@panduan')->name('about.panduan');

Route::get('/search', 'SearchController@index')->name('search.index');

// Route::get('{slug}', 'FrontendController@showPage')->name('pages.show');

Route::post('/get/states', 'FrontendController@getStates')->name('get.states');
Route::post('/get/cities', 'FrontendController@getCities')->name('get.cities');


/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User SD Posts
         */
        Route::get('dashboard/sd', 'SDController@index')->name('sd');
        Route::get('dashboard/sd/get', 'SDController@datatable')->name('sd.get');
        /*
         * User SE Posts
         */
        Route::get('dashboard/se', 'SEController@index')->name('se');

        /*
         * User Agenda Posts
         */
        Route::get('dashboard/agenda', 'AgendaController@index')->name('agenda');

        /*
         * User Video Posts
         */
        Route::get('dashboard/video', 'VideoController@index')->name('video');


        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');

        /*
         * User Profile Picture
         */
        Route::patch('profile-picture/update', 'ProfileController@updateProfilePicture')->name('profile-picture.update');
    });
});

/*
* Show pages
*/
// Route::get('pages/{slug}', 'FrontendController@showPage')->name('pages.show');
