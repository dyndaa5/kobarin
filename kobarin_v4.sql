-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 10, 2018 at 04:35 PM
-- Server version: 10.3.10-MariaDB-log
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kobarin_v4`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_datetime` datetime NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Published','Draft','InActive','Scheduled') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`id`, `name`, `publish_datetime`, `featured_image`, `content`, `meta_title`, `cannonical_link`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Agenda 1', '0000-00-00 00:00:00', '1544449127.png', '<p>test agenda</p>', NULL, NULL, 'http://127.0.0.1:8000/agenda/agenda-1', NULL, NULL, 'Published', 1, NULL, '2018-11-18 07:38:40', '2018-12-10 06:38:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_datetime` datetime NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Published','Draft','InActive','Scheduled') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `name`, `publish_datetime`, `featured_image`, `meta_title`, `cannonical_link`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'self-development', '0000-00-00 00:00:00', '1542549830self-development.png', NULL, NULL, NULL, NULL, NULL, 'Published', 0, NULL, '2018-11-18 05:38:16', '2018-11-18 07:03:50', NULL),
(2, 'self-enhancement', '0000-00-00 00:00:00', '1542549861self-enhancement.png', NULL, NULL, NULL, NULL, NULL, 'Published', 0, NULL, '2018-11-18 05:45:36', '2018-11-18 07:04:21', NULL),
(3, 'daftar-ahli', '0000-00-00 00:00:00', '1542557260kawan-ahli.png', NULL, NULL, NULL, NULL, NULL, 'Published', 0, NULL, '2018-11-18 05:46:17', '2018-11-18 09:07:40', NULL),
(4, 'agenda', '0000-00-00 00:00:00', '1542549897agenda.png', NULL, NULL, NULL, NULL, NULL, 'Published', 0, NULL, '2018-11-18 05:46:33', '2018-11-18 07:04:57', NULL),
(5, 'video', '2018-11-20 00:00:00', '1542646078.jpeg', NULL, NULL, NULL, NULL, NULL, 'Published', 0, NULL, '2018-11-18 05:46:33', '2018-11-18 07:04:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_datetime` datetime NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Published','Draft','InActive','Scheduled') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_map_categories`
--

CREATE TABLE `blog_map_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_map_tags`
--

CREATE TABLE `blog_map_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE `blog_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_datetime` datetime NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Published','Draft','InActive','Scheduled') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `publish_datetime`, `featured_image`, `content`, `meta_title`, `cannonical_link`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 'Ahli', '2018-11-18 17:43:00', NULL, NULL, NULL, NULL, 'ahli', NULL, NULL, 'Published', 0, NULL, '2018-11-18 03:44:03', '2018-11-18 03:44:03', NULL),
(5, 'Publik', '2018-11-18 17:44:00', NULL, NULL, NULL, NULL, 'publik', NULL, NULL, 'Published', 0, NULL, '2018-11-18 03:44:17', '2018-11-18 03:44:17', NULL),
(6, 'Ahli Edukasi', '2018-11-18 17:44:00', NULL, NULL, NULL, NULL, 'ahli-edukasi', NULL, NULL, 'Published', 0, NULL, '2018-11-18 03:44:53', '2018-11-18 03:44:53', NULL),
(7, 'Ahli Konseling', '2018-11-18 17:45:00', NULL, NULL, NULL, NULL, 'ahli-konseling', NULL, NULL, 'Published', 0, NULL, '2018-11-18 03:45:10', '2018-11-18 03:45:10', NULL),
(8, 'Internal', '2018-11-18 17:45:00', NULL, NULL, NULL, NULL, 'internal', NULL, NULL, 'Published', 0, NULL, '2018-11-18 03:46:04', '2018-11-18 03:46:04', NULL),
(9, 'Eksternal', '2018-11-18 17:46:00', NULL, NULL, NULL, NULL, 'eksternal', NULL, NULL, 'Published', 0, NULL, '2018-11-18 03:46:27', '2018-11-18 03:46:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `commentable_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentable_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commented_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commented_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT 1,
  `rate` double(15,8) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assets` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `type_id`, `user_id`, `entity_id`, `icon`, `class`, `text`, `assets`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 2, 'plus', 'bg-green', 'trans(\"history.backend.pages.created\") <strong>Tentang Kami</strong>', NULL, '2018-11-17 12:07:14', '2018-11-17 12:07:14'),
(2, 4, 1, 1, 'save', 'bg-aqua', 'trans(\"history.backend.pages.updated\") <strong>Syarat dan ketentuan</strong>', NULL, '2018-11-17 12:07:57', '2018-11-17 12:07:57'),
(3, 4, 1, 3, 'plus', 'bg-green', 'trans(\"history.backend.pages.created\") <strong>Privasi</strong>', NULL, '2018-11-17 12:08:12', '2018-11-17 12:08:12'),
(4, 4, 1, 4, 'plus', 'bg-green', 'trans(\"history.backend.pages.created\") <strong>Sejarah Kobarin</strong>', NULL, '2018-11-17 12:08:29', '2018-11-17 12:08:29'),
(5, 4, 1, 5, 'plus', 'bg-green', 'trans(\"history.backend.pages.created\") <strong>Panduan Logo</strong>', NULL, '2018-11-17 12:08:47', '2018-11-17 12:08:47'),
(6, 3, 1, 91, 'plus', 'bg-green', 'trans(\"history.backend.permissions.created\") <strong>post-artikel</strong>', NULL, '2018-11-17 13:27:58', '2018-11-17 13:27:58'),
(7, 2, 1, 3, 'save', 'bg-aqua', 'trans(\"history.backend.roles.updated\") <strong>User</strong>', NULL, '2018-11-17 13:28:26', '2018-11-17 13:28:26'),
(8, 2, 1, 4, 'plus', 'bg-green', 'trans(\"history.backend.roles.created\") <strong>Ahli Edukasi</strong>', NULL, '2018-11-18 05:24:19', '2018-11-18 05:24:19'),
(9, 2, 1, 5, 'plus', 'bg-green', 'trans(\"history.backend.roles.created\") <strong>Ahli Konseling</strong>', NULL, '2018-11-18 05:24:35', '2018-11-18 05:24:35'),
(10, 2, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.roles.updated\") <strong>Ahli Edukasi</strong>', NULL, '2018-11-18 05:24:49', '2018-11-18 05:24:49'),
(11, 2, 1, 5, 'save', 'bg-aqua', 'trans(\"history.backend.roles.updated\") <strong>Ahli Konseling</strong>', NULL, '2018-11-18 05:25:02', '2018-11-18 05:25:02'),
(12, 1, 1, 4, 'plus', 'bg-green', 'trans(\"history.backend.users.created\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-11-18 05:28:13', '2018-11-18 05:28:13'),
(13, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:34:38', '2018-12-02 06:34:38'),
(14, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:35:31', '2018-12-02 06:35:31'),
(15, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:37:05', '2018-12-02 06:37:05'),
(16, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:43:37', '2018-12-02 06:43:37'),
(17, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:43:54', '2018-12-02 06:43:54'),
(18, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:44:10', '2018-12-02 06:44:10'),
(19, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:45:04', '2018-12-02 06:45:04'),
(20, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:46:11', '2018-12-02 06:46:11'),
(21, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:46:21', '2018-12-02 06:46:21'),
(22, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:46:35', '2018-12-02 06:46:35'),
(23, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:46:50', '2018-12-02 06:46:50'),
(24, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:47:01', '2018-12-02 06:47:01'),
(25, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:47:09', '2018-12-02 06:47:09'),
(26, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:48:16', '2018-12-02 06:48:16'),
(27, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:48:58', '2018-12-02 06:48:58'),
(28, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:49:07', '2018-12-02 06:49:07'),
(29, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:49:21', '2018-12-02 06:49:21'),
(30, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:49:39', '2018-12-02 06:49:39'),
(31, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:50:02', '2018-12-02 06:50:02'),
(32, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:50:11', '2018-12-02 06:50:11'),
(33, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:50:29', '2018-12-02 06:50:29'),
(34, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:50:48', '2018-12-02 06:50:48'),
(35, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:52:12', '2018-12-02 06:52:12'),
(36, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:53:18', '2018-12-02 06:53:18'),
(37, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:53:33', '2018-12-02 06:53:33'),
(38, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:54:16', '2018-12-02 06:54:16'),
(39, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:56:25', '2018-12-02 06:56:25'),
(40, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:59:18', '2018-12-02 06:59:18'),
(41, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:59:32', '2018-12-02 06:59:32'),
(42, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 06:59:54', '2018-12-02 06:59:54'),
(43, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 07:00:57', '2018-12-02 07:00:57'),
(44, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 07:01:13', '2018-12-02 07:01:13'),
(45, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 07:01:57', '2018-12-02 07:01:57'),
(46, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 07:03:21', '2018-12-02 07:03:21'),
(47, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 07:04:03', '2018-12-02 07:04:03'),
(48, 4, 1, 1, 'save', 'bg-aqua', 'trans(\"history.backend.pages.updated\") <strong>Syarat dan ketentuan</strong>', NULL, '2018-12-02 08:50:09', '2018-12-02 08:50:09'),
(49, 4, 1, 1, 'save', 'bg-aqua', 'trans(\"history.backend.pages.updated\") <strong>Syarat dan ketentuan</strong>', NULL, '2018-12-02 08:50:35', '2018-12-02 08:50:35'),
(50, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 08:59:58', '2018-12-02 08:59:58'),
(51, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 09:01:36', '2018-12-02 09:01:36'),
(52, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 09:31:43', '2018-12-02 09:31:43'),
(53, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 09:32:45', '2018-12-02 09:32:45'),
(54, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 09:34:16', '2018-12-02 09:34:16'),
(55, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 09:34:59', '2018-12-02 09:34:59'),
(56, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 09:40:43', '2018-12-02 09:40:43'),
(57, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-02 09:41:59', '2018-12-02 09:41:59'),
(58, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-07 07:18:29', '2018-12-07 07:18:29'),
(59, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-10 01:11:20', '2018-12-10 01:11:20'),
(60, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-10 06:18:58', '2018-12-10 06:18:58'),
(61, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-10 06:20:06', '2018-12-10 06:20:06'),
(62, 1, 1, 12, 'plus', 'bg-green', 'trans(\"history.backend.users.created\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"hilmy syarif\",12]}', '2018-12-10 06:23:31', '2018-12-10 06:23:31'),
(63, 1, 1, 12, 'trash', 'bg-maroon', 'trans(\"history.backend.users.deleted\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"hilmy syarif\",12]}', '2018-12-10 06:24:12', '2018-12-10 06:24:12'),
(64, 1, 1, 13, 'plus', 'bg-green', 'trans(\"history.backend.users.created\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"hilmy syarif\",13]}', '2018-12-10 06:26:17', '2018-12-10 06:26:17'),
(65, 1, 1, 13, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"hilmy syarif\",13]}', '2018-12-10 06:27:07', '2018-12-10 06:27:07'),
(66, 1, 1, 13, 'trash', 'bg-maroon', 'trans(\"history.backend.users.deleted\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"hilmy syarif\",13]}', '2018-12-10 06:29:44', '2018-12-10 06:29:44'),
(67, 1, 1, 21, 'plus', 'bg-green', 'trans(\"history.backend.users.created\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"hilmy syarif\",21]}', '2018-12-10 06:36:45', '2018-12-10 06:36:45'),
(68, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-10 07:02:17', '2018-12-10 07:02:17'),
(69, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-10 07:07:40', '2018-12-10 07:07:40'),
(70, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Felix Lengkong, Ph.D.\",4]}', '2018-12-10 07:26:39', '2018-12-10 07:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `history_types`
--

CREATE TABLE `history_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_types`
--

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2018-11-17 10:54:43', '2018-11-17 10:54:43'),
(2, 'Role', '2018-11-17 10:54:43', '2018-11-17 10:54:43'),
(3, 'Permission', '2018-11-17 10:54:43', '2018-11-17 10:54:43'),
(4, 'Page', '2018-11-17 10:54:43', '2018-11-17 10:54:43'),
(5, 'BlogTag', '2018-11-17 10:54:43', '2018-11-17 10:54:43'),
(6, 'BlogCategory', '2018-11-17 10:54:43', '2018-11-17 10:54:43'),
(7, 'Blog', '2018-11-17 10:54:43', '2018-11-17 10:54:43');

-- --------------------------------------------------------

--
-- Table structure for table `kawanahlis`
--

CREATE TABLE `kawanahlis` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT 0,
  `institusi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `researchgate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lisensi_professional` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kawanahlis`
--

INSERT INTO `kawanahlis` (`id`, `user_id`, `user_type`, `institusi`, `researchgate`, `linkedin`, `twitter`, `facebook`, `website`, `youtube`, `lisensi_professional`, `about`, `featured_image`, `created_at`, `updated_at`) VALUES
(1, 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2018-11-18 01:46:12', '2018-11-18 01:46:12');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` enum('backend','frontend') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `items` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `type`, `name`, `items`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'backend', 'Backend Sidebar Menu', '[{\"view_permission_id\":\"view-selfdevelopment-permission\",\"icon\":\"fa-list\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.selfdevelopments.index\",\"name\":\"Self-Development\",\"id\":20,\"content\":\"Self-Development\"},{\"view_permission_id\":\"view-selfenhancement-permission\",\"icon\":\"fa-list\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.selfenhancements.index\",\"name\":\"Self-Enhancement\",\"id\":21,\"content\":\"Self-Enhancement\"},{\"view_permission_id\":\"view-agenda-permission\",\"icon\":\"fa-list\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.agendas.index\",\"name\":\"Agenda\",\"id\":23,\"content\":\"Agenda\"},{\"view_permission_id\":\"view-network-permission\",\"icon\":\"fa-share\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.networks.index\",\"name\":\"Networks\",\"id\":25,\"content\":\"Networks\"},{\"view_permission_id\":\"view-banner-permission\",\"icon\":\"fa-photo\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.banners.index\",\"name\":\"Banner\",\"id\":24,\"content\":\"Banner\"},{\"id\":26,\"name\":\"Categories\",\"url\":\"admin.categories.index\",\"url_type\":\"route\",\"open_in_new_tab\":0,\"icon\":\"fa-folder\",\"view_permission_id\":\"view-category-permission\",\"content\":\"Categories\"},{\"view_permission_id\":\"view-video-permission\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.videos.index\",\"name\":\"Videos\",\"id\":28,\"content\":\"Videos\"},{\"view_permission_id\":\"view-access-management\",\"icon\":\"fa-users\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"\",\"name\":\"Access Management\",\"id\":11,\"content\":\"Access Management\",\"children\":[{\"view_permission_id\":\"view-user-management\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.access.user.index\",\"name\":\"User Management\",\"id\":12,\"content\":\"User Management\"},{\"view_permission_id\":\"view-role-management\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.access.role.index\",\"name\":\"Role Management\",\"id\":13,\"content\":\"Role Management\"},{\"view_permission_id\":\"view-permission-management\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.access.permission.index\",\"name\":\"Permission Management\",\"id\":14,\"content\":\"Permission Management\"}]},{\"view_permission_id\":\"view-module\",\"icon\":\"fa-wrench\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.modules.index\",\"name\":\"Module\",\"id\":1,\"content\":\"Module\"},{\"view_permission_id\":\"view-menu\",\"icon\":\"fa-bars\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.menus.index\",\"name\":\"Menus\",\"id\":3,\"content\":\"Menus\"},{\"view_permission_id\":\"view-page\",\"icon\":\"fa-file-text\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.pages.index\",\"name\":\"Pages\",\"id\":2,\"content\":\"Pages\"},{\"view_permission_id\":\"edit-settings\",\"icon\":\"fa-gear\",\"open_in_new_tab\":0,\"url_type\":\"route\",\"url\":\"admin.settings.edit?id=1\",\"name\":\"Settings\",\"id\":9,\"content\":\"Settings\"}]', 1, NULL, '2018-11-17 10:54:43', '2018-11-19 08:57:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_11_02_060149_create_blog_categories_table', 1),
(2, '2017_11_02_060149_create_blog_map_categories_table', 1),
(3, '2017_11_02_060149_create_blog_map_tags_table', 1),
(4, '2017_11_02_060149_create_blog_tags_table', 1),
(5, '2017_11_02_060149_create_blogs_table', 1),
(6, '2017_11_02_060149_create_faqs_table', 1),
(7, '2017_11_02_060149_create_history_table', 1),
(8, '2017_11_02_060149_create_history_types_table', 1),
(9, '2017_11_02_060149_create_modules_table', 1),
(10, '2017_11_02_060149_create_notifications_table', 1),
(11, '2017_11_02_060149_create_pages_table', 1),
(12, '2017_11_02_060149_create_password_resets_table', 1),
(13, '2017_11_02_060149_create_permission_role_table', 1),
(14, '2017_11_02_060149_create_permission_user_table', 1),
(15, '2017_11_02_060149_create_permissions_table', 1),
(16, '2017_11_02_060149_create_role_user_table', 1),
(17, '2017_11_02_060149_create_roles_table', 1),
(18, '2017_11_02_060149_create_sessions_table', 1),
(19, '2017_11_02_060149_create_settings_table', 1),
(20, '2017_11_02_060149_create_social_logins_table', 1),
(21, '2017_11_02_060149_create_users_table', 1),
(22, '2017_11_02_060152_add_foreign_keys_to_history_table', 1),
(23, '2017_11_02_060152_add_foreign_keys_to_notifications_table', 1),
(24, '2017_11_02_060152_add_foreign_keys_to_permission_role_table', 1),
(25, '2017_11_02_060152_add_foreign_keys_to_permission_user_table', 1),
(26, '2017_11_02_060152_add_foreign_keys_to_role_user_table', 1),
(27, '2017_11_02_060152_add_foreign_keys_to_social_logins_table', 1),
(28, '2017_12_10_122555_create_menus_table', 1),
(29, '2017_12_24_042039_add_null_constraint_on_created_by_on_user_table', 1),
(30, '2017_12_28_005822_add_null_constraint_on_created_by_on_role_table', 1),
(31, '2017_12_28_010952_add_null_constraint_on_created_by_on_permission_table', 1),
(32, '2018_11_17_183205_create_selfdevelopments_table', 2),
(33, '2018_11_17_184835_create_selfenhancements_table', 2),
(34, '2018_11_17_185356_create_kawanahlis_table', 2),
(35, '2018_11_17_190249_create_agenda_table', 2),
(36, '2018_11_17_190928_create_banners_table', 2),
(37, '2018_11_17_192940_create_networks_table', 2),
(38, '2018_11_17_193357_create_categories_table', 2),
(39, '0000_00_00_000000_create_comments_table', 3),
(40, '2018_11_19_045202_create_newsletters_table', 4),
(41, '2018_11_19_152154_create_videos_table', 5),
(42, '2018_12_02_132604_add_photo_to_users', 6),
(44, '2018_12_02_155233_add_lisensi_professional_about', 7),
(45, '2018_12_10_073526_create_views_table', 8),
(46, '2018_12_10_075748_add_sosmed_to_user_table', 9),
(47, '2018_12_10_135329_add_institusi_and_jabatan_to_users_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `view_permission_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'view_route',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `view_permission_id`, `name`, `url`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'view-access-management', 'Access Management', NULL, 1, NULL, '2018-11-17 10:54:43', NULL),
(2, 'view-user-management', 'User Management', 'admin.access.user.index', 1, NULL, '2018-11-17 10:54:43', NULL),
(3, 'view-role-management', 'Role Management', 'admin.access.role.index', 1, NULL, '2018-11-17 10:54:43', NULL),
(4, 'view-permission-management', 'Permission Management', 'admin.access.permission.index', 1, NULL, '2018-11-17 10:54:43', NULL),
(5, 'view-menu', 'Menus', 'admin.menus.index', 1, NULL, '2018-11-17 10:54:43', NULL),
(6, 'view-module', 'Module', 'admin.modules.index', 1, NULL, '2018-11-17 10:54:43', NULL),
(7, 'view-page', 'Pages', 'admin.pages.index', 1, NULL, '2018-11-17 10:54:43', NULL),
(8, 'edit-settings', 'Settings', 'admin.settings.edit', 1, NULL, '2018-11-17 10:54:43', NULL),
(9, 'view-blog', 'Blog Management', NULL, 1, NULL, '2018-11-17 10:54:43', NULL),
(10, 'view-blog-category', 'Blog Category Management', 'admin.blogcategories.index', 1, NULL, '2018-11-17 10:54:43', NULL),
(11, 'view-blog-tag', 'Blog Tag Management', 'admin.blogtags.index', 1, NULL, '2018-11-17 10:54:43', NULL),
(12, 'view-blog', 'Blog Management', 'admin.blogs.index', 1, NULL, '2018-11-17 10:54:43', NULL),
(13, 'view-faq', 'Faq Management', 'admin.faqs.index', 1, NULL, '2018-11-17 10:54:43', NULL),
(15, 'view-selfdevelopment-permission', 'SelfDevelopment', 'admin.selfdevelopments.index', 1, NULL, '2018-11-17 11:32:13', '2018-11-17 11:32:13'),
(16, 'view-selfenhancement-permission', 'SelfEnhancement', 'admin.selfenhancements.index', 1, NULL, '2018-11-17 11:48:42', '2018-11-17 11:48:42'),
(18, 'view-agenda-permission', 'Agenda', 'admin.agendas.index', 1, NULL, '2018-11-17 12:02:56', '2018-11-17 12:02:56'),
(19, 'view-banner-permission', 'Banner', 'admin.banners.index', 1, NULL, '2018-11-17 12:09:36', '2018-11-17 12:09:36'),
(20, 'view-network-permission', 'Networks', 'admin.networks.index', 1, NULL, '2018-11-17 12:29:48', '2018-11-17 12:29:48'),
(21, 'view-category-permission', 'Categories', 'admin.categories.index', 1, NULL, '2018-11-17 12:34:05', '2018-11-17 12:34:05'),
(22, 'view-newsletter-permission', 'Newsletter', 'admin.newsletters.index', 1, NULL, '2018-11-18 21:52:10', '2018-11-18 21:52:10'),
(23, 'view-video-permission', 'Videos', 'admin.videos.index', 1, NULL, '2018-11-19 08:22:03', '2018-11-19 08:22:03');

-- --------------------------------------------------------

--
-- Table structure for table `networks`
--

CREATE TABLE `networks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_datetime` datetime NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Published','Draft','InActive','Scheduled') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 - Dashboard , 2 - Email , 3 - Both',
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `message`, `user_id`, `type`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 'User Logged In: Viral', 1, 1, 1, '2018-11-17 10:55:12', '2018-11-17 11:48:07'),
(2, 'User Logged In: System', 1, 1, 1, '2018-11-17 11:31:08', '2018-11-17 11:48:07'),
(3, 'User Logged In: System', 1, 1, 1, '2018-11-17 12:29:16', '2018-11-18 01:54:30'),
(4, 'User Logged In: System', 1, 1, 1, '2018-11-17 13:26:43', '2018-11-18 01:01:05'),
(5, 'User Logged In: System', 1, 1, 1, '2018-11-17 13:39:48', '2018-11-18 01:54:34'),
(6, 'User Logged In: System', 1, 1, 1, '2018-11-17 19:45:27', '2018-11-18 01:54:34'),
(7, 'User Logged In: System', 1, 1, 1, '2018-11-17 20:49:25', '2018-11-18 01:54:34'),
(8, 'User Logged In: System', 1, 1, 1, '2018-11-18 00:54:05', '2018-11-18 01:54:34'),
(9, 'User Logged In: System', 1, 1, 1, '2018-11-18 01:46:08', '2018-11-18 01:54:34'),
(10, 'User Logged In: System', 1, 1, 0, '2018-11-18 03:21:30', NULL),
(11, 'User Logged In: System', 1, 1, 0, '2018-11-18 04:21:23', NULL),
(12, 'User Logged In: System', 1, 1, 1, '2018-11-18 06:21:39', '2018-11-19 06:56:41'),
(13, 'User Logged In: System', 1, 1, 1, '2018-11-18 06:49:42', '2018-11-19 06:56:41'),
(14, 'User Logged In: System', 1, 1, 1, '2018-11-18 07:25:08', '2018-11-19 06:56:41'),
(15, 'User Logged In: System', 1, 1, 1, '2018-11-18 08:01:50', '2018-11-19 06:56:41'),
(16, 'User Logged In: System', 1, 1, 1, '2018-11-18 09:07:16', '2018-11-19 05:07:09'),
(17, 'User Logged In: System', 1, 1, 1, '2018-11-18 09:23:53', '2018-11-19 05:07:09'),
(18, 'User Logged In: System', 1, 1, 1, '2018-11-18 11:18:07', '2018-11-19 05:07:09'),
(19, 'User Logged In: System', 1, 1, 1, '2018-11-18 21:51:14', '2018-11-19 05:07:09'),
(20, 'User Logged In: System', 1, 1, 1, '2018-11-19 05:07:00', '2018-11-19 05:07:09'),
(21, 'User Logged In: System', 1, 1, 1, '2018-11-19 06:41:55', '2018-11-19 06:56:41'),
(22, 'User Logged In: System', 1, 1, 0, '2018-11-19 08:21:21', NULL),
(23, 'User Logged In: System', 1, 1, 0, '2018-11-19 08:57:22', NULL),
(24, 'User Logged In: System', 1, 1, 0, '2018-11-19 09:30:30', NULL),
(25, 'User Logged In: System', 1, 1, 0, '2018-12-01 17:48:52', NULL),
(26, 'User Logged In: System', 1, 1, 0, '2018-12-01 20:21:40', NULL),
(27, 'User Logged In: System', 1, 1, 0, '2018-12-02 03:23:33', NULL),
(28, 'User Logged In: System', 1, 1, 0, '2018-12-02 06:28:42', NULL),
(29, 'User Logged In: System', 1, 1, 0, '2018-12-02 08:21:02', NULL),
(30, 'User Logged In: System', 1, 1, 0, '2018-12-02 08:35:02', NULL),
(31, 'User Logged In: System', 1, 1, 0, '2018-12-02 11:30:25', NULL),
(32, 'User Logged In: System', 1, 1, 0, '2018-12-02 11:32:05', NULL),
(33, 'User Logged In: System', 1, 1, 0, '2018-12-03 03:55:08', NULL),
(34, 'User Logged In: System', 1, 1, 0, '2018-12-04 06:52:33', NULL),
(35, 'User Logged In: System', 1, 1, 0, '2018-12-04 07:46:25', NULL),
(36, 'User Logged In: System', 1, 1, 0, '2018-12-04 07:46:33', NULL),
(37, 'User Logged In: System', 1, 1, 0, '2018-12-04 07:55:00', NULL),
(38, 'User Logged In: System', 1, 1, 0, '2018-12-04 08:27:52', NULL),
(39, 'User Logged In: System', 1, 1, 0, '2018-12-04 08:48:03', NULL),
(40, 'User Logged In: asdf', 1, 1, 0, '2018-12-04 08:57:47', NULL),
(41, 'User Logged In: asdf', 1, 1, 0, '2018-12-04 09:00:14', NULL),
(42, 'User Logged In: asdf', 1, 1, 0, '2018-12-04 09:01:34', NULL),
(43, 'User Logged In: System', 1, 1, 0, '2018-12-04 09:09:57', NULL),
(44, 'User Logged In: System', 1, 1, 0, '2018-12-07 07:01:45', NULL),
(45, 'User Logged In: System', 1, 1, 0, '2018-12-07 07:18:00', NULL),
(46, 'User Logged In: System', 1, 1, 0, '2018-12-07 07:18:04', NULL),
(47, 'User Logged In: System', 1, 1, 0, '2018-12-07 07:33:09', NULL),
(48, 'User Logged In: test', 1, 1, 1, '2018-12-07 08:42:33', '2018-12-10 05:43:50'),
(49, 'User Logged In: System', 1, 1, 1, '2018-12-07 09:03:23', '2018-12-10 05:43:50'),
(50, 'User Logged In: System', 1, 1, 1, '2018-12-10 00:56:06', '2018-12-10 05:43:50'),
(51, 'User Logged In: System', 1, 1, 1, '2018-12-10 01:10:07', '2018-12-10 05:43:50'),
(52, 'User Logged In: System', 1, 1, 1, '2018-12-10 05:43:22', '2018-12-10 05:43:50'),
(53, 'User Logged In: System', 1, 1, 0, '2018-12-10 05:55:44', NULL),
(54, 'User Logged In: System', 1, 1, 0, '2018-12-10 06:17:13', NULL),
(55, 'User Logged In: System', 1, 1, 0, '2018-12-10 07:26:05', NULL),
(56, 'User Logged In: hilmy', 1, 1, 0, '2018-12-10 08:00:55', NULL),
(57, 'User Logged In: hilmy', 1, 1, 0, '2018-12-10 08:19:26', NULL),
(58, 'User Logged In: hilmy', 1, 1, 0, '2018-12-10 09:20:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `page_slug`, `description`, `cannonical_link`, `seo_title`, `seo_keyword`, `seo_description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Syarat dan ketentuan', 'syarat-dan-ketentuan', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Mi bibendum neque egestas congue quisque egestas diam in. Venenatis cras sed felis eget velit aliquet sagittis. Purus in massa tempor nec. Elit at imperdiet dui accumsan sit amet. Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat. Turpis egestas integer eget aliquet nibh. Facilisis leo vel fringilla est ullamcorper eget. A lacus vestibulum sed arcu non odio euismod. Ut tortor pretium viverra suspendisse. In hendrerit gravida rutrum quisque non. Consectetur lorem donec massa sapien faucibus et molestie ac feugiat. Turpis massa sed elementum tempus egestas sed sed risus pretium. Porttitor leo a diam sollicitudin tempor id eu nisl. Purus faucibus ornare suspendisse sed. Imperdiet proin fermentum leo vel orci porta non pulvinar neque. Risus at ultrices mi tempus. Aenean et tortor at risus viverra adipiscing at. Faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis.</p>\r\n<p>Viverra mauris in aliquam sem fringilla ut morbi tincidunt augue. Ornare arcu odio ut sem nulla. Risus at ultrices mi tempus imperdiet nulla malesuada pellentesque. Ultrices in iaculis nunc sed augue lacus viverra. Sit amet luctus venenatis lectus magna fringilla urna porttitor. Ultrices eros in cursus turpis massa tincidunt dui ut. Dui sapien eget mi proin sed. Mauris rhoncus aenean vel elit scelerisque mauris. Commodo ullamcorper a lacus vestibulum. Vel risus commodo viverra maecenas accumsan. Commodo viverra maecenas accumsan lacus.</p>\r\n<p>Orci porta non pulvinar neque laoreet. Vitae tempus quam pellentesque nec nam aliquam sem et tortor. At varius vel pharetra vel turpis. Etiam erat velit scelerisque in dictum. Nec ullamcorper sit amet risus nullam eget. Sit amet risus nullam eget. Iaculis nunc sed augue lacus viverra. Tortor consequat id porta nibh venenatis cras sed. Massa enim nec dui nunc mattis. Libero id faucibus nisl tincidunt eget nullam non nisi est. Aenean sed adipiscing diam donec adipiscing. Iaculis nunc sed augue lacus viverra vitae congue eu.</p>\r\n<p>Viverra orci sagittis eu volutpat odio facilisis. Gravida arcu ac tortor dignissim convallis aenean et tortor at. Velit euismod in pellentesque massa placerat duis ultricies lacus. Justo eget magna fermentum iaculis eu non. Mauris nunc congue nisi vitae suscipit tellus mauris a. Vestibulum sed arcu non odio euismod lacinia at. Consectetur libero id faucibus nisl tincidunt eget nullam non nisi. Dolor morbi non arcu risus quis varius quam quisque id. Sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Et ultrices neque ornare aenean euismod elementum nisi quis. Pretium fusce id velit ut tortor pretium viverra suspendisse. Ornare aenean euismod elementum nisi quis. Cras pulvinar mattis nunc sed. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque.</p>\r\n<p>Viverra nam libero justo laoreet sit. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Dui vivamus arcu felis bibendum ut. Platea dictumst quisque sagittis purus sit amet. Orci porta non pulvinar neque laoreet suspendisse interdum. Cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris. Eu scelerisque felis imperdiet proin fermentum leo. Etiam tempor orci eu lobortis. Eget duis at tellus at urna condimentum mattis pellentesque. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Enim nulla aliquet porttitor lacus luctus accumsan. Laoreet sit amet cursus sit amet dictum sit amet justo. Metus dictum at tempor commodo ullamcorper a lacus vestibulum sed. Nec feugiat in fermentum posuere urna nec tincidunt praesent. Suspendisse ultrices gravida dictum fusce ut placerat. Phasellus egestas tellus rutrum tellus pellentesque.</p>', NULL, NULL, NULL, NULL, 1, 1, 1, '2018-11-17 10:54:43', '2018-12-02 08:50:35', NULL),
(2, 'Tentang Kami', 'tentang-kami', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Mi bibendum neque egestas congue quisque egestas diam in. Venenatis cras sed felis eget velit aliquet sagittis. Purus in massa tempor nec. Elit at imperdiet dui accumsan sit amet. Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat. Turpis egestas integer eget aliquet nibh. Facilisis leo vel fringilla est ullamcorper eget. A lacus vestibulum sed arcu non odio euismod. Ut tortor pretium viverra suspendisse. In hendrerit gravida rutrum quisque non. Consectetur lorem donec massa sapien faucibus et molestie ac feugiat. Turpis massa sed elementum tempus egestas sed sed risus pretium. Porttitor leo a diam sollicitudin tempor id eu nisl. Purus faucibus ornare suspendisse sed. Imperdiet proin fermentum leo vel orci porta non pulvinar neque. Risus at ultrices mi tempus. Aenean et tortor at risus viverra adipiscing at. Faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis.</p>\r\n<p>Viverra mauris in aliquam sem fringilla ut morbi tincidunt augue. Ornare arcu odio ut sem nulla. Risus at ultrices mi tempus imperdiet nulla malesuada pellentesque. Ultrices in iaculis nunc sed augue lacus viverra. Sit amet luctus venenatis lectus magna fringilla urna porttitor. Ultrices eros in cursus turpis massa tincidunt dui ut. Dui sapien eget mi proin sed. Mauris rhoncus aenean vel elit scelerisque mauris. Commodo ullamcorper a lacus vestibulum. Vel risus commodo viverra maecenas accumsan. Commodo viverra maecenas accumsan lacus.</p>\r\n<p>Orci porta non pulvinar neque laoreet. Vitae tempus quam pellentesque nec nam aliquam sem et tortor. At varius vel pharetra vel turpis. Etiam erat velit scelerisque in dictum. Nec ullamcorper sit amet risus nullam eget. Sit amet risus nullam eget. Iaculis nunc sed augue lacus viverra. Tortor consequat id porta nibh venenatis cras sed. Massa enim nec dui nunc mattis. Libero id faucibus nisl tincidunt eget nullam non nisi est. Aenean sed adipiscing diam donec adipiscing. Iaculis nunc sed augue lacus viverra vitae congue eu.</p>\r\n<p>Viverra orci sagittis eu volutpat odio facilisis. Gravida arcu ac tortor dignissim convallis aenean et tortor at. Velit euismod in pellentesque massa placerat duis ultricies lacus. Justo eget magna fermentum iaculis eu non. Mauris nunc congue nisi vitae suscipit tellus mauris a. Vestibulum sed arcu non odio euismod lacinia at. Consectetur libero id faucibus nisl tincidunt eget nullam non nisi. Dolor morbi non arcu risus quis varius quam quisque id. Sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Et ultrices neque ornare aenean euismod elementum nisi quis. Pretium fusce id velit ut tortor pretium viverra suspendisse. Ornare aenean euismod elementum nisi quis. Cras pulvinar mattis nunc sed. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque.</p>\r\n<p>Viverra nam libero justo laoreet sit. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Dui vivamus arcu felis bibendum ut. Platea dictumst quisque sagittis purus sit amet. Orci porta non pulvinar neque laoreet suspendisse interdum. Cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris. Eu scelerisque felis imperdiet proin fermentum leo. Etiam tempor orci eu lobortis. Eget duis at tellus at urna condimentum mattis pellentesque. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Enim nulla aliquet porttitor lacus luctus accumsan. Laoreet sit amet cursus sit amet dictum sit amet justo. Metus dictum at tempor commodo ullamcorper a lacus vestibulum sed. Nec feugiat in fermentum posuere urna nec tincidunt praesent. Suspendisse ultrices gravida dictum fusce ut placerat. Phasellus egestas tellus rutrum tellus pellentesque.</p>', NULL, NULL, NULL, NULL, 1, 1, NULL, '2018-11-17 12:07:14', '2018-11-17 12:07:14', NULL),
(3, 'Privasi', 'privasi', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Mi bibendum neque egestas congue quisque egestas diam in. Venenatis cras sed felis eget velit aliquet sagittis. Purus in massa tempor nec. Elit at imperdiet dui accumsan sit amet. Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat. Turpis egestas integer eget aliquet nibh. Facilisis leo vel fringilla est ullamcorper eget. A lacus vestibulum sed arcu non odio euismod. Ut tortor pretium viverra suspendisse. In hendrerit gravida rutrum quisque non. Consectetur lorem donec massa sapien faucibus et molestie ac feugiat. Turpis massa sed elementum tempus egestas sed sed risus pretium. Porttitor leo a diam sollicitudin tempor id eu nisl. Purus faucibus ornare suspendisse sed. Imperdiet proin fermentum leo vel orci porta non pulvinar neque. Risus at ultrices mi tempus. Aenean et tortor at risus viverra adipiscing at. Faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis.</p>\r\n<p>Viverra mauris in aliquam sem fringilla ut morbi tincidunt augue. Ornare arcu odio ut sem nulla. Risus at ultrices mi tempus imperdiet nulla malesuada pellentesque. Ultrices in iaculis nunc sed augue lacus viverra. Sit amet luctus venenatis lectus magna fringilla urna porttitor. Ultrices eros in cursus turpis massa tincidunt dui ut. Dui sapien eget mi proin sed. Mauris rhoncus aenean vel elit scelerisque mauris. Commodo ullamcorper a lacus vestibulum. Vel risus commodo viverra maecenas accumsan. Commodo viverra maecenas accumsan lacus.</p>\r\n<p>Orci porta non pulvinar neque laoreet. Vitae tempus quam pellentesque nec nam aliquam sem et tortor. At varius vel pharetra vel turpis. Etiam erat velit scelerisque in dictum. Nec ullamcorper sit amet risus nullam eget. Sit amet risus nullam eget. Iaculis nunc sed augue lacus viverra. Tortor consequat id porta nibh venenatis cras sed. Massa enim nec dui nunc mattis. Libero id faucibus nisl tincidunt eget nullam non nisi est. Aenean sed adipiscing diam donec adipiscing. Iaculis nunc sed augue lacus viverra vitae congue eu.</p>\r\n<p>Viverra orci sagittis eu volutpat odio facilisis. Gravida arcu ac tortor dignissim convallis aenean et tortor at. Velit euismod in pellentesque massa placerat duis ultricies lacus. Justo eget magna fermentum iaculis eu non. Mauris nunc congue nisi vitae suscipit tellus mauris a. Vestibulum sed arcu non odio euismod lacinia at. Consectetur libero id faucibus nisl tincidunt eget nullam non nisi. Dolor morbi non arcu risus quis varius quam quisque id. Sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Et ultrices neque ornare aenean euismod elementum nisi quis. Pretium fusce id velit ut tortor pretium viverra suspendisse. Ornare aenean euismod elementum nisi quis. Cras pulvinar mattis nunc sed. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque.</p>\r\n<p>Viverra nam libero justo laoreet sit. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Dui vivamus arcu felis bibendum ut. Platea dictumst quisque sagittis purus sit amet. Orci porta non pulvinar neque laoreet suspendisse interdum. Cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris. Eu scelerisque felis imperdiet proin fermentum leo. Etiam tempor orci eu lobortis. Eget duis at tellus at urna condimentum mattis pellentesque. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Enim nulla aliquet porttitor lacus luctus accumsan. Laoreet sit amet cursus sit amet dictum sit amet justo. Metus dictum at tempor commodo ullamcorper a lacus vestibulum sed. Nec feugiat in fermentum posuere urna nec tincidunt praesent. Suspendisse ultrices gravida dictum fusce ut placerat. Phasellus egestas tellus rutrum tellus pellentesque.</p>', NULL, NULL, NULL, NULL, 1, 1, NULL, '2018-11-17 12:08:12', '2018-11-17 12:08:12', NULL),
(4, 'Sejarah Kobarin', 'sejarah-kobarin', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Mi bibendum neque egestas congue quisque egestas diam in. Venenatis cras sed felis eget velit aliquet sagittis. Purus in massa tempor nec. Elit at imperdiet dui accumsan sit amet. Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat. Turpis egestas integer eget aliquet nibh. Facilisis leo vel fringilla est ullamcorper eget. A lacus vestibulum sed arcu non odio euismod. Ut tortor pretium viverra suspendisse. In hendrerit gravida rutrum quisque non. Consectetur lorem donec massa sapien faucibus et molestie ac feugiat. Turpis massa sed elementum tempus egestas sed sed risus pretium. Porttitor leo a diam sollicitudin tempor id eu nisl. Purus faucibus ornare suspendisse sed. Imperdiet proin fermentum leo vel orci porta non pulvinar neque. Risus at ultrices mi tempus. Aenean et tortor at risus viverra adipiscing at. Faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis.</p>\r\n<p>Viverra mauris in aliquam sem fringilla ut morbi tincidunt augue. Ornare arcu odio ut sem nulla. Risus at ultrices mi tempus imperdiet nulla malesuada pellentesque. Ultrices in iaculis nunc sed augue lacus viverra. Sit amet luctus venenatis lectus magna fringilla urna porttitor. Ultrices eros in cursus turpis massa tincidunt dui ut. Dui sapien eget mi proin sed. Mauris rhoncus aenean vel elit scelerisque mauris. Commodo ullamcorper a lacus vestibulum. Vel risus commodo viverra maecenas accumsan. Commodo viverra maecenas accumsan lacus.</p>\r\n<p>Orci porta non pulvinar neque laoreet. Vitae tempus quam pellentesque nec nam aliquam sem et tortor. At varius vel pharetra vel turpis. Etiam erat velit scelerisque in dictum. Nec ullamcorper sit amet risus nullam eget. Sit amet risus nullam eget. Iaculis nunc sed augue lacus viverra. Tortor consequat id porta nibh venenatis cras sed. Massa enim nec dui nunc mattis. Libero id faucibus nisl tincidunt eget nullam non nisi est. Aenean sed adipiscing diam donec adipiscing. Iaculis nunc sed augue lacus viverra vitae congue eu.</p>\r\n<p>Viverra orci sagittis eu volutpat odio facilisis. Gravida arcu ac tortor dignissim convallis aenean et tortor at. Velit euismod in pellentesque massa placerat duis ultricies lacus. Justo eget magna fermentum iaculis eu non. Mauris nunc congue nisi vitae suscipit tellus mauris a. Vestibulum sed arcu non odio euismod lacinia at. Consectetur libero id faucibus nisl tincidunt eget nullam non nisi. Dolor morbi non arcu risus quis varius quam quisque id. Sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Et ultrices neque ornare aenean euismod elementum nisi quis. Pretium fusce id velit ut tortor pretium viverra suspendisse. Ornare aenean euismod elementum nisi quis. Cras pulvinar mattis nunc sed. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque.</p>\r\n<p>Viverra nam libero justo laoreet sit. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Dui vivamus arcu felis bibendum ut. Platea dictumst quisque sagittis purus sit amet. Orci porta non pulvinar neque laoreet suspendisse interdum. Cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris. Eu scelerisque felis imperdiet proin fermentum leo. Etiam tempor orci eu lobortis. Eget duis at tellus at urna condimentum mattis pellentesque. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Enim nulla aliquet porttitor lacus luctus accumsan. Laoreet sit amet cursus sit amet dictum sit amet justo. Metus dictum at tempor commodo ullamcorper a lacus vestibulum sed. Nec feugiat in fermentum posuere urna nec tincidunt praesent. Suspendisse ultrices gravida dictum fusce ut placerat. Phasellus egestas tellus rutrum tellus pellentesque.</p>', NULL, NULL, NULL, NULL, 1, 1, NULL, '2018-11-17 12:08:29', '2018-11-17 12:08:29', NULL),
(5, 'Panduan Logo', 'panduan-logo', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Mi bibendum neque egestas congue quisque egestas diam in. Venenatis cras sed felis eget velit aliquet sagittis. Purus in massa tempor nec. Elit at imperdiet dui accumsan sit amet. Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat. Turpis egestas integer eget aliquet nibh. Facilisis leo vel fringilla est ullamcorper eget. A lacus vestibulum sed arcu non odio euismod. Ut tortor pretium viverra suspendisse. In hendrerit gravida rutrum quisque non. Consectetur lorem donec massa sapien faucibus et molestie ac feugiat. Turpis massa sed elementum tempus egestas sed sed risus pretium. Porttitor leo a diam sollicitudin tempor id eu nisl. Purus faucibus ornare suspendisse sed. Imperdiet proin fermentum leo vel orci porta non pulvinar neque. Risus at ultrices mi tempus. Aenean et tortor at risus viverra adipiscing at. Faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis.</p>\r\n<p>Viverra mauris in aliquam sem fringilla ut morbi tincidunt augue. Ornare arcu odio ut sem nulla. Risus at ultrices mi tempus imperdiet nulla malesuada pellentesque. Ultrices in iaculis nunc sed augue lacus viverra. Sit amet luctus venenatis lectus magna fringilla urna porttitor. Ultrices eros in cursus turpis massa tincidunt dui ut. Dui sapien eget mi proin sed. Mauris rhoncus aenean vel elit scelerisque mauris. Commodo ullamcorper a lacus vestibulum. Vel risus commodo viverra maecenas accumsan. Commodo viverra maecenas accumsan lacus.</p>\r\n<p>Orci porta non pulvinar neque laoreet. Vitae tempus quam pellentesque nec nam aliquam sem et tortor. At varius vel pharetra vel turpis. Etiam erat velit scelerisque in dictum. Nec ullamcorper sit amet risus nullam eget. Sit amet risus nullam eget. Iaculis nunc sed augue lacus viverra. Tortor consequat id porta nibh venenatis cras sed. Massa enim nec dui nunc mattis. Libero id faucibus nisl tincidunt eget nullam non nisi est. Aenean sed adipiscing diam donec adipiscing. Iaculis nunc sed augue lacus viverra vitae congue eu.</p>\r\n<p>Viverra orci sagittis eu volutpat odio facilisis. Gravida arcu ac tortor dignissim convallis aenean et tortor at. Velit euismod in pellentesque massa placerat duis ultricies lacus. Justo eget magna fermentum iaculis eu non. Mauris nunc congue nisi vitae suscipit tellus mauris a. Vestibulum sed arcu non odio euismod lacinia at. Consectetur libero id faucibus nisl tincidunt eget nullam non nisi. Dolor morbi non arcu risus quis varius quam quisque id. Sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur. Et ultrices neque ornare aenean euismod elementum nisi quis. Pretium fusce id velit ut tortor pretium viverra suspendisse. Ornare aenean euismod elementum nisi quis. Cras pulvinar mattis nunc sed. In nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque.</p>\r\n<p>Viverra nam libero justo laoreet sit. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Dui vivamus arcu felis bibendum ut. Platea dictumst quisque sagittis purus sit amet. Orci porta non pulvinar neque laoreet suspendisse interdum. Cursus vitae congue mauris rhoncus aenean vel elit scelerisque mauris. Eu scelerisque felis imperdiet proin fermentum leo. Etiam tempor orci eu lobortis. Eget duis at tellus at urna condimentum mattis pellentesque. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Enim nulla aliquet porttitor lacus luctus accumsan. Laoreet sit amet cursus sit amet dictum sit amet justo. Metus dictum at tempor commodo ullamcorper a lacus vestibulum sed. Nec feugiat in fermentum posuere urna nec tincidunt praesent. Suspendisse ultrices gravida dictum fusce ut placerat. Phasellus egestas tellus rutrum tellus pellentesque.</p>', NULL, NULL, NULL, NULL, 1, 1, NULL, '2018-11-17 12:08:47', '2018-11-17 12:08:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `sort`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'view-backend', 'View Backend', 1, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(2, 'view-frontend', 'View Frontend', 2, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(3, 'view-access-management', 'View Access Management', 3, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(4, 'view-user-management', 'View User Management', 4, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(5, 'view-active-user', 'View Active User', 5, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(6, 'view-deactive-user', 'View Deactive User', 6, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(7, 'view-deleted-user', 'View Deleted User', 7, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(8, 'show-user', 'Show User Details', 8, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(9, 'create-user', 'Create User', 9, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(10, 'edit-user', 'Edit User', 9, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(11, 'delete-user', 'Delete User', 10, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(12, 'activate-user', 'Activate User', 11, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(13, 'deactivate-user', 'Deactivate User', 12, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(14, 'login-as-user', 'Login As User', 13, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(15, 'clear-user-session', 'Clear User Session', 14, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(16, 'view-role-management', 'View Role Management', 15, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(17, 'create-role', 'Create Role', 16, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(18, 'edit-role', 'Edit Role', 17, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(19, 'delete-role', 'Delete Role', 18, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(20, 'view-permission-management', 'View Permission Management', 19, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(21, 'create-permission', 'Create Permission', 20, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(22, 'edit-permission', 'Edit Permission', 21, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(23, 'delete-permission', 'Delete Permission', 22, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(24, 'view-page', 'View Page', 23, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(25, 'create-page', 'Create Page', 24, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(26, 'edit-page', 'Edit Page', 25, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(27, 'delete-page', 'Delete Page', 26, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(28, 'view-email-template', 'View Email Templates', 27, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(29, 'create-email-template', 'Create Email Templates', 28, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(30, 'edit-email-template', 'Edit Email Templates', 29, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(31, 'delete-email-template', 'Delete Email Templates', 30, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(32, 'edit-settings', 'Edit Settings', 31, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(33, 'view-blog-category', 'View Blog Categories Management', 32, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(34, 'create-blog-category', 'Create Blog Category', 33, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(35, 'edit-blog-category', 'Edit Blog Category', 34, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(36, 'delete-blog-category', 'Delete Blog Category', 35, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(37, 'view-blog-tag', 'View Blog Tags Management', 36, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(38, 'create-blog-tag', 'Create Blog Tag', 37, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(39, 'edit-blog-tag', 'Edit Blog Tag', 38, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(40, 'delete-blog-tag', 'Delete Blog Tag', 39, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(41, 'view-blog', 'View Blogs Management', 40, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(42, 'create-blog', 'Create Blog', 41, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(43, 'edit-blog', 'Edit Blog', 42, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(44, 'delete-blog', 'Delete Blog', 43, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(45, 'view-faq', 'View FAQ Management', 44, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(46, 'create-faq', 'Create FAQ', 45, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(47, 'edit-faq', 'Edit FAQ', 46, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(48, 'delete-faq', 'Delete FAQ', 47, 1, 1, NULL, '2018-11-17 10:54:43', '2018-11-17 10:54:43', NULL),
(49, 'manage-selfdevelopment', 'Manage Selfdevelopment Permission', 0, 1, 1, NULL, '2018-11-17 11:02:42', '2018-11-17 11:02:42', NULL),
(50, 'create-selfdevelopment', 'Create Selfdevelopment Permission', 0, 1, 1, NULL, '2018-11-17 11:02:42', '2018-11-17 11:02:42', NULL),
(51, 'store-selfdevelopment', 'Store Selfdevelopment Permission', 0, 1, 1, NULL, '2018-11-17 11:02:42', '2018-11-17 11:02:42', NULL),
(52, 'edit-selfdevelopment', 'Edit Selfdevelopment Permission', 0, 1, 1, NULL, '2018-11-17 11:02:42', '2018-11-17 11:02:42', NULL),
(53, 'update-selfdevelopment', 'Update Selfdevelopment Permission', 0, 1, 1, NULL, '2018-11-17 11:02:42', '2018-11-17 11:02:42', NULL),
(54, 'delete-selfdevelopment', 'Delete Selfdevelopment Permission', 0, 1, 1, NULL, '2018-11-17 11:02:42', '2018-11-17 11:02:42', NULL),
(55, 'manage-selfenhancement', 'Manage Selfenhancement Permission', 0, 1, 1, NULL, '2018-11-17 11:48:42', '2018-11-17 11:48:42', NULL),
(56, 'create-selfenhancement', 'Create Selfenhancement Permission', 0, 1, 1, NULL, '2018-11-17 11:48:42', '2018-11-17 11:48:42', NULL),
(57, 'store-selfenhancement', 'Store Selfenhancement Permission', 0, 1, 1, NULL, '2018-11-17 11:48:42', '2018-11-17 11:48:42', NULL),
(58, 'edit-selfenhancement', 'Edit Selfenhancement Permission', 0, 1, 1, NULL, '2018-11-17 11:48:42', '2018-11-17 11:48:42', NULL),
(59, 'update-selfenhancement', 'Update Selfenhancement Permission', 0, 1, 1, NULL, '2018-11-17 11:48:42', '2018-11-17 11:48:42', NULL),
(60, 'delete-selfenhancement', 'Delete Selfenhancement Permission', 0, 1, 1, NULL, '2018-11-17 11:48:42', '2018-11-17 11:48:42', NULL),
(61, 'manage-kawanahli', 'Manage Kawanahli Permission', 0, 1, 1, NULL, '2018-11-17 11:54:03', '2018-11-17 11:54:03', NULL),
(62, 'create-kawanahli', 'Create Kawanahli Permission', 0, 1, 1, NULL, '2018-11-17 11:54:03', '2018-11-17 11:54:03', NULL),
(63, 'store-kawanahli', 'Store Kawanahli Permission', 0, 1, 1, NULL, '2018-11-17 11:54:03', '2018-11-17 11:54:03', NULL),
(64, 'edit-kawanahli', 'Edit Kawanahli Permission', 0, 1, 1, NULL, '2018-11-17 11:54:03', '2018-11-17 11:54:03', NULL),
(65, 'update-kawanahli', 'Update Kawanahli Permission', 0, 1, 1, NULL, '2018-11-17 11:54:03', '2018-11-17 11:54:03', NULL),
(66, 'delete-kawanahli', 'Delete Kawanahli Permission', 0, 1, 1, NULL, '2018-11-17 11:54:03', '2018-11-17 11:54:03', NULL),
(67, 'manage-agenda', 'Manage Agenda Permission', 0, 1, 1, NULL, '2018-11-17 12:02:56', '2018-11-17 12:02:56', NULL),
(68, 'create-agenda', 'Create Agenda Permission', 0, 1, 1, NULL, '2018-11-17 12:02:56', '2018-11-17 12:02:56', NULL),
(69, 'store-agenda', 'Store Agenda Permission', 0, 1, 1, NULL, '2018-11-17 12:02:56', '2018-11-17 12:02:56', NULL),
(70, 'edit-agenda', 'Edit Agenda Permission', 0, 1, 1, NULL, '2018-11-17 12:02:56', '2018-11-17 12:02:56', NULL),
(71, 'update-agenda', 'Update Agenda Permission', 0, 1, 1, NULL, '2018-11-17 12:02:56', '2018-11-17 12:02:56', NULL),
(72, 'delete-agenda', 'Delete Agenda Permission', 0, 1, 1, NULL, '2018-11-17 12:02:56', '2018-11-17 12:02:56', NULL),
(73, 'manage-banner', 'Manage Banner Permission', 0, 1, 1, NULL, '2018-11-17 12:09:36', '2018-11-17 12:09:36', NULL),
(74, 'create-banner', 'Create Banner Permission', 0, 1, 1, NULL, '2018-11-17 12:09:36', '2018-11-17 12:09:36', NULL),
(75, 'store-banner', 'Store Banner Permission', 0, 1, 1, NULL, '2018-11-17 12:09:36', '2018-11-17 12:09:36', NULL),
(76, 'edit-banner', 'Edit Banner Permission', 0, 1, 1, NULL, '2018-11-17 12:09:36', '2018-11-17 12:09:36', NULL),
(77, 'update-banner', 'Update Banner Permission', 0, 1, 1, NULL, '2018-11-17 12:09:36', '2018-11-17 12:09:36', NULL),
(78, 'delete-banner', 'Delete Banner Permission', 0, 1, 1, NULL, '2018-11-17 12:09:36', '2018-11-17 12:09:36', NULL),
(79, 'manage-network', 'Manage Network Permission', 0, 1, 1, NULL, '2018-11-17 12:29:48', '2018-11-17 12:29:48', NULL),
(80, 'create-network', 'Create Network Permission', 0, 1, 1, NULL, '2018-11-17 12:29:48', '2018-11-17 12:29:48', NULL),
(81, 'store-network', 'Store Network Permission', 0, 1, 1, NULL, '2018-11-17 12:29:48', '2018-11-17 12:29:48', NULL),
(82, 'edit-network', 'Edit Network Permission', 0, 1, 1, NULL, '2018-11-17 12:29:48', '2018-11-17 12:29:48', NULL),
(83, 'update-network', 'Update Network Permission', 0, 1, 1, NULL, '2018-11-17 12:29:48', '2018-11-17 12:29:48', NULL),
(84, 'delete-network', 'Delete Network Permission', 0, 1, 1, NULL, '2018-11-17 12:29:48', '2018-11-17 12:29:48', NULL),
(85, 'manage-category', 'Manage Category Permission', 0, 1, 1, NULL, '2018-11-17 12:34:05', '2018-11-17 12:34:05', NULL),
(86, 'create-category', 'Create Category Permission', 0, 1, 1, NULL, '2018-11-17 12:34:05', '2018-11-17 12:34:05', NULL),
(87, 'store-category', 'Store Category Permission', 0, 1, 1, NULL, '2018-11-17 12:34:05', '2018-11-17 12:34:05', NULL),
(88, 'edit-category', 'Edit Category Permission', 0, 1, 1, NULL, '2018-11-17 12:34:05', '2018-11-17 12:34:05', NULL),
(89, 'update-category', 'Update Category Permission', 0, 1, 1, NULL, '2018-11-17 12:34:05', '2018-11-17 12:34:05', NULL),
(90, 'delete-category', 'Delete Category Permission', 0, 1, 1, NULL, '2018-11-17 12:34:05', '2018-11-17 12:34:05', NULL),
(91, 'post-artikel', 'Posting Artikel (SD, SE, VIDEO)', 0, 1, 1, NULL, '2018-11-17 13:27:58', '2018-11-17 13:27:58', NULL),
(92, 'manage-newsletter', 'Manage Newsletter Permission', 0, 1, 1, NULL, '2018-11-18 21:52:10', '2018-11-18 21:52:10', NULL),
(93, 'create-newsletter', 'Create Newsletter Permission', 0, 1, 1, NULL, '2018-11-18 21:52:10', '2018-11-18 21:52:10', NULL),
(94, 'store-newsletter', 'Store Newsletter Permission', 0, 1, 1, NULL, '2018-11-18 21:52:10', '2018-11-18 21:52:10', NULL),
(95, 'edit-newsletter', 'Edit Newsletter Permission', 0, 1, 1, NULL, '2018-11-18 21:52:10', '2018-11-18 21:52:10', NULL),
(96, 'update-newsletter', 'Update Newsletter Permission', 0, 1, 1, NULL, '2018-11-18 21:52:10', '2018-11-18 21:52:10', NULL),
(97, 'delete-newsletter', 'Delete Newsletter Permission', 0, 1, 1, NULL, '2018-11-18 21:52:10', '2018-11-18 21:52:10', NULL),
(98, 'manage-video', 'Manage Video Permission', 0, 1, 1, NULL, '2018-11-19 08:22:03', '2018-11-19 08:22:03', NULL),
(99, 'create-video', 'Create Video Permission', 0, 1, 1, NULL, '2018-11-19 08:22:03', '2018-11-19 08:22:03', NULL),
(100, 'store-video', 'Store Video Permission', 0, 1, 1, NULL, '2018-11-19 08:22:03', '2018-11-19 08:22:03', NULL),
(101, 'edit-video', 'Edit Video Permission', 0, 1, 1, NULL, '2018-11-19 08:22:03', '2018-11-19 08:22:03', NULL),
(102, 'update-video', 'Update Video Permission', 0, 1, 1, NULL, '2018-11-19 08:22:03', '2018-11-19 08:22:03', NULL),
(103, 'delete-video', 'Delete Video Permission', 0, 1, 1, NULL, '2018-11-19 08:22:03', '2018-11-19 08:22:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 2),
(2, 3, 2),
(3, 4, 2),
(4, 5, 2),
(5, 6, 2),
(6, 7, 2),
(7, 8, 2),
(8, 16, 2),
(9, 20, 2),
(10, 24, 2),
(11, 25, 2),
(12, 26, 2),
(13, 27, 2),
(14, 28, 2),
(15, 29, 2),
(16, 30, 2),
(17, 31, 2),
(18, 33, 2),
(19, 34, 2),
(20, 35, 2),
(21, 36, 2),
(22, 37, 2),
(23, 38, 2),
(24, 39, 2),
(25, 40, 2),
(26, 41, 2),
(27, 42, 2),
(28, 43, 2),
(29, 44, 2),
(30, 45, 2),
(31, 46, 2),
(32, 47, 2),
(33, 48, 2),
(35, 2, 3),
(36, 91, 3),
(37, 2, 4),
(38, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_user`
--

INSERT INTO `permission_user` (`id`, `permission_id`, `user_id`) VALUES
(1, 42, 2),
(2, 34, 2),
(3, 38, 2),
(4, 29, 2),
(5, 46, 2),
(6, 25, 2),
(7, 44, 2),
(8, 36, 2),
(9, 40, 2),
(10, 31, 2),
(11, 48, 2),
(12, 27, 2),
(13, 43, 2),
(14, 35, 2),
(15, 39, 2),
(16, 30, 2),
(17, 47, 2),
(18, 26, 2),
(19, 8, 2),
(20, 3, 2),
(21, 5, 2),
(22, 1, 2),
(23, 33, 2),
(24, 37, 2),
(25, 41, 2),
(26, 6, 2),
(27, 7, 2),
(28, 28, 2),
(29, 45, 2),
(30, 24, 2),
(31, 20, 2),
(32, 16, 2),
(33, 4, 2),
(34, 2, 3),
(106, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT 0,
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Administrator', 1, 1, 1, 1, NULL, '2018-11-17 03:54:43', '2018-11-17 03:54:43', NULL),
(2, 'Executive', 0, 2, 1, 1, NULL, '2018-11-17 03:54:43', '2018-11-17 03:54:43', NULL),
(3, 'User', 0, 3, 0, 1, 1, '2018-11-17 03:54:43', '2018-11-17 06:28:26', NULL),
(4, 'Ahli Edukasi', 0, 4, 0, 1, 1, '2018-11-17 22:24:19', '2018-11-17 22:24:49', NULL),
(5, 'Ahli Konseling', 0, 5, 0, 1, 1, '2018-11-17 22:24:35', '2018-11-17 22:25:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(56, 12, 3),
(59, 4, 4),
(62, 13, 4),
(64, 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `selfdevelopments`
--

CREATE TABLE `selfdevelopments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_datetime` datetime NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Published','Draft','InActive','Scheduled') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `selfdevelopments`
--

INSERT INTO `selfdevelopments` (`id`, `name`, `publish_datetime`, `featured_image`, `content`, `meta_title`, `cannonical_link`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Benarkah Ada Cinta diantara Setiap Pasangan?', '2018-11-18 18:42:00', '1542645726.jpeg', '<p class=\"p1\">Jika kita mendengar kata &lsquo;cinta&rsquo;, apa sih yang biasanya ada di benak kita ? <em>yup</em>, kasih sayang, kebaikan, kesetiaan dan ketulusan dari apa yang seseorang lakukan untuk pasangannya.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Kalau kita simpulkan dari kamus Webster, arti cinta erat kaitannya dengan ketulusan dan cinta tanpa syarat.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\"><em>&ldquo;Nah kalau gitu, apakah pasti ada cinta di setiap pasangan perkawinan?&rdquo;</em><span class=\"s1\"><em><span class=\"Apple-converted-space\">&nbsp;</span></em></span></p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p1\">Oke, sebelum kita menjawab sendiri, sudah ada penelitian kecil yang dilakukan dengan menanyakan kepada 10 pasangan yang telah menikah antara tiga sampai lima tahun terkait apakah benar pasangan itu mengalami cinta dan apakah mereka benar saling mencintai ? Ternyata, mayoritas menjawab tidak !. Mereka mengatakan bahwa cinta itu hanya ada dalam karya sastra. Kita akan mendapatkan bahwa sebuah cekcok keluh kesah yang terjadi diantara pasangan adalah sesuatu yang sudah biasa. Saling bertahan bahwa dirinya lah yang sudah banyak berkorban untuk mempertahankan hubungan, padahal belum tentu itu benar terjadi. Jadi, udah mulai kebayang belum tentang definisi dan makna cinta sebenarnya ?<span class=\"Apple-converted-space\">&nbsp; </span>Atau, udah mulai bisa mengira sebenarnya ada nggak sih cinta di setiap pasangan ?<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\"><strong>Bukan Cinta tapi Pengembungan Ego</strong></p>\r\n<p class=\"p1\">Sebenarnya ada banyak contoh perilaku yang kelihatannya seperti ekspresi cinta tapi sebenarnya egotistik. Coba bayangkan seorang suami yang begitu &lsquo;mencintai&rsquo; istrinya sehingga saat mereka terpisah karena urusan pekerjaan, si suami terus menerus menelpon guna menanyakan keberadaan dan rekan-rekan seperjalanan istrinya.</p>\r\n<p class=\"p1\">Ada juga suami yang membeli mobil baru dan memberikan kepada istri pada saat ulang tahun si istri. Ia membayangkan kesenangan istri saat menerima hadiahnya. Jika dilihat lebih jauh, tindakan ini tidak sesungguhnya menunjukkan ekspresi cinta melaikan pemuasan egotistik bahwa suami merupakan orang yang patut dibanggakan.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Sering terjadi, pasangan melakukan tindakan-tindakan yang tampaknya bermakna cinta. Namun tanpa disadari mereka mengembungkan ego mereka masing-masing melalui tindakan itu. Akibatnya kebahagiaan dan kepuasan perkawinan bukan diusahakan berdua melainkan dikuasai secara<span class=\"Apple-converted-space\">&nbsp; </span>sepihak oleh dia yang memiliki kekuasaan finansial. Maka, kebahagiaan atas nama cinta bukan dikemudikan bersama-sama melainkan dimonopoli satu pihak. Jadi, karena semua diusahakan demi terlihat cinta, tapi nyatanya tidak, kan ?</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\"><strong>Tipu Diri dalam Cinta</strong></p>\r\n<p class=\"p1\">Jika dilihat dalam proses konseling, tanpa disadari sejumlah konselor membantu klien mengembangkan &ldquo;tipuan diri dalam cinta&rdquo;. <em>Wah, maksudnya gimana tuh?<span class=\"Apple-converted-space\">&nbsp;</span></em></p>\r\n<p class=\"p1\"><span class=\"Apple-converted-space\">&nbsp;</span>Oke, jadi begini. Sebagai contoh, nasihat atau petunjuk dalam konsultasi psikologi di media massa sering berbentuk &ldquo;sekali jadi&rdquo; atau bisa dibilang instan. Mereka memberikan petunjuk-petunjuk teknis dan praktis namun tidak kuat tertanam dalam sikap seseorang dan tidak mengelola proses pikir yang menjadi akar bagi tumbuhnya perilaku sehat. Contoh-contoh nasihat sekali jadi: &ldquo;Jika engkau ingin membuat pasanganmu bahagia, kirimkanlah kepadanya bunga mawar sekali seminggu di kantornya.&rdquo;<span class=\"s1\"><span class=\"Apple-converted-space\">&nbsp;</span></span></p>\r\n<p class=\"p1\">Apakah tindakan ini akan berhasil? Belum tentu! Bagaimana jika pasangan penerima bunga itu mempunyai pengalaman traumatis tentang bunga mawar? Barangkali ia jauh lebih membutuhkan kecupan saat ia pulang. Atau, bagaimana jika orangnya sangat berhitung dalam hal keuangan? Barangkali ia lebih senang bahwa uang pembeli bunga ditabung dan dipakai untuk libur bersama di akhir tahun.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\">Itulah yang disebut tipu diri dalam cinta. Orang mengekspresikan cinta dalam tindakan-tindakan hampa, yang sebenarnya merupakan pengembungan ego. Ia sendiri yang puas dengan tindakan itu bukan pasangannya. Pasangannya hanya berpura-pura senang.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>', 'Benarkah Ada Cinta diantara Setiap Pasangan?', NULL, 'benarkah-ada-cinta-diantara-setiap-pasangan', '<p>Jika kita mendengar kata &lsquo;cinta&rsquo;, apa sih yang biasanya ada di benak kita ? <em>yup</em>, kasih sayang, kebaikan, kesetiaan dan ketulusan dari apa yang seseorang lakukan untuk pasangannya.</p>', 'benarkah, cinta', 'Published', 4, NULL, '2018-11-17 21:42:41', '2018-12-04 00:17:48', NULL),
(3, 'CARA LULUS TES PSIKOLOG PERUSAHAAN', '2018-11-18 19:04:00', '1542645953.jpeg', '<p class=\"p1\">Dewasa ini, semakin banyak perusahaan dan instansi yang menggunakan tes psikologi untuk menyeleksi calon karyawannya. Menurut Spector (2012), tes psikologi ini merupakan salah satu cara yang secara valid mampu untuk mengukur kesesuaian antara harapan perusahaan dan keadaan calon karyawan. Tes psikologi dapat memberikan gambaran potensi seseorang dari berbagai aspek, yakni aspek kognitif, sikap kerja, dan kepribadian seseorang.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\">Tes Psikologi merupakan salah satu alat yang tepat untuk memberikan gambaran seseorang. Hal ini disebabkan oleh terdapat berbagai jenis tes dan panduan penilaian yang sudah terukur validitas dan reliabilitasnya. Namun, semakin sering digunakan maka semakin banyak pula penyimpangan yang terjadi akibat proses seleksi tersebut. Penyimpangan yang paling menonjol adalah calon karyawan berusaha mencari &ldquo;jalan pintas&rdquo; agar dapat lolos tes psikologi tersebut. Mereka menggunakan berbagai cara termasuk menggunakan &ldquo;Buku Panduan&rdquo; untuk lulus Tes Psikologi.Banyak buku yang dijual secara bebas menyatakan bahwa dengan membeli buku mereka maka seseorang dijamin akan lulus masuk ke perusahaan atau instansi yang didambakan.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Pada dasarnya tes psikologi yang diberikan oleh perusahaan dapat berbeda antar perusahaan yang satu dengan perusahaan yang lainnya. Hal ini disebabkan oleh kebutuhan perusahaan tersebut terhadap aspek calon karyawan/karyawan yang ingin dinilai. Hal ini yang mengakibatkan ketika seseorang menggunakan suatu buku untuk lolos seleksi, Ia sebenarnya belum tentu akan lolos karena Ia belum mengetahui langsung jenis tes apa yang Ia dapatkan.</p>\r\n<p class=\"p1\">Selain perbedaan pada hal yang diukur, pada jenis-jenis tes tertentu seseorang tidak bisa menginstruksikan secara rinci hal yang harus dilakukan. Contohnya pada tes yang sering dikatakan oleh orang awam tes menggambar, seseorang tidak bisa memberikan panduan secara jelas bagaimana cara menggambar. Walaupun pada buku tersebut diberikan contoh gambar yang menurut buku itu sesuai, pada dasarnya bukan hal tersebut yang dilihat atau dinilai perusahaan. Hal-hal seperti ini pada akhirnya membawa calon karyawan tersebut dinilai sebagai seseorang yang berlaku curang dan justru berujung pada kegagalan.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p1\">Jadi pada akhirnya, cara agar lulus tes psikologi pada satu instansi adalah memperiapkan diri sebaik mungkin. Baik secara fisik seperti istirahat dan mengkonsumsi makanan yang cukup sebelum melakukan test, maupun mempersiapkan diri dan tidak panik saat tes berlangsung. Mencari tahu tentang perusahaan yang akan dilamar juga perlu agar mampu memahami apa yang diharapkan dari Anda saat melakukan tes. Perilaku yang baik dan sopan serta cermat mendengarkan setiap instruksi yang diberikan pembawa tes sangat penting dilakukan. Hal tersebut yang lebih menunjang keberhasilan Anda saat mengerjakan tes psikologi di suatu instansi/perusahaan.</p>', NULL, NULL, 'cara-lulus-tes-psikolog-perusahaan', NULL, NULL, 'Published', 4, NULL, '2018-11-17 22:04:59', '2018-11-19 02:46:32', NULL),
(4, 'CARA MENJADI PEMAAF', '2018-11-18 19:05:00', '1542646027.jpeg', '<p class=\"p1\">&ldquo;Dalam beretika, pertama-tama bukanlah permaafannya, tetapi kesalahannya.&rdquo;</p>\r\n<p class=\"p1\">Demikian tulis Hendardi, Ketua Badan Pengurus Setara Institute,<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\"><strong>Mengapa memaafkan?</strong></p>\r\n<p class=\"p3\">Agar dapat memahami makna memaafkan, kita perlu merenungkan situasi hidup tanpa permaafan. Hidup tanpa permaafan melanggengkan derita psikis yang berawal dari sikap permusuhan dan keinginan mengalahkan. Biasanya sikap dan keinginan ini (tanpa disadari) berlatar belakang amarah, suatu emosi yang menghabiskan energi mental dan memperpanjang stres.</p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p class=\"p3\">&ldquo;Kita pun terpenjara dalam keinginan berbalas dendam. &ldquo;</p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p class=\"p3\">Dengan dendam sebagai motif psikis, kita menginginkan si bersalah menderita. Dendam, tulis John Monbourquette (2000) dalam <em>How to Forgive</em>, merupakan keadilan instinktual yang mencuat dari alam bawah sadar. Derita menghendaki derita atas nama keadilan instinktual. Akibatnya,<span class=\"Apple-converted-space\">&nbsp; </span>kita terpikat dan terikat oleh rantai derita, berbalut kekerasan yang tiada putusnya.</p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p class=\"p3\">&ldquo;Rantai derita mesti diputus oleh sikap memaafkan.&rdquo;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\"><strong>Melibatkan totalitas kedirian</strong></p>\r\n<p class=\"p3\">Memaafkan merupakan proses panjang, menyakitkan sekaligus membebaskan. Dan karena itulah ia melibatkan totalitas kedirian kita sebagai manusia. Ia harus mulai dari keputusan untuk tidak berbalas dendam. Dari sudut pandang keadilan instinktual keputusan psikis ini sungguh menyakitkan.</p>\r\n<p class=\"p3\">Setelah berkeputusan demikian, kita berusaha menelusuk masuk ke relung kalbu sendiri. Kita bersadar diri dan menemukan berbagai kelemahan sendiri seperti rasa malu, kecenderungan agresif, keinginan berbalas dendam, rasa tertelantar, dan keinginan untuk melupakan begitu saja. Sungguh menyakitkan karena pemeriksaan batin menyadarkan kita bahwa ternyata kita tidak jauh berbeda dari orang yang bersalah pada kita.</p>\r\n<p class=\"p3\">Jika kita terpengaruh berbagai kelemahan itu, kita terjebak pada cara pandang picik yang menjadikan<span class=\"Apple-converted-space\">&nbsp; </span>kesalahan si bersalah bagai bencana. Itulah yang disebut <em>catastrophizing, </em>tulis Aaron Beck (1999) dalam <em>Prisoners of Hate: The Cognitive Basis of Anger, Hostility, and Violence.</em><span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p3\">Jadi, menyadari kelemahan sendiri, kita melangkah ke proses <em>reframing</em>, meletakkan kesalahan itu ke dalam konteks yang lebih luas dan memandangnya dengan kacamata yang lebih bening alias adaptif. Proses tidak berhenti di situ. Kita berusaha menyadari martabat orang yang bersalah. Betapapun bersalahnya orang itu, dia pun mampu berubah dan menjadi baik.</p>\r\n<p class=\"p3\">Memang proses ini mengandaikan kita siap akan risiko bahwa ia kemungkinan mengulangi kesalahannya. Tapi, begitulah permaafan otentik dan tulus. Kata Jacques-Marie Pohier dalam John Monbourquette (2000): &ldquo;Karena itu memaafkan itu sulit karena kita takut akan risikonya.&rdquo; <span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\"><strong>Langkah-langkah memaafkan</strong></p>\r\n<p class=\"p3\">Dalam <em>Putting Forgiveness into Practice</em>, Doris Donneley (1982) menjabarkan langkah-langkah memaafkan sebagai berikut: mengenali luka batin kita, memutuskan untuk memaafkan, menyadari kesulitan dalam memberi maaf, dan menyadari dampak negatif dari ketiadaan permaafan.</p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p class=\"p3\">Sementara David Norris (1984) dalam <em>Forgiving from the Heart</em><span class=\"Apple-converted-space\">&nbsp; </span>mengusulkan lima langkah: memperteguh niat memaafkan, secara akurat memeriksa kembali pelanggaran (kesalahan) orang yang akan dimaafkan, memaknakan kembali luka batin akibat kesalahan, membina kembali relasi yang terputus, dan mengintegrasikan kembali berbagai retak psikis yang dialami akibat luka batin.</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p4\">&nbsp;</p>', NULL, NULL, 'cara-menjadi-pemaaf', NULL, NULL, 'Published', 4, NULL, '2018-11-17 22:05:30', '2018-11-19 02:47:19', NULL),
(5, 'Cinta Monyet - Cinta Paranoid', '2018-11-18 19:05:00', '1542646077.jpeg', '<p class=\"p1\">Cinta ABG (anak baru <em>gede</em>) serring disebut &ldquo;cinta monyet&rdquo;, cinta yang lebih sering cemburu buta dan senang membatasi ruang gerak pasangan cinta. Tapi, dari mana asal-usul &ldquo;cinta monyet&rdquo;?<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Menurut kolega senior, almarhum Drs. Frans Odjan, &ldquo;cinta&rdquo; itu diberi atribut &ldquo;monyet&rdquo; karena demikianlah pola cinta Ibu monyet terhadap anak monyet. Saat terancam bahaya &ndash; misalnya saat anak monyet akan dirampas seorang pemburu &ndash; Ibu monyet akan mendekap anak monyet ke dadanya. Begitu erat dekapan itu, sehingga anak monyet yang masih bertubuh ringkih justru terbekap sangat erat dan mati perlahan di dada Ibunya. Anak monyet mati dalam dekapan dan bekapan &ldquo;kasih sayang&rdquo;. Tragis!</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Cinta paranoid</strong></p>\r\n<p class=\"p1\">Ciri fenomenologis cinta paranoid itu adalah larangan yang tidak masuk di akal terhadap pasangan. Kendati demikian, alasan yang diberikan oleh si paranoid guna menjelaskan larangannya sering begitu kuatnya sehingga sulit dibantah. Akibatnya pasangan hanya bisa merasa terbatasi dan sulit bergerak. Bahkan pasangan akan terus merasa dicurigai serta serba salah.</p>\r\n<p class=\"p1\">Menurut Theodore Millon dan kawan-kawan dalam buku monumental <em>Disorders of Personality: DSM-IV(diagnostic and statistical manual of mental disorders-IV) and Beyond</em> (1996), paranoid sering berperilaku waspada. Ia sangat peka terhadap sedikit celaan dari pasangannya. Ia tidak mau dikontrol pasangannya. Ia tidak mudah memaafkan. Sebagai pendendam, ia suka berbalas dendam.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Ia bersikap tegas dan keras terhadap pasangannya. Kekerasan dan ketegasan bukan hanya bersifat verbal tapi bisa juga fisik. Akibatnya sering terjadi kasus kekerasan dalam rumah tangga (KDRT). Tidak jarang relasi paranoid ini berakhir dengan membunuh pasangan.</p>\r\n<p class=\"p1\">Secara kognitif ia bersikap skeptis dan sinis terhadap &rdquo;maksud tersembunyi&rdquo; pasangannya. Barangkali pasangannya tidak bermaksud jelek, tapi tindakan pasangan sering ditafsirkan negatif. Misalnya, pasangan diundang makan malam dan dalam perjalanan pulang, mobilnya mogok; Akibatnya, pasangan pulang larut malam. Kenyataan ini akan secara meyakinkan ditafsirkan sebagai perbuatan serong. Si paranoid begitu lihai memberi pembuktian bahwa benar pasangan itu telah berselingkuh. Ia mampu mencari dan menambah fakta tambahan untuk membuktikan keyakinannya.</p>\r\n<p class=\"p1\">Gambaran diri pasangan paranoid ini tidak pernah berubah. Ia selalu merasa penting dan terhormat. Akibatnya ia bersikap seakan-akan tidak tergantung pada pasangannya dan tidak membutuhkan perhatian pasangannya. Tentu, ia tidak bersedia mendengarkan nasihat pasangannya. Ia juga tidak mau mengakui ciri-ciri negatif yang sebenarnya dimilikinya. Sebaliknya, ia sangat kritis terhadap ciri-ciri serupa yang dimiliki pasangannya. Misalnya, ia bagaikan tidak menyadari bahwa hubungannya dengan lawan jenis sudah mengarah pada perselingkuhan. Tapi, ia akan sangat peka dan menuduh pasangannya berselingkuh saat ia menangkap signal palsu perselingkuhan pasangannya seperti pulang terlambat karena mobil mogok.</p>\r\n<p class=\"p1\">Ekspresi emotif paranoid itu dingin tanpa perasaan. Ia jarang tersenyum. Jangan harap humor menyeruak dari mulutnya. Sebaliknya, ia mudah cemburu dan jengkel. Ia mudah bereaksi marah.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Pola pikir paranoia</strong></p>\r\n<p class=\"p1\">Secara etimologis <em>paranoia</em> (bahasa Yunani) yang dalam konteks psikologi telah digunakan sejak 2000 tahun itu berarti &ldquo;<em>thinking besides oneself</em>&rdquo; alias berpikir serampangan. Dalam bukunya <em>Emotional Vampires: Dealing with People Who Drain You Dry</em> (2001), Albert Einstein, PhD. mengatakan bahwa istilah ini dipakai untuk menggambarkan berbagai macam <em>craziness</em> (tidak masuk di akal), khususnya menyangkut keyakinan tanpa dasar dari seorang yang paranoid.</p>\r\n<p class=\"p1\">Paranoia, kata Einstein, mudah dimengerti jika kita mencermati pola pikir yang menghasilkan keyakinan keliru, bukan kekeliruan pikiran itu sendiri. Lain halnya dengan orang obsesif-kompulsif yang begitu sibuk dengan segala detil yang kecil sehingga pikirannya tidak terfokus. Sebaliknya paranoid menjadikan dirinya gila dengan kesibukan merangkai berbagai hal detil menjadi satu keseluruhan yang utuh.</p>\r\n<p class=\"p1\">Misalnya, ia sibuk mengumpulkan signal-signal palsu untuk membangun pembuktikan bahwa pasangannya berselingkuh. Signal palsu itu bisa berupa suatu waktu pasangannya pulang malam. Suatu waktu ia menemukan surat dari seseorang bagi pasangannya. Suatu saat ia mendengar pasangannya cekikikan saat bertelepon. Suatu kesempatan ia membaca SMS &ldquo;<em>take care</em>&rdquo; dalam handphone pasangannya. Dan seterusnya.</p>\r\n<p class=\"p1\">Signal-signal itu disebut palsu karena mengandung berbagai penafsiran. Tapi, bagi paranoid, signal tersebut akan selalui ditafsirkan dalam kategori &ldquo;putih-hitam&rdquo; atau &ldquo;cinta-benci&rdquo;. Masalahnya, paranoid lebih sering melihat sisi &ldquo;hitam&rdquo; atau &ldquo;benci&rdquo; dalam setiap detil fakta. Jadi, pada signal-signal palsu tersebut ia melihat dalam delusi bahwa pasangannya &ldquo;tidak menyukai&rdquo; (=membenci) dirinya.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Kenyataan klinis</strong></p>\r\n<p class=\"p1\">Menurut Millon dan kawan-kawan, ada tiga kenyataan klinis (<em>clinical features</em>) yang mendasari pola pikir paranoid.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Pertama, paranoid yang mencapai 0,5-2.5% dari total populasi masyarakat itu secara psikis sangat bergantung pada pasangannya. Ketergantungan itu bukan terutama karena kelemahan dan kekurangannya, melainkan karena ketidakpercayaannya pada orang lain. Mempercayakan diri pada orang lain itu sama saja dengan menjerumuskan diri pada kemungkinan untuk dihianati cinta.</p>\r\n<p class=\"p1\">Kedua, kecurigaan dan delusi (bahwa dirinya tidak dicintai) selalu mewarnai pola pikir paranoid yang secara statistik lebih banyak pria daripada wanita. Keengganannya untuk mendengarkan (nasihat) orang lain membuat dirinya terisolasi secara psikis dan lepas dari kemungkinan untuk meluruskan cara berpikirnya yang negatif.</p>\r\n<p class=\"p1\">Ketiga, kewaspadaan dan sikap bermusuhan menimbulkan ketegangan psikis seorang paranoid yang biasanya telah mengembangkan kepribadian ini sejak masa kanak-kanak. Kelemahan diri ini ditutupi dengan citra diri yang tinggi dengan memproyeksikan kekurangan diri itu pada pasangan.</p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Andaikan...</strong></p>\r\n<p class=\"p1\">Andaikan kita telah terbuai dalam cinta pasangan paranoid, begini nasihat Bernstein (hlm. 214), &ldquo;Jangan menyembunyikan apa pun.&rdquo; Kita bersikap apa adanya, sebab apa pun yang disembunyikan pasti akan dijumpai oleh pasangan paranoid. Selanjutnya, &ldquo;Bersikaplah setia, tapi tidak perlu membuktikan bahwa Anda itu setia.&rdquo; Sebab, sekali kita membuktikan diri setia, maka kita akan selalu dituntut untuk selalu membuktikan diri setia. Dan, itu akan sangat melelahkan. Biarlah si paranoid itu mencari bukti bagi dirinya sendiri.</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p6\">Penulis adalah dosen program Bimbingan dan Konseling Atma Jaya dan Fakultas Psikologi Universitas Pelita Harapan</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p7\"><span class=\"Apple-converted-space\">&nbsp;</span>Artikel ini pernah dimuat di rubrik Konsultasi Keluarga, Majalah Hidup Katolik, no. 46 Tahun ke-61, 18 November 2007</p>', NULL, NULL, 'cinta-monyet-cinta-paranoid', NULL, NULL, 'Published', 4, NULL, '2018-11-17 22:05:58', '2018-11-19 02:47:57', NULL),
(6, 'Dari Mana Datangnya Predator Anak?', '2018-11-18 19:06:00', '1544457608.jpg', '<p class=\"p1\">Menurut Komisi Perlindungan Anak Nasional, laporan ke kantor mereka dalam lima tahun terakhir persentasi kejadian berlipat ganda sebanyak 200%. Menurut Seto Mulyadi pemerhati kesejahteraan anak sejak lama, angka tersebut hanyalah &ldquo;puncak gunung es.&rdquo; Artinya, jumlahnya kira-kira hanya satu per sepulah dari jumlah kasus sebenarnya.</p>\r\n<p class=\"p1\">Banyak kasus tidak dilaporkan keluarga karena terjadi di lingkungan keluarga itu sendiri. Rasa malu keluarga membuat kasus menguap begitu saja.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p4\"><strong>Pembentuk predator</strong></p>\r\n<p class=\"p1\">Pedator anak. Dari mana asal-muasalnya? Seorang ahli psikologi di tim panelis sangat yakin pornografi adalah penyebaBnya. Ia pun memberikan sejumlah fakta tentang meluasnya pornografi. Selama ini saya juga mendengar banyak orang dan ahli menunjuk ke arah pornografi sebagai biang kerok masalah.</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p1\">Saya mempunyai pikiran berbeda. Dengan cara berpikir <strong>teleologis</strong> (ajaran yang menerangkan segala sesuatu dan segala kejadian menuju pada tujuan tertentu) saya ingin mencari &ldquo;penyebab awal&rdquo; dari gejala yang muncul sejak zaman purbakala sebelum adanya pornografi moderen. Jika kita melongok ke masyarakat klasik Yunani relasi seksual antara guru (pria) dan anak didik (pria) itu merupakan bagian dari budaya formal. Anak-anak laki-laki dididik di asarama dan diajar oleh guru-guru pria. Ibu-ibu dan anak-anak perempuan tinggal di rumah untuk urusan dapur dan melahirkan anak saja.</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p1\">Relasi seksual antara guru pria dan anak-anak laki-laki justru menjadi sarana untuk pengajaran cinta tertinggi. Itulah sebabnya pada masa itu ada patung-patung seksi dan telanjang, justru bukan patung perempuan atau anak gadis melainkan patung anak laki-laki atau pemuda.</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p1\">Sekarang ini, di mana calon predator itu &ldquo;disiapkan&rdquo;?</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p1\">Menurut pendapat saya, bukanlah pornografi yang membentuk predator itu. Pornografi itu justru menjadi akibat dari suatu rangkaian proses sebab-akibat. Kita mesti mencari penyebab awal. Rangkaian sebab-akibat itu dapat dicontohkan oleh teori bola sodok (biliar). Cara berpikir teleologis paling bagus digambarkan dengan teori bola sodok. Bola utama menyodok bola kedua, dan bola kedua menyodok bola ketiga, dan seterusnya.</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p4\"><strong>Pola pengasuhan anak</strong></p>\r\n<p class=\"p1\">Bagi saya &ldquo;bola utama&rdquo; itu adalah cara orangtua atau pengasuh mengasuh anak sejak bayi sampai usia kira-kira lima sampai tujuh tahun. Pengasuhan yang kurang tepat akan menghasilkan konsep diri yang keliru pada anak. Konsep diri yang keliru itu akan menghasilkan anak yang tidak percaya diri.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Anak yang tidak percaya diri akan melahirkan anak dengan regulasi diri yang kacau. Regulasi diri yang kacau akan mengakibatkan anak sulit mengendalikan dirinya. Salah satu dampaknya adalah kurangnya atau tidak adanya kedekatan emosional yang nyaman (tidak ada <em>secure attachment</em>) dengan orang lain. Anak berelasi dengan orang lain secara tidak nyaman.</p>\r\n<p class=\"p1\">Nah, keadaan ini akan diperparah oleh pengalaman-pengalaman traumatis seperti keluarga berantakan, perceraian orangtua, kematian orangtua, kematian kakak, pelecehan di rumah atau di sekolah, dan seterusnya. Akibatnya, dalam tingkat tertentu anak mengalami gangguan kepribadian seperti kepribadian antisosial. Dari sini baru muncullah masalah-masalah perlawanan terhadap tata nilai dan tata aturan sosial seperti pornografi dan predator seksual.</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p1\">Jadi, asulah anak-anak dengan cara yang benar. Anak dengan kepribadian yang sehat tidak akan mencari pelarian ke pornografi. Barangkali anak yang berkepribadian yang sehat itu akan menonton pornografi, sekedar untuk mengetahuinya. Tapi, ia tidak melakukannya secara kompulsif (tidak sehat).</p>', NULL, NULL, 'dari-mana-datangnya-predator-anak', NULL, NULL, 'Published', 4, NULL, '2018-11-17 22:06:34', '2018-12-10 09:29:13', NULL),
(8, 'DRAKULA NARSIS', '2018-11-18 19:07:00', '1544457673.jpg', '<p class=\"p1\">Seorang Psikolog Makati Medical Center di Manila, mengatakan dalam <em>Personalities and Psychopathology</em>. &ldquo;Semua pemimpin itu berkepribadian narisistik, tapi tidak semua narsis itu menjadi pemimpin,&rdquo; <span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\">Theodore Millon dalam buku monumental <em>Disorders of Personality: DSM-IV(diagnostic and statistical manual of mental disorders-IV) and Beyond</em> (1996) melukiskan arogansi drakula narsis. Mereka lihai melanggar norma, hukum, dan aturan sosial. Hukum dan aturan hanya berlaku pada orang lain, tidak pada diri sendiri. Mereka mencari keuntungan diri sendiri. Sangat peka akan hak sendiri tanpa peduli hak orang lain. Diri dianggap penting. Orang lain tiada artinya.</p>\r\n<p class=\"p3\">Mereka senang membayangkan diri sebagai yang ter... Terbaik, terhebat. Suka menerawang ke alam uang, kuasa, dan cinta. Tidak malu berdusta demi ilusi kebesaran diri. Mereka tampak bercitra diri tinggi. Tampil elegan, mengagumkan, dan menjadi pusat perhatian. Sikap mengambang dan tidak tulus. Cepat mengubah tampilan sikap sesuai tuntutan keadaan. Kemampuan rasionalisasi mengagumkan. Lihai berkelit dari kekeliruan dan kegagalan. Karena itu mereka tampil tenang dan berwibawa.</p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p class=\"p3\">Menghadapi drakula narsis, perasaan kita bercampur aduk. Sebagai pengikut atau penggemar, kita bangga akan pencapaian para narsis. Tapi, kita sebal akan kebusukan mereka. Kita kesal mereka mengabaikan kebutuhan dan kepentingan kita. Tapi, kita mengiya pada kecenderungan mereka untuk dikagumi. Kita jengkel sekaligus terpukau akan tampilan mereka.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p3\">Masalahnya, bagaimana jika para narsis pemimpin itu saling bertarung? Krisis! Lihatlah sejarah bangsa ini! Setiap krisis besar dilatarbelakangi pertarungan drakula narsis.</p>\r\n<p class=\"p3\">Namun, krisis tidak akan berkepanjangan karena para narsis akan berkelit. Mereka berkelit tanpa malu dan sesal bagaikan tak ada apa-apa. Mereka melakukan <em>deal</em> dan perdamaian pura-pura. Apa hendak dikata? &ldquo;Kita membutuhkan mereka, sebab tanpa para narsis, siapa yang akan memimpin kita?,&rdquo; tulis Bernstein (hlm. 132).<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Rasa Bersalah dan Rasa Malu<span class=\"Apple-converted-space\">&nbsp;</span></strong></p>\r\n<p class=\"p3\">Kita berharap setiap pemimpin dapat mencontoh sikap Toshikatsu Matsuoka, menteri pertanian dan kehutanan Jepang di masa pemerintahan kabinet Shinzo Abe, yang bunuh diri pada 28 Mei 2007 karena skandal keuangan.</p>\r\n<p class=\"p3\">Namun, harapan kita tidak sesuai dengan logika berpikir narsis yang mempunyai &ldquo;<em>Big Ego</em>&rdquo;. Jangan keliru, keakuan tinggi ini berpangkal pada <em>self-esteem</em> yang rendah. Lagi pula, <em>Superego</em> (kesadaran moral dan hukum) narsis itu lemah. Akibatnya, rasa bersalah dan rasa malu mengendor. &ldquo;Budaya malu&rdquo; dan &ldquo;budaya salah&rdquo; tidak diberdayakan dalam diri drakula narsis karena proses pembelajaran dan pola pengasuhan keliru di masa kecil.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p3\">Sebaliknya, bangsa Jepang telah membudayakan &ldquo;rasa malu&rdquo; dan &ldquo;rasa bersalah&rdquo; sejak abad ke-8, awalnya jaman para Shogun. Citra diri yang gagal (dalam mewujudkan amanah sosial) mengakibatkan rasa malu dan rasa bersalah. Sebagai bentuk tanggung jawab sosial atas citra diri gagal, mereka lalu mengurbankan diri (<em>harakiri</em> atau <em>seppuku</em>).</p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p5\"><em>Pendidikan di dalam keluarga itu mesti dilanjutkan di dalam dunia pendidikan formal.</em></p>', NULL, NULL, 'drakula-narsis', NULL, NULL, 'Published', 4, NULL, '2018-11-17 22:07:33', '2018-12-10 09:28:37', NULL),
(11, 'Mencegah Kecanduan Internet melalui  REBT (Rational Emotive Behavioral Therapy)', '2018-11-18 19:08:00', '1544457701.jpg', '<p class=\"p1\">Internet merupakan alat pembelajaran yang sangat bermanfaat bagi anak dan remaja. Hampir semua yang mereka butuhkan dapat ditelurusi melalui internet. Internet melayani mereka dengan kemudahan. Tanpa biaya besar dan tanpa upaya berarti, dari ruang belajar, mereka bisa menemukan berbagai informasi di perpustakaan mahaluas yang bernama internet.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\">Berbagai kemudahan tersebut tanpa disadari membawa anak dan remaja ke kondisi psikis berbahaya. Namanya adalah <em>internet addiction</em> alias kecanduan berinternet. Tanpa mampu mengontrol diri mareka duduk berjam-jam sehari menikmati berbagai fasilitas internet seperti <em>chatting</em>, <em>gaming</em>, dan <em>surfing</em> (semisal laman pornografi).<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\">Fenomena kecanduan internet mulai diperhatikan para ahli psikologi klinis seperti Kimberly Young, Ph.D dan David Greenfield, Ph.D. yang mengistilahkannya sebagai <em>internet addiction </em>yang mempunyai keserupaan mekanisme psikis dengan kecanduan merokok, minum alkohol, dan berjudi. Orang menjadi begitu tergantung akan stimulasi informasi internet (semisal sosialisasi <em>chatting, games</em>, dan tampilan pornografi) sehingga kesenangan (<em>pleasure</em>) hidupnya menjadi terbatas pada stimulasi itu. Pelan tapi pasti mereka mengabaikan tugas sekolah, pekerjaan rumah, dan relasi sosial dengan anggota keluarga lain. Mereka akan terperangkap pada &lsquo;batas toleransi&rsquo; yang semakin lama semakin meningkat. Waktu yang dibutuhkan untuk mencapai kepuasan (berinternet) semakin lama semakin tinggi. Sudah ada contoh di Korea, kecanduan ini bisa mematikan.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\">Penulis tertarik berbagi pikiran dan keahlian (psikologi konseling klinis) dengan para guru dan orang tua guna mencegah dan mengatasi masalah kecanduan anak dan remaja akan internet (<em>chatting, gaming, surfing</em>) melalui kegiatan konseling dengan pendekatan <em>rational emotive behavioral therapy</em> (REBT).</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\">Dengan modul yang berorientasi REBT, pembimbing (guru atau orang tua) pertama-tama akan dituntun untuk memperbaiki cara pandang (kognitif) anak tentang internet. Dengan kognitif yang positif, berbagai masalah emosi dan afeksi anak akan ditanggulangi. Kemudian, berbekal berbagai teknik modifikasi perilaku, anak dan remaja diajarkan &lsquo;memperlakukan&rsquo; internet dan berbagai fasilitasnya dengan lebih adaptif.</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p4\"><span class=\"Apple-converted-space\">&nbsp;</span>BBC News. 10 August 2005. S Korean dies after games session.<span class=\"Apple-converted-space\">&nbsp; </span>HYPERLINK \"http://newsvote.bbc.co.uk/2/hi/ technology/ 4137782.stm\" <span class=\"s1\">http://newsvote.bbc.co.uk/2/hi/ technology/ 4137782.stm</span></p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p3\">&nbsp;</p>', NULL, NULL, 'mencegah-kecanduan-internet-melalui-rebt-rational-emotive-behavioral-therapy', NULL, NULL, 'Published', 4, NULL, '2018-11-17 22:09:08', '2018-12-10 09:29:47', NULL),
(13, 'MENJADIKAN PERSAHABATAN BERARTI', '2018-11-18 19:09:00', '1544459141.jpg', '<p class=\"p1\">&ldquo;<em>Virtuous friendship is one of the most glorious attainments one can achieve</em>.&rdquo;<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Demikian preposisi Aristoteles (filsuf besar Yunani klasik, 384-322 SM) dalam bukunya <em>Nicomachen Ethics</em>.<span class=\"s1\"><span class=\"Apple-converted-space\">&nbsp;</span></span></p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\">&ldquo;Persahabatan yang luhur merupakan pencapaian paling mulia yang dapat kita hasilkan&rdquo;<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\">Saat menghadapi permasalahan dalam kehidupan, <em>social support</em> atau dukungan sosial sangatlah dibutuhkan. Persahabatan<span class=\"Apple-converted-space\">&nbsp; </span>merupakan <em>therapeutic factor</em> alias faktor yang dapat menyembuhkan. <em>Curhat</em> (curahan hati) sebenarnya bukan sekedar keluh kesah melainkan bentuk dukungan sosial antar sahabat yang bersifat <em>therapeutic </em>(menyembuhkan).</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong><em>Friendship of virtue</em></strong></p>\r\n<p class=\"p3\">Seorang sahabat yang balik menghibur teman yang dirundung kebingungan. Persahabatan mereka termasuk dalam <em>friendship of virtue</em> menurut Aristoteles.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p3\">Ada dua jenis persahabatan yang perlu diketahui. Pertama,<span class=\"Apple-converted-space\">&nbsp; </span><em>Friendship of utility</em> atau persahabatan atas dasar kebutuhan salah satu atau kedua pihak. Aristoteles memberi contoh persahabatan dalam konteks dagang. Barangkali sekarang ini persahabatan semacam ini dapat disebut persahabatan bisnis.</p>\r\n<p class=\"p3\">Kedua, Aristoteles menggunakan istilah <em>friendship of pleasure</em> (persahabatan atas dasar kesenangan badaniah). Aristoteles berpendapat, persahabatan atas dasar kebajikan justru merupakan bentuk persahabatan yang bernilai paling tinggi. Ia menyebutnya persahabatan &ldquo;...yang paling lengkap, antara orang-orang yang baik dan yang sama baiknya di dalam mewujudkan kebajikan.&rdquo; Persahabatan semacam ini tahan lama dan sulit untuk ditemukan. Karena jarang sekali kita menemukan orang bajik dan persahabatan atas dasar kebajikan itu sulit dicapai.</p>\r\n<p class=\"p3\">Di sinilah letak perbedaan antara frienship of value dan friendship of utility serta friendship of pleasure: Lebih banyak waktu dan perhatian bagi sahabat daripada bagi disi sendiri. Dengan kata lain, friendship of value itu serupa dengan <em>altruistic friendship</em> (lebih mengutamakan sahabat daripada diri sendiri).</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Akhir kata...</strong></p>\r\n<p class=\"p3\">Pada saat mengalami masalah-masalah dalam kehidupan kita sungguh membutuhkan dukungan sosial yang sering diperoleh dari persahabatan yang tulus dan bajik. Air mata seorang sahabat &ndash; sebagai ekspresi empati &ndash; justu lebih terapotik (menyembuhkan) daripada nasihat psikoterapis.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p4\">FELIX LENGKONG, Ph.D.</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p4\">Psikolog klinik yang berpraktik pribadi dan mengajar di Universitas Atma Jaya Jakarta dan Universitas Gunadarma, Jakarta.</p>', NULL, NULL, 'menjadikan-persahabatan-berarti', NULL, NULL, 'Published', 4, NULL, '2018-11-17 22:10:04', '2018-12-10 09:32:47', NULL),
(15, 'Perkawinan dan Gerak-Gerik Cinta', '2018-11-18 19:10:00', '1544458904.jpg', '<p class=\"p1\"><em>Pernah nggak sih, kita merasa pasangan kita memiliki gerak-gerik cinta yang sedikit berbeda dari biasanya ?</em></p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><span class=\"Apple-converted-space\">&nbsp;</span>Menjadi sesuatu yang wajar ketika kita merasakan ada sebuah hal yang berbeda dari pasangan dan membuat kita menjadi ingin tahu, apa sih yang sebenarnya terjadi. Nah, untuk membantu menemukan jawabannya, berikut ini bentuk-bentuk dasar gerak-gerik pencinta yang diambil dari buku John Lee <em>Colours of Love: An Exploration of the Ways of Loving </em>(1973; 1988).</p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p class=\"p5\"><span class=\"s1\"><strong>Pencinta erotik </strong><em>(</em></span><em>Eros &ndash; Loving an ideal person</em>)</p>\r\n<p class=\"p6\">Eros (bahasa Yunani) &ndash; yang bermakna birahi &ndash; berarti cinta akan keindahan. Biasanya pencinta erotik tertarik dengan kekasihnya berdasarkan daya tarik fisik dan jatuh cinta pada pandangan pertama. Senang memanggil kekasihnya &ldquo;Cantik&rdquo; atau &ldquo;Tampan,&rdquo; ia memandang perkawinan sebagai dunia fantasi. Ia selalu bergairah dan kehidupan sexual perkawinan menjadi sangat intens. Masalahnya, ia cepat bosan karena secara alamiah daya tarik fisik menurun.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p6\">Sikap dan perilaku pencinta erotik ini tampak di film <em>HYPERLINK \"http://en.wikipedia.org/wiki/Pretty_Woman\" \\o \"Pretty Woman\"</em><span class=\"s2\"><em>Pretty Woman</em></span> (1990), <em>HYPERLINK \"http://en.wikipedia.org/wiki/Titanic_(1997_film)\" \\o \"Titanic (1997 film)\"</em><span class=\"s2\"><em>Titanic</em></span> (1997), dan <em>HYPERLINK \"http://en.wikipedia.org/wiki/Romeo_and_Juliet\" \\o \"Romeo and Juliet\"</em><span class=\"s2\"><em>Romeo and Juliet</em></span> (2010).</p>\r\n<p class=\"p7\">&nbsp;</p>\r\n<p class=\"p5\"><span class=\"s1\"><strong>Pemain cinta </strong><em>(</em></span><em>Ludos &ndash; Love as a game)</em></p>\r\n<p class=\"p6\"><em>&lsquo;Patah tumbuh hilang berganti&rsquo; </em>atau <em>&lsquo;Mati satu tumbuh seribu&rsquo;.<span class=\"Apple-converted-space\">&nbsp;</span></em></p>\r\n<p class=\"p6\">Pernah mendengar peribahasa tersebut ? <em>Yup!</em>, orang yang memiliki prinsip ini, mereka memiliki tipe yang disebut <em>ludos</em> (Yunani: permainan). Bagi orang yang tergolong dalam pemain cinta, cinta itu bagaikan permainan yang mesti dimenangkan. Karena itu ia disebut penakluk. Cinta bukan sesuatu yang serius, hanya <em>fun</em> alias kegiatan menyenangkan. Ia lebih tertarik pada kuantitas bukan kualitas. Orang dengan tipe ini cenderung doyan selingkuh dan bisa kecanduan mencari taklukan baru. Jadi, jangan harap melihat pemain cinta berpatah hati.</p>\r\n<p class=\"p6\">Contohnya dapat dilihat di film HYPERLINK \"http://en.wikipedia.org/wiki/Kids_(film)\" \\o \"Kids (film)\"<span class=\"s2\"><em>Kids</em></span> (1995), HYPERLINK \"http://en.wikipedia.org/wiki/Cruel_Intentions\" \\o \"Cruel Intentions\"<span class=\"s2\"><em>Cruel Intentions</em></span> (1999), dan HYPERLINK \"http://en.wikipedia.org/wiki/Dangerous_Liaisons\" \\o \"Dangerous Liaisons\"<span class=\"s2\"><em>Dangerous Liaisons</em></span> (2012).</p>\r\n<p class=\"p8\">&nbsp;</p>\r\n<p class=\"p9\"><span class=\"s1\"><strong>Sahabat yang kekasih </strong><em>(</em></span><em>Storge &ndash; Love as friendship)</em></p>\r\n<p class=\"p6\">&ldquo;<em>Witing tresno jalaran soko kulino</em>&rdquo; berarti &ldquo;Cinta tumbuh karena terbiasa.&rdquo;</p>\r\n<p class=\"p6\"><span class=\"Apple-converted-space\">&nbsp;</span>Begini kiranya deskripsi singkat pencinta <em>storge</em> (Yunani: afeksi, perasaan kasih). Cinta berawal dari persahabatan. Ia memilih kekasih berdasarkan kesamaan sikap dan latar belakang. Komitmen terhadap perkawinan sangat tinggi. Ia tidak suka selingkuh. Memiliki rasa hormat dan pengertian yang tinggi terhadap pasangan. Ia memandang anak serta perkawinan sebagai pewujudan ikatan kasih. Sex bukan urusan penting.</p>\r\n<p class=\"p6\">Masalahnya, pencinta &ndash; seperti di film <em>When Harry Met Sally </em>(1989)<em>, Love and Basketball </em>(2000)<em>, </em>dan <em>Zack and Miri Make a Porno</em> (2008) &ndash; ini kurang bergairah secara seksual.</p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p class=\"p9\"><span class=\"s1\"><strong>Pencinta azas manfaat</strong></span> <span class=\"s1\"><em>(</em></span><em>Pragma (Ludos + Storge) &ndash; Realistic and practical love)</em></p>\r\n<p class=\"p6\"><em>Pragma</em> (Yunani) berarti perbuatan atau tindakan. Saya menamainya pencinta asas manfaat. Pencinta yang rasional dan realistik ini menganggap perkawinan sebagai kesempatan untuk mewujudkan kebutuhan pribadi. Ia suka membuat daftar calon kekasih sebelum memilih satu berdasarkan kriteria tertentu &ndash; sebagai contoh &ndash; <em>bibit</em> (Jawa: rupa; asal-usul, keturunan), <em>bebet</em> (Jawa: keluarga, lingkungan pergaulan) dan <em>bobot</em> (Jawa: nilai pribadi seperti kepribadian, pendidikan, pekerjaan, ekonomi/finansial).<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p10\">Ia tampak dalam film <em>Ordinary People</em> (1980) dan <em>Pride and Prejudice </em>(2005).</p>\r\n<p class=\"p8\">&nbsp;</p>\r\n<p class=\"p9\"><span class=\"s1\"><strong>Kekasih pencemburu </strong><em>(</em></span><em>Mania (Eros + Ludos) &ndash; Obsessive love)</em></p>\r\n<p class=\"p6\">Pasangan kalian termasuk orang yang cemburuan ? Nah, bisa jadi, ia masuk dalam tipe <em>maniac lover</em>!. <em>Mania</em> (Yunani: semangat) membuatnya terobsesi akan cinta. Ia bisa sangat mencintai, tapi bisa juga sangat membenci. Karena curiga, gelagat sekecil apa pun bisa mengubah cinta menjadi benci. Ia sering melebih-lebihkan tentang kekasihnya seperti &ldquo;sangat cantik&rdquo;, &ldquo;sangat baik&rdquo;, &ldquo;tiada duanya.&rdquo; Bagi dia, cinta dan kekasih adalah harga diri.</p>\r\n<p class=\"p6\">Ia ada dalam film <em>Taxi Driver </em>(1976), <em>Endless Love </em>(1981), dan <em>Fatal Attraction </em>(1987).</p>\r\n<p class=\"p7\">&nbsp;</p>\r\n<p class=\"p9\"><span class=\"s1\"><strong>Pencinta altruistik </strong><em>(</em></span><em>Agape (Eros + Storge) &ndash; Selfless love)</em></p>\r\n<p class=\"p6\">Pada tipe pencinta altruistik, ia melihat pasangan sebagai kekasih sekaligus berkah yang mesti dipelihara dan dirawat. Begitu terpesona sehingga ia rela berkorban demi cinta. Itulah dia yang disebut <em>agape</em> (Yunani: [1] termangu, [2] cinta altruistik).<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p6\">Cinta bukan lagi ketertarikan fisik melainkan spiritual. Ia mengasihi dan tidak mengharapkan dikasihi. Pencinta yang rela berkurban ini biasa disebut orang baik. Masalahnya, ia kadang dipandang terlalu baik, sehingga pasangannya mudah merasa bersalah.</p>\r\n<p class=\"p6\">Sikap dan perilaku pencinta alturistik ini tampak di film <em>Somewhere in Time</em> (1980), <em>Forrest Gump</em> (1994), dan <em>The Gift of the Magi</em> (2010).</p>\r\n<p class=\"p4\">&nbsp;</p>\r\n<p class=\"p11\"><strong>Kombinasi gerak-gerik cinta</strong></p>\r\n<p class=\"p6\">Setelah kita mengenal enam tipe pecinta diatas, ada dua hal yang perlu diperhatikan. <em>Pertama</em>, enam gaya atau gerak-gerik di atas hanyalah bentuk dasar. Pada setiap orang memiliki kombinasi dari beberapa gaya. Namun, dari kombinasi tersebut ada satu gaya yang sangat dominan dalam sikap dan perilakunya. <em>Kedua</em>, gerak-gerik cinta itu merupakan suatu proses yang secara ideal berpuncak pada pencinta altruistik (agape) di masa tua.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p12\">&nbsp;</p>\r\n<p class=\"p13\"><em>Jadi, mari tetap mencintai pasangan kita dengan tulus, ya!</em></p>', NULL, NULL, 'perkawinan-dan-gerak-gerik-cinta', NULL, NULL, 'Published', 4, NULL, '2018-11-17 22:11:01', '2018-12-10 09:32:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `selfenhancements`
--

CREATE TABLE `selfenhancements` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_datetime` datetime NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Published','Draft','InActive','Scheduled') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `selfenhancements`
--

INSERT INTO `selfenhancements` (`id`, `name`, `publish_datetime`, `featured_image`, `content`, `meta_title`, `cannonical_link`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 'DEPRESI? COBA LIHAT DIRIMU YANG DULU', '0000-00-00 00:00:00', '1544455706.jpg', '<p class=\"p1\"><em>&ldquo;Masa kanak-kanak mempengaruhi masa dewasa seperti pagi mengindikasikan suasana siang</em>&rdquo; <strong>John Milton</strong> (sastrawan Inggris, 1608-1674)</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Persaingan antar-anak</strong></p>\r\n<p class=\"p4\">Menelaah sejarah awal dalam keluarga, salah satunya persaingan anak dalam keluarga (<em>sibling rivalry</em>) dapat melihat gambaran diri masa kini. Contohnya menafsirkan pengalaman adik-adik (yang lebih &lsquo;dimanja&rsquo; dan lebih menarik) sebagai kenyataan bahwa diri lebih kurang &lsquo;berharga&rsquo;, bahkan kurang disayang orangtua, merasa kurang menarik. Lalu, merasa cemburu akan &lsquo;cinta&rsquo; yang dialami adik-adik. Kemudian diri sendiri merasa bersalah karena mencemburui adik-adik tanpa dasar yang jelas. Menganggap hidup kurang dicintai dan tidak bahagia, merasa ditolak dan gagal. Sejarah awal hidup dalam keluarga melahirkan kecenderungan <em>self-defeating</em> (diri yang selalu kalah).</p>\r\n<p class=\"p4\">Pelukisan <strong>Robert Spitzer</strong> dkk (1983) dalam <em>Psychopathology: A Case Book</em>, mengatakan Anda mengalami <em>hopelessness</em> dan <em>helplessness</em>. Hidup tanpa asa dan daya.</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Masalah depresi</strong></p>\r\n<p class=\"p4\">Sesuai rincian simtom dalam <em>Diagnostic Criteria from DSM-IV</em> (DSM=<em>diagnostic and statistical manual</em>), kemungkinan diri mengalami <em>adjustment disorder with depressed mood</em> (masalah penyesuaian diri dengan keadaan yang berakibat depresi). Sejarah hidup banyak mengindikasikan gejala-gejala tersebut.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p5\"><span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p4\"><strong>Theodore Millon</strong> dkk (1996) dalam <em>Disorders of Personality: DSM-IV(diagnostic and statistical manual of mental disorders-IV) and Beyond</em> melukiskan gejala-gejala seperti yang tersebut sebagai indikasi masalah kepribadian depresif. Kesulitan menyesuaikan diri dengan keadaan lingkungan bukan hanya bersifat sementara melainkan menetap. Kesulitan itu telah menjadi bagian integral kedirian.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong><em>Cognitive triad</em></strong></p>\r\n<p class=\"p4\">Menurut <strong>Aaron Beck</strong> dkk (1979) dalam <em>Cognitive Therapy of Depression</em>, pandangan dasar negatif yang berciri <em>self-defeating</em> (meng-kalah-kan diri) akan muncul dalam bentuk <em>negative schemas</em> (cara-cara berpikir negatif) dalam tiga aspek: citra diri, relasi antarpribadi, dan masa depan.</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p4\"><em>Pertama</em>, kendati ada banyak hal positif dalam diri, selalu merasa <em>helpless</em> (tanpa daya).</p>\r\n<p class=\"p4\"><em>Kedua, </em>lebih mudah mencermati sisi negatif tentang relasi dengan orang lain. Untuk menghindar dari problematika pergaulan, menyendiri dan mengurangi kegiatan sosial menjadi pilihan.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p4\"><em>Ketiga</em>, cara pandang negatif itu berpengaruh pada cara mencermati masa depan. Dunia seakan runtuh,<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong>&lsquo;Menghidupkan&rsquo; harapan</strong></p>\r\n<p class=\"p4\">Konselor yang beraliran kognitif akan membantu menghilangkan pandangan dasar negatif<span class=\"Apple-converted-space\">&nbsp; </span>yang berciri meng-kalah-kan diri, akan dibantu mengatasi masalah tanpa asa dan daya.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p4\">Salah satu strategi ampuh adalah strategi A-B-C dalam <em>The New Handbook of Cognitive Therapy Techniques (</em><strong>Rian McMullin,</strong> 2000).<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p4\">Pertama<span class=\"Apple-converted-space\">&nbsp; </span>mengganti faktor B (keyakinan) negatif menjadi B (keyakinan) positif seperti <strong><em>self-enhancing</em> (memiliki kiat-kiat mengembangkan diri).</strong></p>\r\n<p class=\"p4\"><span class=\"Apple-converted-space\">&nbsp;</span>Dengan pandangan dasar positif, Anda akan mampu mengatasi berbagai faktor A (permasalahan) seperti <strong>lingkungan hidup yang sulit</strong>.</p>\r\n<p class=\"p4\"><span class=\"Apple-converted-space\">&nbsp;</span>Faktor C ( Akibat) Anda akan <strong>mengalami suasana yang lebih menyenangkan.<span class=\"Apple-converted-space\">&nbsp;</span></strong></p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p class=\"p4\">Ringan-beratnya permasalahan kita berawal dari positif-negatifnya pandangan dasar kita tentang citra diri sendiri. Jika kita mempunyai pandangan diri positif, kita akan memiliki cara berpikir positif. Alhasil, masalah akan dihadapi dengan cara yang lebih positif.</p>\r\n<p class=\"p5\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p6\"><strong><span class=\"Apple-converted-space\">&nbsp; &nbsp; &nbsp; </span>Felix Lengkong, M.A., Ph.D</strong>.</p>', NULL, NULL, 'depresi-coba-lihat-dirimu-yang-dulu', NULL, NULL, 'Published', 1, NULL, '2018-11-17 15:07:06', '2018-12-10 08:28:26', NULL),
(9, 'KUNCINYA ADALAH PERHATIAN', '0000-00-00 00:00:00', '1544456668.jpg', '<p class=\"p1\">Beberapa tahun lalu saya mengajar Psikologi Konseling di salah satu kampus ternama. Pada pertemuan pertama dan kedua, hampir setengah kelas kosong. Padahal para mahasiswa &ndash; umumnya laki-laki &ndash; sudah berada tidak jauh dari ruang kelas. Mereka masih bercengkerama dan merokok. Setelah sekitar 15 menit kelas mulai barulah mereka masuk bergerombol.</p>\r\n<p class=\"p1\">Saya tidak ciut hati atau marah menghadapi gejala tersebut. Malah saya tersenyum dan berkata dalam hati: &ldquo;Kasihan, mereka membutuhkan perhatian&hellip;&rdquo;<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Lalu pada pertemuan ketiga saya datang setengah jam lebih cepat dan bergabung dengan mereka serta ikut bercengkerama. Kebetulan, topik pembicaraan adalah sepak bola yang juga merupakan kesenangan saya. Dengan mudah saya mendekati salah satu mahasiswa &ndash; berbadan besar dan tampak agak sangar &ndash; dan saya memberi dia tugas: &ldquo;Ucok, setiap ada kelas, tolong ajak teman-teman masuk sebelum saya datang, ya.&rdquo;</p>\r\n<p class=\"p1\">&ldquo;Iya, Pak,&rdquo; jawabnya, &ldquo;Tapi, nama saya bukan Ucok, Pak.&rdquo; Nah, di sini saya menunjukkan perhatian saya baginya. &ldquo;Saya tahu itu bukan namamu. Tapi, begitulah saya memanggil mahasiswa kesayangan saya. Jadi, mulai sekarang, saya memanggilmu Ucok, boleh kah?&rdquo; &ldquo;Dengan senang hati, Pak,&rdquo; jawabnya dengan wajah sumringah.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Perhatian dan regulasi diri</strong></p>\r\n<p class=\"p1\">Pertama-tama kita mesti menyadari bahwa bagi anak dan remaja perilaku itu memiliki arti. Tampak sekilas, para mahasiswa saya itu mungkin bandel dan sok jagoan. Tapi, barangkali sebelumnya mereka merasa bahwa dosen tidak memperhatikan atau kurang menganggap mereka. Namun, mereka tidak tahu cara yang lebih efektif untuk mencari perhatian, selain datang terlambat ke dalam kelas.</p>\r\n<p class=\"p1\">Dalam keadaan kurang diakui, kurang dianggap, anak-anak akan mengalami bahwa &ldquo;kemampuan regulasi diri&rdquo; (<em>self regulatory abilities</em>) mereka menjadi rapuh bahkan hancur. Regulasi diri adalah kemampuan untuk mengontrol hasrat atau niat, baik untuk menghentikan suatu tindakan atau prilaku (jika dibutuhkan), kendati anak sebenarnya ingin meneruskannya, maupun untuk mulai melakukan suatu tindakan (jika dibutuhkan) kendati anak sebenarnya tidak mau melakukannya.</p>\r\n<p class=\"p1\">Contoh, menghadapi perlakuan kurang perhatian dari dosen, mahasiswa merajuk dengan masuk kelas terlambat. Nah, saya berusaha memberi perhatian dengan bergaul bersama mereka sebagai sesama penggemar sepak bola. Setelah diberi perhatian, regulasi diri mereka membaik. Mereka berhasrat untuk masuk kelas tepat pada waktunya (kendati sebenarnya lebih menyenangkan bagi mereka untuk terus bercengkerama di luar kelas). Demikin juga mereka berhasrat menghentikan tindakan pembangkangan (kendati mereka sebenarnya mereka belum mau masuk kelas).</p>\r\n<p class=\"p1\">Jadi, perhatian penuh kasih membuat regulasi diri mereka menjadi berfungsi baik.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Kedekatan emosional dan konep diri</strong></p>\r\n<p class=\"p1\">Perhatian &ndash; yang lebih cocok diterjemahkan <em>care</em> &ndash; mempunyai hubungan dengan suatu teori lain yang disebut <em>attachment</em> (kedekatan secara emosional).</p>\r\n<p class=\"p1\"><em>Care</em> (perhatian yang penuh kasih sayang) di dalam mengasuh atau mendidik anak akan menghasilkan anak dan remaja yang memiliki <em>secure attachment</em>. Dengan <em>secure attachment</em> anak merasa nyaman dan tentram dalam berelasi dengan orang lain, <em>attachment</em> merupakan inti dari proses pengasuhan anak dan remaja.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Anak yang dibesarkan dengan <em>care</em> and <em>secure attachment</em> akan mengembangkan konsep diri yang positif. Konsep diri adalah rangkaian keyakinan tentang diri seperti &ldquo;Saya adalah anak yang pintar, saya adalah anak yang disayang, saya adalah anak yang cantik.&rdquo; Konsep diri yang kurang lebih positif akan menghasilkan rasa percaya diri anak.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\"><strong>Perhatian dan kemandirian anak</strong></p>\r\n<p class=\"p1\">Anak yang dibesarkan dengan perhatian alias <em>care</em> akan mengembangkan konsep diri yang positif serta memilik rasa percaya diri. Selanjutnya, rasa percaya diri ini membuat ia merasa nyaman di dalam relasi antarpribadi.</p>\r\n<p class=\"p1\">Jika ia mengalami proses pertumbuhan yang teratur seperti ini, ia tidak akan menuntut perhatian secara berlebihan dan secara keliru melalui perilaku-perilaku yang tidak sesuai.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Bahkan, setelah kebutuhan akan perhatian terpuaskan, anak mulai berproses mencari <em>secure base</em>. Yaitu, Mmenyadari bahwa orangtua atau pengasuh memberi dia perhatian yang cukup, dia mulai menemukan zona nyaman, di mana ia menjadi tidak tergantung pada orangtua atau pengasuh. Anak menjadi mandiri. Dia merasa nyaman untuk mengeksplorasi wilayah baru. Yaitu, pergaulan dengan sesama teman, tak tergantung pada orangtua atau pengasuh. Ia mulai membentuk <em>peer group</em> (kelompok teman sebaya) dengan pola prilaku yang baik dan tidak saling mengeksploitasi.</p>\r\n<p class=\"p3\"><strong>Jadi...</strong></p>\r\n<p class=\"p1\">Memberi perhatian bukan berarti menghukum prilaku anak. Atau membiarkan prilaku itu berlangsung terus. Memberi perhatian itu berarti kita menginterpretasi kembali prilaku anak dan menanggapinya secara positif. Dengan demikian, kondisi yang sifat konfrontatif dan sikap pemberontakan anak pelan-pelan berubah menjadi tenang. Anak belajar memahami perasaannya lebih baik dan mengembangkan cara-cara yang lebih dewasa guna mengelola emosinya. Hasil dari memberi perhatian itu sederhana saja: Perilaku negatif di dalam mencari perhatian itu pelan-pelan surut.</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p4\">&nbsp;</p>', NULL, NULL, 'kuncinya-adalah-perhatian', NULL, NULL, 'Published', 4, NULL, '2018-11-17 15:07:57', '2018-12-10 08:57:30', NULL),
(10, 'Membentuk Anak Percaya Diri', '0000-00-00 00:00:00', '1544456695.jpg', '<p class=\"p1\">&ldquo;Kepercayaan diri itu tidak berdiri sendiri. Kepercayaan diri berada dalam satu rangkaian pembentukan kepribadian bagi anak&rdquo;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\"><strong>Rangkaian pembentukan kepribadian</strong></p>\r\n<p class=\"p1\">Rangkaian itu diawali dengan gaya pengasuhan anak yang kita terapkan. Gaya pengasuhan yang tepat (istilah teknisnya, adaptif) akan menghasilkan secure attachment pada anak. <em>Secure attachment </em>adalah perasaan nyaman saat berinteraksi dengan pengasuh (orangtua atau pengasuh anak) serta orang lain. Setelah perasaan nyaman ini terbentuk, anak akan merasakan bahwa ia disayang, diperhatikan, dan dianggap. Perasaan ini akan menghasilkan konsep diri (<em>self concept</em>) yang positif, di mana anak merasa diterima oleh oleh lingkungannya. Rasa diterima ini akan menghasilkan rasa percaya diri yang biasa disebut <em>self confidence</em> di lingkungan pendidikan.</p>\r\n<p class=\"p1\">Kendati demikian, self confidence ini tidak boleh kebablasan yang selanjutnya akan mengakibatkan anak menjadi &ldquo;tidak tahu diri&rdquo; alias semaunya. Artinya, ego menggelembung sehingga anak menjadi tidak tahu aturan. Itulah sebabnya &ndash; kata Sigmund Freud (1965-1939) penemu psikoanalisis &ndash; anak mesti dihadapkan pada <em>superego</em>.<span class=\"Apple-converted-space\">&nbsp; </span>Superego sebenarnya merupakan rangkaian pembelajaran tentang moralitas, tatanan sosial, aturan-aturan dari keluarga dan lingkungan pendidikan sosial. Interaksi antara keinginan-keinginan yang egotistik dan superego, membuat anak membentuk fasilitas internal yang disebut self control dan self regulation. Superego membantu anak mengontrol keinginan diri yang membuatnya bisa kebablasan. Dengan demikian ia membentuk di dalam dirinya <em>self control</em> (kontrol diri). Adanya berbagai self control yang berhadapan dengan berbagai keinginan internal dan stimulasi eksternal membuat anak membangun fasilitas serupa yang disebut <em>self-regulation</em> atau pengaturan diri.</p>\r\n<p class=\"p1\">Regulasi diri ini sangat penting bagi terbentuknya gaya kepribadian anak selanjutnya. Kendati demikian kali ini saya ingin membahas tentang cara-cara pembentukan rasa percaya diri.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\"><strong>Jangan melarang dan jangan memerintah<span class=\"Apple-converted-space\">&nbsp;</span></strong></p>\r\n<p class=\"p1\">Pengalaman saya menjadikan anak-anak saya sebagai kelinci percobaan tentang pengasuhan anak serta pengalaman saya mendidik dua ekor anjing di rumah adalah bahwa larangan dan perintah tidak akan menghasilkan anak yang percaya diri. Kita harus mengusahakan agar anak itu merasa bahwa ia disayang, diperhatikan, serta dianggap. Larangan atau perintah itu merupakan cara <em>short cut</em>.</p>\r\n<p class=\"p1\">Kata dosen saya almarhum Eugene Flameygh, <em>short cut</em> alias jalan pintas itu adalah cara setan. Terlihat gampang dan menghasilkan dalam jangka pendek, namun dalam jangka panjang, hasilnya negatif.</p>\r\n<p class=\"p1\">Cara yang sulit dalam jangka pendek namun menghasilkan dalam jangka panjang, adalah menjelaskan dan mengajak anak berpikir tentang masalah yang dihadapi. Saya teringat pengalaman dengan anak perempuan saya (saat itu 4 tahun). Ia ingin memiliki satu mainan. Saat kami sedang mengitari <em>department store</em> (toko besar) di dekat rumah, ia melihat dan mengambil mainan yang dia cari. Ia minta saya membelinya. Rupanya, harganya dua kali lipat dari uang yang saya miliki di dompet saat itu. Saya mengeluarkan dompet saya dari saku dan menunjukkan uang saya kepadanya sambil berkata: &ldquo;Saya senang membelikan mainan itu bagimu, tapi uang kita tidak cukup.&rdquo; Lalu kami berjalan terus. Ia berjalan di belakang saya. Ia tidak marah, namun saya melihat air matanya menetes. Itu ekspresi bahwa ia mengerti saya namun keinginannya untuk memiliki mainan itu besar sekali. Kemudian ia menemukan mainan serupa, tapi harganya sesuai dengan uang di dompet saya. Ia bertanya: &ldquo;Ini dapat dibeli?&rdquo; Melihat harganya terjangkau, saya langsung membayarnya.</p>\r\n<p class=\"p1\">Pengalaman tersebut mengajarkan pada anak bahwa tidak semua keinginannya dapat dipenuhi karena keadaan tidak memungkinkan. Tapi, ia juga sadar bahwa ia disayang dan diperhatikan. Sejak saat itu ia belajar untuk bernegosiasi jika ia ingin mendapatkan sesuatu. Atau, ia ikut menabung uang. Jika saya menawarkan untuk membeli sesuatu baginya, ia malah bertanya, &ldquo;Adakah uangnya? Jangan merepotkan.&rdquo;</p>\r\n<p class=\"p1\">Kita mesti menciptakan kondisi bahwa anak merasa disayang dan dianggap. Perasaan disayang dan dianggap itu akan menciptakan rasa nyaman di dalam dirinya. Filosofi pendidikan zaman dulu, mengatakan perasaan disayang itu akan mengakibatkan anak <em>ngelunjak</em>. Artinya, terus meminta dan merasa tidak cukup. Filosofi ini dibenarkan dengan pepatah ini &ldquo;<em>kasih hati mau jantung</em>&rdquo;.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\"><strong>Memberi contoh dan teladan</strong></p>\r\n<p class=\"p1\">Di dalam pendidikan ala kaum <em>behaviorist </em>(pengembangan prilaku) and <em>social learning theory</em> (teori pembelajaran sosial), teknik modeling itu sangat penting. Artinya, orangtua mesti memberikan contoh dan teladan dalam kaitan dengan rasa percaya diri.</p>\r\n<p class=\"p1\">Barangkali kita mesti mulai mendata cara-cara keliru yang dilakukan orangtua kita di masa lalu berdasarkan filosofi pendidikan anak masa lalu. Ingat, kita cenderung mengulang cara orangtua kita yang kita alami. Menjadi orangtua itu tidak ada sekolahnya. Kita belajar dari orangtua kita termasuk cara-cara yang salah. Kita membuat data tentang cara-cara yang salah itu dan kita berhenti meneruskannya kepada generasi berikut, khususnya cara-c ara yang disebut sebagai &ldquo;jalan pintas&rdquo;.</p>\r\n<p class=\"p1\">Kita berusaha menjadi &ldquo;cermin positif&rdquo; bagi anak. Anak yang percaya diri itu adalah anak yang mempunyai gambaran diri yang positif (<em>positive self image</em> atau <em>positive self concept</em>). Bagaimana dia mendapatkannya? Ia bercermin pada diri orangtua dan pada cara orangtua memperlakukannya. Orangtua yang menjadi &ldquo;cermin positif&rdquo; adalah orangtua yang hidup penuh kasih sayang satu sama lain dan orangtua yang menunjukkan kasih sayang bagi anaknya. Setelah konsep diri positif terbentuk dan setelah rasa percaya diri dibangun dalam diri anak, orangtua mesti membantu menjaganya pada saat anak mulai bersosialisasi orang anak-anak lain misalnya di sekolah. Guru atau konselor sekolah mesti ikut dilibatkan. Jika anak mengalami gangguan seperti <em>bullying</em> atau perundungan oleh teman sekolah, orangtua dan konselor sekolah mesti bekerja sama menghilangkan perundungan.</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p2\">&nbsp;</p>', NULL, NULL, 'membentuk-anak-percaya-diri', NULL, NULL, 'Published', 4, NULL, '2018-11-17 15:08:29', '2018-12-10 08:56:56', NULL),
(12, 'Mendalami arti kesepian dan hubungannya dengan pernikahan', '0000-00-00 00:00:00', '1544455808.jpg', '<p class=\"p1\">Pasti kita sering mendengar istilah &lsquo;perawan tua&rsquo; (<em>pratu</em>) atau &lsquo;bujang lapuk&rsquo; (<em>bupuk</em>). Istilah ini kerap membuat banyak orang, terutama perempuan, takut atau malu diri saat mendengar komentar semacam ini. Apalagi pada saat mereka telah melewati batas usia kawin.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Seperti kisah bujang lapuk bernama Lars dalam film <em>Lars and the Real Girl. </em>Lars adalah seorang pemuda yang sulit dalam membangun relasi dengan lingkungannya, baik dalam keluarga, rekan kerja, ataupun orang-orang se paroki.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Suatu hari Lars mengumumkan kepada kakaknya dan keluarga bahwa ia kedatangan tamu yang ia kenal melalui internet, bernama Bianca. Tamu itu adalah seorang perempuan calon misionaris yang berkursi roda. Keluarga kakaknya terkaget-kaget saat mengetahui bahwa Bianca hanyalah boneka berukuran manusia dewasa.</p>\r\n<p class=\"p1\">Kakaknya meyakinkan Lars bahwa Bianca mesti dibawa berobat ke dokter keluarga. Kebetulan dokter itu juga seorang psikolog. Mereka meragukan kesehatan mental Lars. Kecurigaan ini mereka sampaikan kepada dokter. Dokter memperlakukan Bianca layaknya manusia dengan maksud untuk mengecek dan mengobati gangguan mental Lars bersamaan dengan pemeriksaan terhadap Bianca.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p1\"><strong>Dua tipe kesepian</strong></p>\r\n<p class=\"p1\">Banyak sekali penyebab yang melatarbelakangi orang-orang untuk menunda pernikahannya seperti sibuk bekerja, masih ingin bebas menikmati masa muda, bahkan takut untuk memulai hubungan baru karena trauma masa lalu. Tapi, apakah kamu tau faktor yang paling sering dialami oleh sesorang? Yap! Sulit untuk bersosialisasi.</p>\r\n<p class=\"p1\">Dalam psikologi, hal ini biasa disebut <em>loneliness </em>(kesepian) atau <em>aloneless </em>(kesendirian). Apa sih yang bikin anak muda jaman sekarang takut sama kesepian? Karena banyak masyarakat yang suka berprasangka buruk terhadap orang lain yang masih <em>single</em> pada batas usia, yang katanya, udah pantas menikah.</p>\r\n<p class=\"p1\">Seseorang sosiolog, Robert Weiss mengatakan bahwa kesepian erat kaitannya dengan sulitnya membangun relasi sosial. Kesepian terbagi atas dua tipe: (a) kesepian yang terjadi akibat isolasi emosi/afeksi (<em>emotional loneliness</em>), dan (b) kesepian akibat isolasi sosial (<em>social loneliness</em>).<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\"><em>Emotional loneliness </em>ini dibarengi dengan <em>social loneliness</em>. Apa yang membuat orang dewasa mengalami 2 hal tersebut? Karena mereka mempunyai jaringan sosial yang terbatas sehingga mereka hanya mempunyai sedikit teman, sahabat, dan kenalan. Sempitnya jaringan sosial memberikan sedikit pilihan bagi orang dewasa untuk memilih.</p>\r\n<p class=\"p1\"><strong><em>Behavior repertoire</em></strong></p>\r\n<p class=\"p1\">Perkawinan dalam sepi terjadi karena pratu dan bupuk itu mempunyai behavioral repertoire atau perbendaharaan perilaku (perilaku yang menghambat orang dalam membangun relasi). Contohnya, pratu atau bupuk memiliki sifat yang sensitif, tidak suka dikritik, dibantah, atau ditolak. Ia tidak mau membangun<span class=\"Apple-converted-space\">&nbsp; </span>relasi karena takut dipermalukan dan diejek. Ketakutan semacam ini membuatnya tidak memiliki cara (perilaku) yang tepat guna menghadapi situasi semacam itu.</p>\r\n<p class=\"p1\">Demikian juga dengan bupuk seperti Lars yang menikmati kesendirian dan tidak suka bergaul. Perbendaharaan perilakunya tidak dilengkapi dengan perilaku-perilaku bergaul. Artinya, ia bukan <em>anak gaul</em> dan tentu tidak tahu bergaul. Pratu dan bupuk semacam ini juga menunjukkan emosi yang dingin, tanpa ekspresi. Remaja sekarang suka menyebutkan &ldquo;Ia terlalu <em>cool</em>.&rdquo; Akibatnya, ia menjadi kaku dan kikuk.</p>\r\n<p class=\"p1\">Tidak jauh berbeda dengan perilaku pratu dan bupuk yang aneh, esentrik, dan khas. Perilaku yang tidak biasa ini membuat ia sulit dipahami oleh pasangannya. Apalagi ia mempunyai kecemasan sosial yang begitu tinggi sehingga ia tidak percaya pada orang sekitarnya.</p>\r\n<p class=\"p1\">Lalu, ada pratu dan bupuk parno yang saya ibaratkan sebagai pencinta yang menipu. Ia berperilaku seakan-akan dia mencintai pasangannya sehingga pasangannya terkecoh. Padahal ia sebenarnya hanyalah mencintai dirinya sendiri dan ia tidak mempunyai gambaran yang jelas bagaimana sebenarnya cinta yang ia butuhkan. Akibatnya, ia menuntut cinta yang tidak masuk akal. Ia lalu menjadi pencuriga dan pencemburu terhadap pasangannya.</p>\r\n<p class=\"p1\"><em>So</em>, gimana? Lebih baik, sibukkan diri untuk memperbaiki kekurangan yang ada dalam diri sebelum menikah. Karena perkawinan itu sendiri tidak akan mampu menyembuhkannya.</p>\r\n<p class=\"p2\">&nbsp;</p>\r\n<p class=\"p3\">Every time I look at you I get a fierce desire to be lonesome.<br />Oscar Levant (1906-1972)</p>', NULL, NULL, 'mendalami-arti-kesepian-dan-hubungannya-dengan-pernikahan', NULL, NULL, 'Published', 1, NULL, '2018-11-17 15:09:37', '2018-12-10 08:30:08', NULL),
(14, 'Kecanduan Pornografi? berikut faktor penyebab dan cara mengatasinya..', '0000-00-00 00:00:00', '1544456722.jpg', '<p class=\"p1\"><em>&ldquo; Kecanduan membuat penderita gagal mengontrol perilaku dan tak mampu menghentikannya kendati telah tampak akibat-akibat negatif</em>. &ldquo;</p>\r\n<p class=\"p3\"><strong>Faktor-faktor penyebab</strong></p>\r\n<p class=\"p1\"><em>Faktor pertama</em> adalah<span class=\"Apple-converted-space\">&nbsp; </span>proses intrapsikis. Saat orang mengalami kecemasan, ia mengalami perubahan neurologis di dalam area limbik. Perubahan ini membuat dopamin meningkat. Peningkatan dopamin mengawali proses kecanduan. proses ini ditandai oleh munculnya kegairahan, kepuasan, dan fantasi. Selanjutnya perilaku menyengangkan ini akan diulangi. Pengulangan yang semakin intensif menjadikan penderita ketagihan.</p>\r\n<p class=\"p1\"><em>Faktor kedua</em>, Si pencandu<span class=\"Apple-converted-space\">&nbsp; </span>menjadi kompulsif dan diwujudkan dalam bentuk perilaku mencari produk-produk pornografi internet, membuat penderita tidak mampu lagi mengontrol diri. Lalu, penderita terjebak dalam perilaku-perilaku yang merusak diri seperti <em>begadang</em> di depan komputer dan tak mampu berhenti. Pencarian menjadi semakin intens seiring dengan kemunculan fantasi-fantasi sexual yang semakin liar. Fantasi ini juga membuat pencandu mencari berbagai modus perilaku sexual baru karena perilaku sebelumnya tidak memuaskan lagi.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\"><em>Faktor ketiga</em> adalah lingkungan psikologis. Belum ada kontrol sosial dan kontrol hukum dalam dunia maya. Variasi layanan pornografi internet menciptakan ketergantungan. Pengalaman unik<span class=\"Apple-converted-space\">&nbsp; </span>melalui internet membuat orang cepat tertarik, seperti relasi menjadi cepat akrab, gampang mengakses, relasi tanpa nama asli, identitas tersamarkan.</p>\r\n<p class=\"p3\"><strong>Dampak negatif<span class=\"Apple-converted-space\">&nbsp;</span></strong></p>\r\n<p class=\"p1\">Dalam proses peningkatan kecanduan, penderita terjerumus ke dalam kebutuhan yang tidak bisa ditolak. Ia membutuhkan kondisi sexual yang semakin lama semakin menggairahkan, memuaskan, dan memenuhi fantasi lebih liar. Penderita menjadi lupa waktu. Akibat lainnya adalah terganggunya fungsi-fungsi sosial dan kerja. Juga, ada konsekuensi psikis. Kecanduan mengakibatkan gangguan kecemasan. Semakin cemas semakin kecanduan. Sebaliknya, semakin kecanduan semakin cemas.</p>\r\n<p class=\"p1\">Bisa terjadi gangguan fisiologis seperti kerusakan otak. Seorang peneliti <em>Mark Kastleman</em> dan rekan-rekan menemukan fakta bahwa kecanduan pornografi dapat mengakibatkan kerusakan otak.</p>\r\n<p class=\"p3\"><strong>Terganggunya Hubungan dengan pasangan tetap<span class=\"Apple-converted-space\">&nbsp;</span></strong></p>\r\n<p class=\"p1\">Masalahnya terletak pada fantasi liar yang hanya dipuaskan oleh berbagai produk situs porno. Hubungan dengan istri/suami telah menjadi rutinitas tanpa fantasi. Rutinitas berkembang menjadi keterpaksaan. Apalagi jika ada perasaan kesal terhadap pasangan. Hilanglah daya tarik yang sebanarnya menjadi pencetus kegairahan dan orgasme.</p>\r\n<p class=\"p1\">Sebenarnya fasilitas seksual seperti alat kelamin itu netral. Sex menjadi enak karena fantasi dan daya tarik. Rasa nikmat itu sendiri hanyalah kesan psikologis. Semakian liar fantasi itu semakin bergairah dan tertarik kita akan pasangan. Fantasi dan ketertarikan mencetus perubahan fisiologis di otak dan alat kelamin yang selanjunya menghasilkan rasa enak. Lalu, muncullah orgasme.</p>\r\n<p class=\"p3\"><strong>Cara mengatasi masalah <span class=\"Apple-converted-space\">&nbsp;</span></strong></p>\r\n<p class=\"p1\">Jika sudah mencapai tahap adiksi, maka penderita mesti dibantu melalui konseling/terapi, khususnya konseling keluarga atau konseling perkawinan. Kita mesti melihatnya dalam konteks &ldquo;teori sistem&rdquo;.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Ada dua masalah besar di sini: masalah kecanduan dan masalah relasi pasangan. Teori sistem melihat bahwa gangguan perilaku kecanduan pada suami mengganggu relasi suami-istri. Akibatnya terjadi ko-dependensi. Artinya, suami menderita kecanduan, sementara istri menjadi terganggu alias ko-dependen. Jadi, penanganan kecanduan mesti melibatkan istri yang menjadi ko-dependen. Bersamaan dengan penanganan kecanduan, masalah relasi suami-istri juga mesti diperhatikan.</p>\r\n<p class=\"p1\">Jika belum menjadi kecanduan, penderita dapat dibantu dengan modifikasi kognitif dan behavioral. Pengubahan cara berpikir dan pandangan hidup serta pengubahan perilaku.Dengan pengubahan itu penderita diharapkan mampu mengontrol dan menjauhkan diri dari sarana-sarana pornografi. Diharapkan ia akan mampu memperbaiki perilaku sexual dan relasi sexual dengan pasangan.</p>\r\n<p class=\"p1\">Jika klien itu seorang mahasiswa, maka sebaiknya orangtua turut dilibatkan. Mereka nanti berfungsi sebagai kontrol sosial. Sebaiknya, masalah ini jangan dijadikan rahasia pribadi. Sebab dalam konteks kecanduan, kontrol diri (<em>self control</em>) itu sudah ambruk. Pencandu mesti dibantu oleh kontrol sosial (<em>social control</em>) dan dukungan untuk sembuh (<em>social support</em>).</p>\r\n<p class=\"p3\"><strong>Terapi yang dibutuhkan</strong></p>\r\n<p class=\"p1\">Tergantung pada orientasi terapi atau orientasi konseling yang dipakai konselor atau psikolog. Hampir semua orientasi atau pendektan terapi itu telah membuktikan bahwa mereka masing-masing mampu membantu.</p>\r\n<p class=\"p1\">Ada macam-macam orientasi atau pendekatan terapi seperti psikoanalisis, psikodinamika, behavioral, kognitif, <em>client-centred</em>, gestalt, eksistensial, analisis transaksional, dan multi modal. Setiap terapi ini mempunyai starategi dan teknik-teknik sahih. Dalam hal teknik, setiap terapi bisa saling meminjam. Namun, berkaitan strategi, mereka mempunyai cara masing-masing.</p>\r\n<p class=\"p1\">Saya pribadi menerapkan terapi kognitif dengan tingkat keberhasilan yang tinggi. Penjelasan saya di atas banyak berlatar belakang terapi kognitif.</p>\r\n<p class=\"p3\"><strong>Tips untuk sembuh</strong></p>\r\n<p class=\"p1\">Untuk menyembuhkan unsur fisiologis, kita mengandalkan obat. Untuk menyembuhkan mental, dibutuhkan diri sendiri: tekad untuk sembuh. Di samping itu dibutuhkan juga dukungan dari pasangan atau orangtua<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Pasangan memahami pasangan yang bermasalah. Mereka saling menerima diri apa adanya. Lalu mereka saling menyembuhkan &lsquo;luka-luka batin&rsquo; (istilah dalam konseling pastoral/spiritual) berkaitan dengan relasi mereka. Mereka membarakan kembali api cinta di antara mereka.</p>\r\n<p class=\"p1\">Ada tiga macam cinta: <em>agape</em>, <em>eros</em>, dan <em>sextos</em>. Semakin lama relasi cinta, relasi itu berproses menjadi cinta <em>agape</em> (kasih sayang). Suami-istri berkasih sayang bagaikan saudara kakak beradik. Setelah mencapai suasana <em>agape</em>, pasangan kadang lupa bahwa mereka masih membutuhkan <em>eros</em>. Dengan <em>eros</em> mereka menumbuhkan gairah serta fantasi tentang satu sama lain. Mereka harus pintar mencari dan membuat suasana baru, baik suasana batin maupun suasana lingkungan. Selanjutnya, <em>eros</em> berproses dan digenapkan dengan <em>sextos</em> alias kegiatan seksual yang dipuncaki dengan persetubuhan yang berorgasme.</p>', NULL, NULL, 'kecanduan-pornografi-berikut-faktor-penyebab-dan-cara-mengatasinya', NULL, NULL, 'Published', 4, NULL, '2018-11-17 15:10:31', '2018-12-10 08:56:29', NULL),
(16, 'Hal yang menghantui dalam menjalani hubungan percintaan', '0000-00-00 00:00:00', '1544455871.jpg', '<p class=\"p1\"><em>&ldquo;Yang bikin saya kesal, istri saya selalu bercerita tentang teman-teman pria yang telah berhasil, jadi ini, jadi itu. Saya pura-pura mendengarkan tapi hati saya panas. Apalagi jika dia pulang diantar salah seorang pria yang &ndash; katanya &ndash; teman lama. Kadang-kadang perut saya terasa sembelit karena marah.&rdquo;</em></p>\r\n<p class=\"p3\">Pernah nggak sih kita merasakan hal-hal yang secara nggak disadari sebenarnya bisa mengganggu relasi kita dengan pasangan ? Memang, setiap pengalaman dalam berhubungan bisa mempengaruhi kualitas relasi kita dengan pasangan. Yuk, disimak !</p>\r\n<p class=\"p5\"><strong>Cinta terancam</strong></p>\r\n<p class=\"p3\">Hantu yang mengancam setiap relasi cinta, bisa terjadi baik &lsquo;cinta monyet&rsquo; di masa remaja maupun cinta perkawinan di masa dewasa. Hantu itu tiada lain dan tak bukan bernama kecemburuan. Eh tapi, sebenarnya apasih kecemburuan itu ? Kecemburuan merupakan reaksi emosi negatif terhadap ancaman &ndash; baik nyata maupun bayangan &ndash; yang menghantu di dalam setiap relasi cinta. Sederhananya, ada perasaan negatif yang kita rasakan ketika ada sesuatu yang mengganggu di hubungan kita dengan pasangan. Orang sering mengasosiasikan kecemburuan dengan para remaja yang bercinta monyet. Tetapi, rupanya perasaan cemburu itu tidak hanya terjadi di kalangan remaja saja <em>lho</em>, kendati sudah tua-tua, ada teman saya yang berusia 50-an tahun pun mengalami gejolak emosi yang serupa.</p>\r\n<p class=\"p3\">Banyak juga orang beranggapan, kecemburuan dalam kadar tertentu merupakan bukti cinta terhadap kekasih. Kecemburuan dengan kadar yang sedang membuat kita menyadari dan memeriksa kadar relasi cinta kita dan &ndash; akibatnya &ndash; menguatkan ikatan kasih di antara kekasih. Lagi pula kita tidak bisa memastikan jenis relasi sosial mana yang membuat pasangan kita jatuh dalam perselingkuhan. <em>Kok gitu </em>? Karena kecemburuan merupakan mekanisme internal di dalam diri setiap orang yang cemburu. Jadi, tergantung mekanisme internal psikolog pasangan kita.</p>\r\n<p class=\"p6\"><strong>Pasangan yang terancam cemburu</strong></p>\r\n<p class=\"p3\"><em>&ldquo;Lebih cenderung mana sih, wanita atau pria yang punya sifat cemburu ?&rdquo;</em></p>\r\n<p class=\"p3\">Oke, sebenarnya bukan relasi kita dengan orang lain yang membuat pasangan kita cemburu, melainkan kondisi internal pasangan kita.Orang sering mengatakan, wanita lebih mudah cemburu daripada pria. Alasan mereka, wanita lebih menggunakan perasaan daripada pria yang lebih kognitif (pikiran). Penelitian ahli menunjukkan, ini stereotip keliru. Kepribadian pencemburu dimiliki baik perempuan dan laki-laki dengan perbandingan berimbang. <em>See? Jadi terjawab ya pertanyaan diatas hehe</em></p>\r\n<p class=\"p3\">Tipe kepribadian bukan ditentukan oleh jenis kelamin melainkan beberapa faktor seperti gen (turunan), pola asuh di masa kecil, kondisi keluarga dan relasi saudara, serta peristiwa-peristiwa hidup yang berpengaruh. Jika di masa kecil kita mengalami pengasuhan yang membuat kita merasa terancam tidak disayang di masa kecil, kemungkinan besar kita akan menjadi pencemburu. Anak yang terlantar dan kekurangan kasih sayang biasanya akan menuntut cinta yang irasional (= tak masuk diakal) di masa dewasa.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p3\">Pengalaman negatif di masa kecil atau di masa lalu bisa juga menjadi seseorang pencuriga. Ia menjadi terlalu peka terhadap gelagat-gelagat yang sebenarnya biasa saja bagi orang normal. Pasangan yang pernah mengalami perceraian akan mudah curiga dan cemburu terhadap pasangan. Pria yang kurang beruntung dalam karir dibanding karir isterinya kadang mengalami rasa rendah diri dan dia mengkompensasi kondisi itu dengan cemburu terhadap istri.</p>\r\n<p class=\"p3\">Itulah sebabnya sebaiknya sebelum melangkah ke mahligai perkawinan, kita sebaiknya berkonsultasi ke ahli, guna saling mempelajari sejarah hidup masing-masing menurut perspektif psikologi.</p>\r\n<p class=\"p5\"><strong>Beberapa prinsip mengolah relasi cinta</strong></p>\r\n<p class=\"p3\"><em>Cinta tanpa cemburu membutuhkan waktu untuk bertumbuh</em>.</p>\r\n<p class=\"p3\"><em>Yup, </em>setiap perjalanan cinta mensyaratkan kesabaran sekaligus usaha. Cinta tanpa cemburu direalisasikan melalui kesamaan minat dan keyakinan, serta melalui banyak pengalaman jatuh-bangun.</p>\r\n<p class=\"p3\">Cinta penuh kasih sayang berbeda dengan cinta erotis yang membutuhkan keuletan fisik, alkohol, serta Viagra guna merangsang emosi sensual yang dianggap sebagai ekspresi cinta. Sebaiknya, sebelum membuat komitmen perkawinan, kita sudah berusaha mengenal, menyukai, serta mencintai pasangan itu dengan kasih sejati.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p3\">Jangan keliru, saat cintah penuh kasih sayang itu tercapai, maka cinta itu akan bertahan begitu saja. Cinta sejati perlu terus diupayakan. Jika kecemburuan masih terus menghantu relasi cinta, sebaiknya kita berkonsultasi dengan konselor/psikolog, terutama jika sudah terjadi KDRT (kekerasan dalam rumah tangga).</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p7\">&nbsp;</p>', NULL, NULL, 'hal-yang-menghantui-dalam-menjalani-hubungan-percintaan', NULL, NULL, 'Published', 1, NULL, '2018-11-17 15:11:32', '2018-12-10 08:31:11', NULL),
(17, 'PERSELINGKUHAN TIDAK MEMANDANG USIA PERNIKAHAN?', '0000-00-00 00:00:00', '1544456754.jpg', '<p class=\"p1\"><strong>Alasan selingkuh</strong></p>\r\n<p class=\"p1\">Begitu banyak penyebab yang bisa kita data &ndash; sama banyaknya dengan alasan orang menikah. Menurut para psikolog beraliran behaviorisme, tindakan menyenangkan seperti perselingkuan cenderung diulang dan dipertahankan.</p>\r\n<p class=\"p1\"><span class=\"Apple-converted-space\">&nbsp;</span>Para psikoanalis berasumsi, penyebabnya adalah konflik-konflik yang tidak terselesaikan di dalam perkawinan.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Para ahli dari pembelajaran sosial yakin, orang-orang melakukannya karena mereka belajar dari generasi sebelumnya melalui proses <em>modeling</em>.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Para ahli biopsikologi menuduh gen sebagai penyebabnya. Hormon testosteron diproduki berlebihan sehingga dorongan untuk berelasi intim itu sangat kuat, terutama pada kaum adam.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Para sosiolog menyebutkan bahwa mobilitas sosial yang semakin tinggi memberi alasan bagi banyaknya perselingkuhan.</p>\r\n<p class=\"p1\">Sementara itu para ahli yang beraliran psikologi humanistik lebih menekankan pengaruh emosi seperti kebosanan, kejengkelan, dendam dan amarah terhadap pasangan. Mereka yang beraliran kognitif menekankan kontrol diri. Orang tidak mampu menghadapi godaan &ndash; kendati menyadari itu keliru &ndash; karena kontrol diri sebagai fasilitas psikis internal sudah rapuh.</p>\r\n<p class=\"p1\">Menurut<span class=\"Apple-converted-space\">&nbsp; </span>banyak psikolog Amerika Serikat tujuan perselingkuhan adalah hubungan intim. Akibatnya, di dalam melakukan terapi perkawinan, mereka sangat sibuk memperbaiki cara-cara atau teknik-teknik bercinta. Wah, perkawinan menjadi sangat mekanistik. Itulah sebabnya mereka cukup sering menuduh perselingkuhan<span class=\"Apple-converted-space\">&nbsp; </span>sebagai pencarian variasi teknik bercinta.</p>\r\n<p class=\"p1\"><span class=\"Apple-converted-space\">&nbsp;</span>Saya pribadi lebih menekanan faktor mental. Itulah sebabnya, saya menekankan pentingnya upaya menumbuhkan kasih-sayang. Masalahnya, amat sering kasih-sayang itu tidak terjadi. Kebanyakan pasangan menyalahartikan kasih-sayang<span class=\"Apple-converted-space\">&nbsp; </span>dengan &ldquo;Saya cinta dia, karena &hellip;.&rdquo; Biasanya, titik-titik itu diisi dengan berbagai kelebihan orang &ldquo;yang dicinta&rdquo;.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Nah, kemudian kelebihan-kelebihan itu dilipatgandakan melalui fantasi. Fantasi membuat harapan menjadi berlebihan. Ternyata setelah menikah, harapan itu jauh dari kenyataan. Nah, inilah awal perselingkuhan.</p>\r\n<p class=\"p1\">Orang kemudian berproses serupa. Menghadapi kenyataan tak memuaskan pada pasangan, orang kemudian berfantasi berlebihan tentang pasangan selingkuh. Itulah yang saya sebut: &ldquo;Membandingkan duri di dalam perkawinan dengan bunga di dalam perselingkuhan.&rdquo;</p>\r\n<p class=\"p1\"><strong>Upaya mencegah perselingkuhan</strong></p>\r\n<p class=\"p1\">Pada saat mobilitas sosial semakin tinggi &ndash; di mana kedua-duanya keluar rumah dan bekerja di tempat kerja masing-masing &ndash; kita hanya bisa berpasrah dan berserah serta berpikir positif bahwa pasangan kita tidak berselingkuh. Kita tidak bisa menguasai dan mengontrol pasangan kita. Tuhan saja tidak mampu mengontrolnya apalagi kita yang terbatas ini.</p>\r\n<p class=\"p1\">Status sebagai istri atau suami tidaklah berarti bahwa kita memiliki dia dan mengontrol dia. Selagi dia mempunyai mata dan hatinya sendiri, dia adalah dirinya sendiri, dan dia bisa saja tertarik akan orang lain. Kita mesti menerima kenyataan tersebut guna menenangkan diri sendiri.</p>\r\n<p class=\"p3\">&nbsp;</p>\r\n<p class=\"p4\">Beberapa tindakan dan perilaku berikut ini perlu dipertahankan demi kebaikan bersama. <strong><em>Bakti</em>: Saling peduli di kala susah dan senang</strong>. <em>Devoted to You,</em> kata Everly Brothers pada 1960-an. <strong><em>Rasa hormat</em>: Apresiasi terhadap tindakan pasangan </strong>dalam kaitan dengan kepuasan hidup perkawinan dan keluarga. <em>A Little Respect</em>, demikian judul lagu Vince Clark dan Andy Bell. <em>Percaya</em>: <strong>Pikiran positif tentang pasangan</strong> satu sama lain. Dengan kata lain, <em>Trust</em> seperti dinyanyikan band rok The Cure.</p>\r\n<p class=\"p4\"><strong><em>Sikap toleran</em>:</strong> Kesabaran dan mengalah tidak mengurangi gensi diri. Penyanyi duet Slow Blow dari Islandia menuturkan gagasan tersebut dengan lagu <em>Within Tolerance</em>. <em>Ketulusan</em>: Saat pasangan berbicara tentang suatu fakta, kita berusaha berlapang dada. Kata Billy Joel dalam lagu <em>Honesty</em> bahwa &ldquo;<em>honesty is such a lonely word.&rdquo;</em></p>\r\n<p class=\"p4\"><strong><em>Keinginan untuk bersama</em>:</strong> Semua nilai di atas menjadi sia-sia jika kita tidak mempunyai keinginan untuk hidup bersama. Kuncinya ada pada keinginan itu sendiri. Begitulah seruan Gary Glitter dalam lagu<span class=\"Apple-converted-space\">&nbsp; </span><em>Never Want the Rain</em>.</p>\r\n<p>&nbsp;</p>\r\n<p class=\"p4\">Akhir kata, kasih-sayang dalam perkawinan adalah penerimaan diri pasangan apa adanya, baik kekurangan mau pun kelebihan.</p>', NULL, NULL, 'perselingkuhan-tidak-memandang-usia-pernikahan', NULL, NULL, 'Published', 4, NULL, '2018-11-17 15:12:17', '2018-12-10 08:55:23', NULL),
(18, 'Cara mengatasi perilaku Perundungan atau Bullying', '0000-00-00 00:00:00', '1544456783.jpg', '<p class=\"p1\"><strong>KEKERASAN<span class=\"Apple-converted-space\">&nbsp;</span></strong></p>\r\n<p class=\"p1\">Siapakah anak yang berkemungkinan tinggi menjadi pelaku kekerasan? Biasanya mereka adalah anak-anak yang sudah menikmati enaknya (<em>reward</em>) bertindak keras terhadap sesamanya. Misalnya, mereka mendapatkan permen dari si kurban yang ketakutan. Juga, pelaku kekerasan itu sudah kehilangan belas kasihan pada temannya. Rendah sudah tingkat rasa bersalah (<em>guilt</em>).</p>\r\n<p class=\"p1\">Kadang-kadang pelaku itu yakin, si korban justru yang menjadi penyebab tindakan kekerasan. Lebih lucu lagi, pelaku percaya, korban pantas mendapatkan perlakuan keras. Kadang juga, pelaku keliru menginterpretasi perlakuan orang terhadap dirinya. Dia merasa orang menyepelekannya.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Kadang pelaku itu justru merupakan korban. Misalnya, di rumah ia diperlakukan keras oleh orang tuanya atau kakak-kakaknya. Di rumah ia dikuasai, di sekolah ia yang menguasai.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Perilaku kekerasan itu bisa bermacam-macam bentuknya. Ada kekerasan fisik. Ada juga kekerasan verbal berupa kata-kata menyakitkan hati. Kekerasan itu bisa pula berbentuk intimidasi atau ancaman dan alienasi sosial (pengucilan).</p>\r\n<p class=\"p1\">&nbsp;</p>\r\n<p class=\"p1\"><strong>STRATEGI<span class=\"Apple-converted-space\">&nbsp;</span></strong></p>\r\n<p class=\"p1\">Untuk melepaskan diri dari perangkap para antisosial itu, Anda dapat menerapkan bebarapa strategi berikut.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Pertama, <strong>carilah bantuan orang lain</strong>. Jika memungkinkan Anda mencari bantuan orang (dewasa) terdekat seperti guru atau anggota satpam. Atau, sebelum diserang, Anda minta bantuan orang lain untuk mengawasi keadaan.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Karena para penyerang itu doyan akan anak yang lemah, maka <strong>jangan takut menantang</strong>. Tataplah tajam mata penyerang sambil berkata: &ldquo;Saya tidak suka akan perilakumu. Hentikan!&rdquo; Tapi, jika Anda betul-betul takut, cara ini tidak perlu diterapkan. Mencari bantuan itu lebih baik.</p>\r\n<p class=\"p1\">Kadang-kadang <strong>humor dapat membantu meredakan suasana</strong>. Sambil mencari bantuan dari guru atau anggota satpam, Anda bisa mengalihkan perhatian penyerang itu dengan guyonan untuk memperlambat serangan sebelum bantuan datang. Karena itu Anda mesti bisa menguasai perasaan takut.</p>\r\n<p class=\"p1\"><strong>Menghindar merupakan strategi yang efektif.</strong> Apalagi jika penyerang itu sendirian dan berada cukup jauh dari Anda. Karena itu Anda mesti mempelajari situasi-situasi di mana serangan itu suka terjadi. Misalnya, jika serangan sering terjadi saat Anda bersendiri, maka Anda berusaha untuk terus bersama orang lain yang bisa melindungi Anda.</p>\r\n<p class=\"p1\"><strong>&ldquo;Pernyataan diri&rdquo; atau <em>self-talk</em></strong> bisa diterapkan saat diperlakukan kasar. Strategi ini dimaksudkan untuk <strong>menenangkan diri</strong>. Simpanlah di benak Anda suatu &ldquo;pernyataan diri&rdquo; yang dapat menenangkan Anda. Misalnya, &ldquo;Bukan kesalahan saya bahwa ia berperilaku kasar terhadap saya.&rdquo; Atau, &ldquo;Sikap kasarnya itu menampilkan kepribadiannya yang lemah.&rdquo; Penyataan diri ini dimaksudkan untuk menguatkan diri kita secara psikis dan kita tidak menyalahkan dan menyesali diri sendiri.</p>\r\n<p class=\"p1\">Strategi &ldquo;pengakuan diri&rdquo; juga merupakan cara yang baik. Sering orang dilecehkan dan diperlakukan kasar karena stereotip. Misalnya, bahasa dan gaya bicara yang <em>ndeso</em> (orang kampung). Menghadapi serangan semacam ini, akuilah diri Anda dengan berkata: &ldquo;Memang, saya orang kampung. Apa salahnya menjadi orang kampung?&rdquo;<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Itulah sejumlah strategi menghadapi perlakuan kasar dan keras. Terapkanlah strategi-strategi itu sesuai tuntutan keadaan.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Ingatlah bahwa para pelaku kekarasan itu suka membuat Anda marah dengan menyerang sisi lemah Anda. Kelamahan Anda diungkit-ungkit agar Anda marah. Saat marah, Anda menjadi labil dan mudah diserang. Jadi, <em>keep cool</em> alias tetap tenang. Sambil memikirkan konsekuensi negatif dari kemarahan Anda, berusahalah menghitung ke belakang, misalnya dari 30 sampai 0. Saat sampai ke angka 0, kemarahan sudah mereda.<span class=\"Apple-converted-space\">&nbsp;</span></p>\r\n<p class=\"p1\">Kemarahan itu berarti Anda memberi kepada si penyerang kontrol akan diri Anda. Dengan kontrol diri yang kuat, Anda menjadi pemenang. Pemenang bukan berarti jumlah serangan balasan terhadap si penyerang. Kemenangan berarti kemampuan Anda mengontrol emosi Anda.</p>', NULL, NULL, 'cara-mengatasi-perilaku-perundungan-atau-bullying', NULL, NULL, 'Published', 4, NULL, '2018-11-17 15:12:44', '2018-12-10 08:54:50', NULL);
INSERT INTO `selfenhancements` (`id`, `name`, `publish_datetime`, `featured_image`, `content`, `meta_title`, `cannonical_link`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(19, 'kenapa musik dapat dimanfaatkan sebagai terapi menghadapi kecemasan? ini penjelasannya', '0000-00-00 00:00:00', '1544456805.jpg', '<p>Pada dasarnya seorang individu yang mengalami&nbsp; <strong><em>anxiety</em> (kecemasan) disebabkan oleh produksi hormon tiroksin yang tinggi dalam otak manusia</strong>. Seseorang yang mengalami proses emosional yang negatif akan merangsang hipotalamus memproduksi hormon tiroksin yang tinggi. Hal tersebut yang menyebabkan individu mudah lelah, mudah cemas, mudah tegang, mudah takut, dan susah tidur, sehingga keadaan individu menjadi kurang optimal (Vianna, Barbosa, Carvalhaes, &amp; Cunha (2012). Untuk menanggulangi hal tersebut, seseorang harus dapat menyeimbangkan diri dalam setiap kondisi yang dialami. Otak manusia memiliki empat morfin alami tubuh, yaitu hormon positif yang dapat meredakan penyakit dan membuat hidup menjadi bahagia. Morfin-morfin tersebut ialah hormon endorphin, dopamin, serotonin, dan oksitosin. Fungsi dari morfin-morfin alami ini dapat membuat tubuh menjadi lebih rileks, sehingga dapat mereduksi kecemasan atau stres.</p>\r\n<p>Salah satu intervensi untuk meningkatkan produksi hormon endorphin dan serotonin ialah dengan melakukan relaksasi melalui mendengarkan musik. Secara psikologis, musik memiliki hubungan yang positif dalam kehidupan manusia. Musik dapat membuat seseorang menjadi lebih rileks, mengurangi stres, menimbulkan rasa aman dan sejahtera, meningkatkan rasa bahagia, meningkatkan kecerdasan, meningkatkan rasa percaya diri, dan membantu melepaskan rasa sakit. Hal ini diperkuat juga oleh penelitian yang membuktikan bahwa musik dapat meningkatkan produksi hormon endorphin dan serotonin yang mengakibatkan seorang individu dapat merasa lebih bahagia dan mereduksi kecemasan yang dialami. Musik sebagai suatu intervensi yang dapat dilakukan dalam membantu seorang individu dalam mereduksi kecemasan telah banyak terbukti.&nbsp;</p>\r\n<p>Musik dapat digunakan untuk membantu individu memahami perkembangan emosi dan kognitif mereka. Individu dapat mendengarkan lagu, ataupun memainkan alat musik secara aktif. Melalui musik, konselor dapat membuat proses konseling menjadi lebih menarik dan efektif. Ada beberapa teknik yang dapat membantu konselor dan konseli dalam melakukan reframing ide, memfokuskan perspektif, eksternalisasi emosi, dan memperdalam pemahaman dari sebuah pengalaman atau masalah. Salah satunya ialah melalui musik. Penggunaan musik dalam proses konseling memiliki banyak manfaat yang terapeutik. Musik dapat dijadikan sebagai salah satu strategi konseling berupa teknik relaksasi untuk mengurangi, menurunkan dan mengatasi kecemasan dan ketegangan emosi.</p>\r\n<p>Teknik relaksasi merupakan <em>coping skill</em> yang efektif untuk menurunkan tingkat kecemasan. Musik digunakan sebagai media untuk menenangkan, dan membantu konseli untuk merasa nyaman, sehingga proses konseling menjadi lebih efektif. Penggunaan musik dalam proses konseling dikenal sebagai <em>music therapy</em>. Kajian bahwa <em>music therapy</em> sebagai salah satu bentuk intervensi terapi ekspresif atau seni kreatif dalam pendekatan konseling integratif (<em>integrative approach</em>), yang dapat diterapkan dalam proses konseling juga dijelaskan secara konkret .Terapi musik yang dilakukan di College of Notre Dame, Belmont, California menggunakan stimulus suara (bunyi, musik) untuk mengetahui dampak suara terhadap kondisi stres dan rileks yang dialami seseorang, saat ini telah mendunia.</p>\r\n<p>&nbsp;Terapi musik dapat berdampak positif untuk mengatasi kecemasan. Terapi musik&nbsp; merupakan teknik yang sangat mudah dilakukan dan terjangkau, namun efeknya cukup besar. Musik dapat digunakan sebagai pendekatan dalam membantu individu yang mengalami hambatan kondisi fisik, perilaku, dan psikologis agar mampu menjadi lebih baik. Musik dapat digunakan sebagai intervensi untuk menurunkan <em>academic anxiety</em> yang dialami oleh mahasiswa penyusun skripsi. Namun, penelitian ini hanya membuktikan penggunaan musik Mozart sebagai <em>passive music therapy</em> (terapi musik pasif) saja, dan belum mengkaji tentang pemberian musik dalam teknik <em>active music therapy</em> (terapi musik aktif).</p>', NULL, NULL, 'http://kobarin.id/self-enhancement/kenapa-musik-dapat-dimanfaatkan-sebagai-terapi-menghadapi-kecemasan-ini-penjelasannya', NULL, NULL, 'Published', 13, NULL, '2018-12-09 16:03:04', '2018-12-10 08:54:02', NULL),
(21, 'Menghasilkan karya saat konseling, bagaimana bisa?', '0000-00-00 00:00:00', '1544456848.jpg', '<p>Penerapan terapi musik&nbsp; dibagi menjadi dua, yaitu <em>passive music therapy</em> dan <em>active music therapy</em>. Terapi musik pasif (<em>passive music therapy</em>) adalah pemberian terapi musik yang dilakukan dengan cara mengajak konseli untuk mendengarkan sebuah instrumen tertentu secara seksama. Sedangkan, terapi musik&nbsp; aktif (<em>active music therapy</em>) adalah proses pemberian terapi musik yang dilakukan dengan cara mengajak konseli untuk memainkan sebuah instrumen, bernyanyi, maupun menciptakan lagu. Kedua teknik terapi musik ini dapat dilakukan melalui konseling individual maupun kelompok. Proses pemberian kedua teknik ini dalam praktik konseling, konselor dapat melakukan kegiatan seperti mendengarkan musik kepada konseli, melakukan improvisasi, dan menciptakan lagu. Dengan mengajak konseli untuk mendengarkan musik, dapat membantu&nbsp; konseli mengubah suasana hati mereka menjadi lebih positif, dan mengurangi tingkat kecemasan yang mereka alami. Ketika konselor bekerja dengan konseli dengan menggunakan musik, improvisasi dapat dicapai secara konkret dengan meminta konseli untuk melakukan variasi pada tema musik yang ada (Wigram 2004, dalam). Konselor dapat mengajak konseli untuk memainkan alat musik mereka dan mengubah melodi (yaitu, membuat musik menjadi lebih cepat, lebih lambat, atau divariasikan).</p>\r\n<p>Teknik terakhir yang dapat digunakan ialah dengan menciptakan lagu. Proses menciptakan dan mengembangkan sebuah lagu, dipandang sebagai terapi yang berasal dari dalam diri konseli itu sendiri (Nordoff &amp; Robbins, 1977 dalam). Menciptakan lagu adalah tindakan kreatif yang menempatkan konseli berada pada perasaan mereka sendiri. Hal ini dapat digunakan sebagai cara untuk penyembuhan yang melekat dalam tindakan kreatif (Schmidt, 1983 dalam). Dalam praktek yang sebenarnya, konselor dapat meminta atau mendorong konseli untuk menulis atau menciptakan sebuah karya lagu yang mewakili diri mereka sendiri, sehingga pada sesi berikutnya konseli dapat berbicara tentang pengalaman menciptakan sebuah karya lagu tersebut sebagai bentuk katarsis. Selain itu juga, dalam teknik active music therapy, konselor dapat mengajak konseli untuk menciptakan sebuah lirik lagu yang sesuai dengan permasalahan yang mereka alami untuk membantu konseli menyampaikan perasaan sakit yang dialami sehingga membantunya dalam proses recovery.&nbsp;</p>\r\n<p>Terapi musik dirancang dengan pengenalan yang mendalam terhadap keadaan dan permasalahan yang tengah dialami oleh konseli, sehingga teknik yang diberikan akan berbeda untuk setiap individu.&nbsp; Misalnya saja, ada konseli yang lebih sesuai dengan menggunakan teknik terapi musik aktif, tetapi ada juga yang lebih terbantu dengan teknik terapi musik pasif, ataupun sebaliknya. Setiap terapi musik juga akan berbeda maknanya bagi setiap orang. Kesesuaian terapi musik akan sangat ditentukan oleh nilai- nilai individual, falsafah yang dianut, pendidikan, tatanan klinis, dan latar belakang budaya.</p>\r\n<p>Semua terapi musik memiliki tujuan yang sama, yaitu membantu mengekspresikan perasaan, membantu rehabilitasi fisik, memberi pengaruh positif terhadap kondisi suasana hati dan emosi, meningkatkan memori, serta menyediakan kesempatan yang unik untuk berinteraksi dan membangun kedekatan emosional antara konseli dengan konselor (konseling individual dan kelompok) maupun antara konseli dengan konseli (konseling kelompok). Dengan demikian, terapi musik juga diharapkan dapat membantu mengatasi stres, mencegah penyakit dan meringankan rasa sakit yang dialami oleh seorang individu.</p>\r\n<p>jadi, bukan hal yang tidak mungkin saat terjadinya proses konseling klien/konseli dapat menghasilkan karya khususnya dalam karya seni musik yaitu penciptaan sebuah lagu.</p>', NULL, NULL, 'http://kobarin.id/self-enhancement/menghasilkan-karya-saat-konseling-bagaimana-bisa', NULL, NULL, 'Published', 13, NULL, '2018-12-09 16:10:53', '2018-12-10 08:53:25', NULL),
(22, 'solusi cerdas menghadapi cemas karena skripsi', '0000-00-00 00:00:00', '1544456877.jpg', '<p>Hmm…menghadapi skripsi??? Setiap mahasiswa pasti menghadapi skripsi,dan itu seringkali bikin streesssss….pusingggg….cemas-cemas-cemas… nah di artikel ini ada cara menghadapinya loh.. yap <em>music therapy</em>….</p>\r\n<p><em>Music therapy</em> dalam penerapannya dapat meningkatkan produksi keempat hormon positif yang ada di dalam tubuh manusia, yaitu <strong>endorphin, dopamin, serotonin, dan oksitosin</strong>. Fungsi dari keempat hormon positif tersebut dapat <strong>membuat tubuh menjadi lebih rileks</strong>, <strong>mereduksi kecemasan atau stres</strong>, <strong>meningkatkan kebahagiaan, meningkatkan kecerdasan, dan meningkatkan rasa percaya diri</strong>. Pemberian <em>music therapy</em> sebagai layanan intervensi untuk membantu mahasiswa dalam mereduksi academic anxiety akan jauh lebih efektif dibandingkan dengan layanan intervensi konseling konvensional yang selama ini dilakukan oleh konselor pendidikan, karena <strong>dengan <em>music therapy</em> mahasiswa dapat mereduksi kecemasannya terhadap skripsi dan meningkatkan rasa percaya dirinya dalam  menyelesaikan skripsi</strong>.</p>\r\n<p>Dalam rumusan The American <em>Music therapy</em> <em>Association</em>, dikatakan bahwa “terapi musik adalah suatu profesi yang menggunakan musik dan aktivitas musik untuk mengatasi berbagai masalah dalam aspek fisik, psikologis, kognitif, dan kebutuhan sosial individu”. Dari aspek fisik, terapi musik sebagai sebuah teknik yang digunakan untuk penyembuhan suatu penyakit dengan menggunakan bunyi atau irama tertentu yang diberikan oleh seseorang yang berprofesi sebagai seorang dokter dengan spesialisasi psikiatri. Dari aspek psikologis dan kognitif, terapi musik diberikan oleh seseorang yang berprofesi sebagai psikolog atau psikoterapis. Sedangkan dari aspek kebutuhan sosial individu, terapi musik dapat diberikan oleh seseorang yang berprofesi sebagai seorang konselor. Dalam setiap praktik profesionalnya, seorang terapis musik dapat menggunakan beberapa jenis musik tertentu untuk  membantu para klien dalam mengentaskan permasalahannya, yaitu dengan menggunakan musik klasik, instrumentalis, maupun <em>slow music</em> (Potter & Perry, 2005 dalam).</p>\r\n<p>Terapi musik adalah proses yang menggabungkan antara aspek penyembuhan musik itu sendiri dengan proses konseling untuk membantu konseli dalam mengentaskan permasalahan yang melingkupi hal fisik atau tubuh, emosi, mental, spiritual, kognitif, dan kebutuhan sosial seseorang.</p>', NULL, NULL, 'http://kobarin.id/self-enhancement/solusi-cerdas-menghadapi-cemas-karena-skripsi', NULL, NULL, 'Published', 13, NULL, '2018-12-09 16:15:18', '2018-12-10 08:51:45', NULL),
(23, 'Konseling dengan musik, kenapa tidak?', '0000-00-00 00:00:00', '1544456903.jpg', '<p>Dalam keilmuan Bimbingan &amp; Konseling (Psikologi Pendidikan), terapi musik dapat dijadikan sebagai salah satu bentuk intervensi yang dapat digunakan oleh para konselor dalam praktik konselingnya. Melalui musik, proses &nbsp;rapport antara konselor dan konseli dapat berlangsung dengan baik, karena musik merupakan bahasa yang universal dan dapat menembus batas-batas multikultural. Dalam sejarahnya, sejak peradaban yang lalu musik dipergunakan oleh manusia dalam proses penyembuhan di berbagai kebudayaan di dunia, sehingga saat ini musik dapat digunakan dalam proses konseling modern.</p>\r\n<p>Proses konseling modern saat ini memang harus menarik dan menyenangkan, karena dapat membuat konseli bersikap lebih terbuka dan termotivasi untuk berubah ke arah yang lebih baik, sehingga proses konseling menjadi lebih efektif. Efektivitas proses konseling dengan menggunakan terapi musik sangat terapeutik, karena melalui terapi musik dapat membantu konselor dan konseli melakukan reframing ide, memfokuskan perspektif, eksternalisasi emosi, dan memperdalam pemahaman dari sebuah pengalaman atau masalah. Dengan demikian, ada sebuah proses katarsis yang terjadi dalam proses konseling melalui terapi musik ini. Konseli dapat menemukan beberapa penghiburan dan bahkan beberapa resolusi penuh makna, terutama jika mereka berbagi kata-kata dari sebuah lagu dengan konselor</p>\r\n<p>. Dalam proses konseling, musik dapat membantu konseli dan konselor menemukan tujuan yang hendak dicapai, sehingga membantu konseli melihat berbagai kemungkinan dan membantu mereka untuk menyadari hal-hal positif yang dapat dilakukan untuk membantu mereka menyelesaikan masalah. Seorang konselor di dalam setiap praktiknya dituntut untuk memiliki rasa empati, kesabaran, ketulusan untuk membantu orang lain, bijaksana, penuh pengertian, namun juga fleksibel dan memiliki rasa humor. Semua persyaratan di atas penting karena dalam praktiknya, konselor bekerja dengan individu yang sedang menghadapi masalah, menderita sakit, mengalami stres berat, membutuhkan perhatian lebih karena keterbatasan- keterbatasannya, atau tersingkir dari relasi sosial yang wajar karena berbagai sebab baik secara internal maupun eksternal.</p>\r\n<p>Seorang konselor juga harus menjunjung tinggi etika profesi, yaitu dengan menjaga kerahasiaan data konseli, menghormati hak-hak konseli, melakukan prosedur konseling dengan benar, dan menjaga etika kerja profesional baik dengan sesama konselor maupun dengan profesi terkait lainnya. Oleh karena itu, pendidikan dan pelatihan untuk menjadi seorang konselor yang dapat memberikan terapi musik itu bersifat lintas bidang, karena terapi musik adalah bidang multidisiplin yang terkait erat dengan bidang-bidang pengetahuan di luar musik pula. Sangat penting bahwa seorang konselor memahami psikologi musik dengan baik, diperlukan pengetahuan terhadap berbagai proses pendengaran dan proses kerja otak yang menjadi dasar-dasar proses fisiologis terjadinya persepsi musik. Selain itu, dengan memahami psikologi musik, seorang konselor dimungkinkan untuk menjelaskan berbagai pengalaman musikal dan merancang jenis musik yang sesuai bagi konselinya.</p>\r\n<p style=\"text-align: center;\"><em>&ldquo;Musik membuat hidup lebih baik&rdquo;</em></p>', NULL, NULL, 'http://kobarin.id/self-enhancement/konseling-dengan-musik-kenapa-tidak', NULL, NULL, 'Published', 13, NULL, '2018-12-09 19:36:48', '2018-12-10 08:49:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keyword` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disclaimer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_analytics` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_video1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_video2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_video3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_video4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `explanation1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `explanation2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `explanation3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `explanation4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `logo`, `favicon`, `seo_title`, `seo_keyword`, `seo_description`, `company_contact`, `company_address`, `from_name`, `from_email`, `facebook`, `linkedin`, `twitter`, `google`, `copyright_text`, `footer_text`, `terms`, `disclaimer`, `google_analytics`, `home_video1`, `home_video2`, `home_video3`, `home_video4`, `explanation1`, `explanation2`, `explanation3`, `explanation4`, `created_at`, `updated_at`) VALUES
(1, '1542513010kobarin-logo2.png', '1543931954logo_non_background_k6z_icon.ico', 'KOBARIN.ID', NULL, NULL, NULL, NULL, 'Kobarin No-Reply', 'no-reply@kobarin.id', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<!-- Global site tag (gtag.js) - Google Analytics -->\r\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-129085821-1\"></script>\r\n<script>\r\n  window.dataLayer = window.dataLayer || [];\r\n  function gtag(){dataLayer.push(arguments);}\r\n  gtag(\'js\', new Date());\r\n\r\n  gtag(\'config\', \'UA-129085821-1\');\r\n</script>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-04 07:24:58');

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT 0,
  `is_term_accept` tinyint(1) NOT NULL DEFAULT 0 COMMENT ' 0 = not accepted,1 = accepted',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lisensi_professional` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `is_term_accept`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`, `photo`, `lisensi_professional`, `about`) VALUES
(1, 'System', 'Administrator', 'admin@admin.com', '$2y$10$Fupjf/hYRsQI262CdXaT8.R0mwGSIwz9gn4GBJ1aoJ0vnUKrd5HLK', 1, 'a1812a0d3527de12d3a99e2e8a9e5a85', 1, 0, 'vHMr6bwXXVWiYuseThEVaHI7fPPP9oAPuOIC0qw1mh2nkYfo8j4tUMYOnqBv', 1, 1, '2018-11-17 03:54:42', '2018-12-07 02:55:42', NULL, NULL, NULL, NULL),
(2, 'Vipul', 'Basapati', 'executive@executive.com', '$2y$10$eaDhhPWXQ16RzGK5ZNxwze05JX7iYWcwalnuD.tkbfZHIh3ZBn0Mq', 1, 'd7cd4996f3c5ab26feed79d96156e675', 1, 0, NULL, 1, NULL, '2018-11-17 03:54:42', '2018-11-17 03:54:42', NULL, NULL, NULL, NULL),
(3, 'User', 'Test', 'user@user.com', '$2y$10$RbsDCCatSJQk3MawZh6SAeYpiUQwKiSHoPWDzETknUbgrovk.SVwW', 1, '3a60ca78c82ecdfb9d3331184cdd68b4', 1, 0, NULL, 1, NULL, '2018-11-17 03:54:43', '2018-11-17 03:54:43', NULL, NULL, NULL, NULL),
(4, 'Felix', 'Lengkong, Ph.D.', 'felix@kobarin.id', '$2y$10$JXJ.gOsYRAGDC5.O2RsPOek1/E5Z2obF/N0XE2I.KoM6YIYtpWAAu', 1, '4f420ae631d687eecc49936a4ef80dbd', 1, 0, NULL, 1, NULL, '2018-11-17 22:28:13', '2018-12-07 15:44:25', NULL, '1543759443.jpeg', '<ul style=\"list-style-type: circle;\">\r\n<li>Dokter Filsafat di La Salle University</li>\r\n<li>Psikolog Klinis</li>\r\n<li>Spesialis Psikoanalisis</li>\r\n</ul>', '<ul style=\"list-style-type: circle;\">\r\n<li>Dosen Psikologi di Universitas Gunadarma</li>\r\n<li>Dosen Etika Profesi di Universitas Katolik Atma Jaya Indonesia</li>\r\n</ul>'),
(12, 'asdf', 'asdfg', 'vanguard.basker@gmail.com', '$2y$10$4kxvY3WC83X6yJVVWxa5OedwVUyiI0K7lcys.JxJopJZdIeY9OdK6', 1, '06a8b25576f96275f0a10b7ecdbeb90f', 1, 1, 'at4PrDd5Z1CPOeNt3wEy8G4eQ9p6vRpKQ0mlsULeyypvEVxPE36lJAUC1KdC', NULL, NULL, '2018-12-07 03:03:13', '2018-12-07 03:03:52', NULL, NULL, NULL, NULL),
(13, 'Raxel', 'Situmorang', 'raxel@kobarin.id', '$2y$10$Q1J2/vMJzyilYU08x9L0leatqR.F9l0fM9BtrM5Q2HO0SzwGO60OS', 1, '473b2bf92322096b7dce8f6341a8394a', 1, 0, NULL, 1, NULL, '2018-12-07 16:00:26', '2018-12-07 16:02:25', NULL, '1544248945.jpeg', '<p>asdf</p>', '<p>asdf</p>'),
(14, 'hilmy', 'syarif', 'hilmysyarif@gmail.com', '$2y$10$Tt/SQncVPZmONqnuftzj3u/XYi6eZG7jW5IVx3juEFKbLPfa2Uai6', 1, '71db5e1d98519458137c1864d7b39347', 1, 1, 'am8WyQsMSGIKENOFGjHHU61BA0yV1umKc84vcYEHuRzYXqkIkODzQ0vtm5aB', NULL, NULL, '2018-12-09 22:53:02', '2018-12-09 22:53:22', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_datetime` datetime DEFAULT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Published','Draft','InActive','Scheduled') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `name`, `publish_datetime`, `featured_image`, `content`, `meta_title`, `cannonical_link`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test', '0000-00-00 00:00:00', 'tentang-kami.png', '<p style=\"text-align: center;\"><iframe src=\"https://www.youtube.com/embed/7kxSQMqxhJw\" width=\"560\" height=\"315\" frameborder=\"0\" allowfullscreen=\"\"></iframe></p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem donec massa sapien faucibus et molestie ac feugiat sed. Vestibulum rhoncus est pellentesque elit ullamcorper dignissim. Egestas dui id ornare arcu odio. Adipiscing diam donec adipiscing tristique risus. Sit amet luctus venenatis lectus magna fringilla. Tortor id aliquet lectus proin nibh. Lectus nulla at volutpat diam ut venenatis. Diam sollicitudin tempor id eu nisl nunc mi ipsum faucibus. Praesent semper feugiat nibh sed pulvinar proin gravida hendrerit. Tortor aliquam nulla facilisi cras fermentum odio eu feugiat. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Facilisis magna etiam tempor orci eu lobortis elementum nibh tellus.</p>\r\n<p>Gravida in fermentum et sollicitudin ac orci. Ultrices tincidunt arcu non sodales. Mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare. At auctor urna nunc id. In nibh mauris cursus mattis molestie a. At elementum eu facilisis sed. Tellus cras adipiscing enim eu turpis egestas pretium aenean pharetra. Aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices. Hac habitasse platea dictumst vestibulum. Arcu non odio euismod lacinia at quis risus sed vulputate.</p>\r\n<p>Turpis nunc eget lorem dolor sed viverra ipsum nunc. Sed blandit libero volutpat sed cras ornare arcu. Malesuada fames ac turpis egestas integer. Mauris ultrices eros in cursus turpis massa. Enim praesent elementum facilisis leo. Vitae tortor condimentum lacinia quis vel eros donec ac. Aliquam etiam erat velit scelerisque in dictum non. Habitant morbi tristique senectus et netus et malesuada fames ac. Nunc aliquet bibendum enim facilisis gravida neque convallis a. Nulla facilisi etiam dignissim diam quis. Urna nec tincidunt praesent semper.</p>\r\n<p>Non odio euismod lacinia at quis risus. Quis vel eros donec ac odio. Mattis rhoncus urna neque viverra justo. Aliquam vestibulum morbi blandit cursus risus at ultrices. Enim tortor at auctor urna nunc id cursus metus. Quam id leo in vitae turpis massa. Ac auctor augue mauris augue neque gravida. Diam vulputate ut pharetra sit amet aliquam id diam maecenas. Interdum posuere lorem ipsum dolor sit amet consectetur. Arcu dictum varius duis at. Vitae tempus quam pellentesque nec. Magna etiam tempor orci eu lobortis. Elit at imperdiet dui accumsan sit amet nulla. Morbi leo urna molestie at. Tempus quam pellentesque nec nam aliquam sem et tortor. Laoreet suspendisse interdum consectetur libero id. Eros in cursus turpis massa. Leo integer malesuada nunc vel risus commodo viverra maecenas accumsan. Tristique senectus et netus et malesuada fames.</p>\r\n<p>Enim tortor at auctor urna nunc id cursus metus. Malesuada fames ac turpis egestas sed. At urna condimentum mattis pellentesque id. Proin sagittis nisl rhoncus mattis rhoncus. Nibh tortor id aliquet lectus proin nibh. Sit amet risus nullam eget felis eget nunc lobortis. Mi quis hendrerit dolor magna eget est lorem ipsum dolor. Pellentesque sit amet porttitor eget. Duis convallis convallis tellus id. Pharetra vel turpis nunc eget lorem. Vulputate mi sit amet mauris commodo quis imperdiet. Aliquam ut porttitor leo a diam sollicitudin. Senectus et netus et malesuada fames ac turpis egestas integer. Et netus et malesuada fames ac turpis egestas integer eget. Ullamcorper malesuada proin libero nunc consequat. Lacus viverra vitae congue eu. Arcu dui vivamus arcu felis bibendum ut tristique et. Magna etiam tempor orci eu lobortis elementum nibh tellus.</p>\r\n<p>&nbsp;</p>', NULL, NULL, 'http://127.0.0.1:8000/videos/test', NULL, NULL, 'Published', 4, NULL, '2018-11-19 09:05:52', '2018-11-19 09:05:52', NULL),
(2, 'Test video', '0000-00-00 00:00:00', 'share.jpg', '<p style=\"text-align: center;\"><iframe src=\"https://www.youtube.com/embed/br9J69dSnIQ\" width=\"560\" height=\"315\" frameborder=\"0\" allowfullscreen=\"\"></iframe></p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem donec massa sapien faucibus et molestie ac feugiat sed. Vestibulum rhoncus est pellentesque elit ullamcorper dignissim. Egestas dui id ornare arcu odio. Adipiscing diam donec adipiscing tristique risus. Sit amet luctus venenatis lectus magna fringilla. Tortor id aliquet lectus proin nibh. Lectus nulla at volutpat diam ut venenatis. Diam sollicitudin tempor id eu nisl nunc mi ipsum faucibus. Praesent semper feugiat nibh sed pulvinar proin gravida hendrerit. Tortor aliquam nulla facilisi cras fermentum odio eu feugiat. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Facilisis magna etiam tempor orci eu lobortis elementum nibh tellus.</p>\r\n<p>Gravida in fermentum et sollicitudin ac orci. Ultrices tincidunt arcu non sodales. Mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare. At auctor urna nunc id. In nibh mauris cursus mattis molestie a. At elementum eu facilisis sed. Tellus cras adipiscing enim eu turpis egestas pretium aenean pharetra. Aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices. Hac habitasse platea dictumst vestibulum. Arcu non odio euismod lacinia at quis risus sed vulputate.</p>\r\n<p>Turpis nunc eget lorem dolor sed viverra ipsum nunc. Sed blandit libero volutpat sed cras ornare arcu. Malesuada fames ac turpis egestas integer. Mauris ultrices eros in cursus turpis massa. Enim praesent elementum facilisis leo. Vitae tortor condimentum lacinia quis vel eros donec ac. Aliquam etiam erat velit scelerisque in dictum non. Habitant morbi tristique senectus et netus et malesuada fames ac. Nunc aliquet bibendum enim facilisis gravida neque convallis a. Nulla facilisi etiam dignissim diam quis. Urna nec tincidunt praesent semper.</p>\r\n<p>Non odio euismod lacinia at quis risus. Quis vel eros donec ac odio. Mattis rhoncus urna neque viverra justo. Aliquam vestibulum morbi blandit cursus risus at ultrices. Enim tortor at auctor urna nunc id cursus metus. Quam id leo in vitae turpis massa. Ac auctor augue mauris augue neque gravida. Diam vulputate ut pharetra sit amet aliquam id diam maecenas. Interdum posuere lorem ipsum dolor sit amet consectetur. Arcu dictum varius duis at. Vitae tempus quam pellentesque nec. Magna etiam tempor orci eu lobortis. Elit at imperdiet dui accumsan sit amet nulla. Morbi leo urna molestie at. Tempus quam pellentesque nec nam aliquam sem et tortor. Laoreet suspendisse interdum consectetur libero id. Eros in cursus turpis massa. Leo integer malesuada nunc vel risus commodo viverra maecenas accumsan. Tristique senectus et netus et malesuada fames.</p>\r\n<p>Enim tortor at auctor urna nunc id cursus metus. Malesuada fames ac turpis egestas sed. At urna condimentum mattis pellentesque id. Proin sagittis nisl rhoncus mattis rhoncus. Nibh tortor id aliquet lectus proin nibh. Sit amet risus nullam eget felis eget nunc lobortis. Mi quis hendrerit dolor magna eget est lorem ipsum dolor. Pellentesque sit amet porttitor eget. Duis convallis convallis tellus id. Pharetra vel turpis nunc eget lorem. Vulputate mi sit amet mauris commodo quis imperdiet. Aliquam ut porttitor leo a diam sollicitudin. Senectus et netus et malesuada fames ac turpis egestas integer. Et netus et malesuada fames ac turpis egestas integer eget. Ullamcorper malesuada proin libero nunc consequat. Lacus viverra vitae congue eu. Arcu dui vivamus arcu felis bibendum ut tristique et. Magna etiam tempor orci eu lobortis elementum nibh tellus.</p>', NULL, NULL, 'http://127.0.0.1:8000/videos/test-video', NULL, NULL, 'Published', 4, NULL, '2018-11-19 09:08:19', '2018-11-19 09:08:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE `views` (
  `id` int(10) UNSIGNED NOT NULL,
  `viewable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `viewable_id` bigint(20) UNSIGNED NOT NULL,
  `visitor` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `viewed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `views`
--

INSERT INTO `views` (`id`, `viewable_type`, `viewable_id`, `visitor`, `viewed_at`) VALUES
(1, 'App\\Models\\Agenda\\Agenda', 1, '127.0.0.1', '2018-12-10 00:41:06'),
(2, 'App\\Models\\Agenda\\Agenda', 1, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 00:41:28'),
(3, 'App\\Models\\Agenda\\Agenda', 1, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 00:42:58'),
(4, 'App\\Models\\Agenda\\Agenda', 1, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 00:43:01'),
(5, 'App\\Models\\Agenda\\Agenda', 1, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 00:44:04'),
(6, 'App\\Models\\Selfdevelopment\\Selfdevelopment', 2, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 00:50:48'),
(7, 'App\\Models\\Selfdevelopment\\Selfdevelopment', 3, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 00:50:55'),
(8, 'App\\Models\\Agenda\\Agenda', 1, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 00:51:23'),
(9, 'App\\Models\\Selfdevelopment\\Selfdevelopment', 2, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 00:53:54'),
(10, 'App\\Models\\Video\\Video', 1, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 00:54:20'),
(11, 'App\\Models\\Selfdevelopment\\Selfdevelopment', 2, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 05:34:20'),
(12, 'App\\Models\\Selfdevelopment\\Selfdevelopment', 2, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 05:37:24'),
(13, 'App\\Models\\Selfdevelopment\\Selfdevelopment', 2, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 05:37:57'),
(14, 'App\\Models\\Video\\Video', 1, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 05:44:09'),
(15, 'App\\Models\\Agenda\\Agenda', 1, '4RgtBUSHthd7u2WeyLZfc3q3A3RxhNzHKDRRVQnDm1hTmQVw3OfkCA2NB6IAAe8TS1Xo8bSReYochZd9', '2018-12-10 06:39:08'),
(16, 'App\\Models\\Agenda\\Agenda', 1, '4RgtBUSHthd7u2WeyLZfc3q3A3RxhNzHKDRRVQnDm1hTmQVw3OfkCA2NB6IAAe8TS1Xo8bSReYochZd9', '2018-12-10 06:39:56'),
(17, 'App\\Models\\Agenda\\Agenda', 1, '4RgtBUSHthd7u2WeyLZfc3q3A3RxhNzHKDRRVQnDm1hTmQVw3OfkCA2NB6IAAe8TS1Xo8bSReYochZd9', '2018-12-10 06:41:13'),
(18, 'App\\Models\\Agenda\\Agenda', 1, '4RgtBUSHthd7u2WeyLZfc3q3A3RxhNzHKDRRVQnDm1hTmQVw3OfkCA2NB6IAAe8TS1Xo8bSReYochZd9', '2018-12-10 06:42:50'),
(19, 'App\\Models\\Agenda\\Agenda', 1, '4RgtBUSHthd7u2WeyLZfc3q3A3RxhNzHKDRRVQnDm1hTmQVw3OfkCA2NB6IAAe8TS1Xo8bSReYochZd9', '2018-12-10 06:43:13'),
(20, 'App\\Models\\Agenda\\Agenda', 1, '4RgtBUSHthd7u2WeyLZfc3q3A3RxhNzHKDRRVQnDm1hTmQVw3OfkCA2NB6IAAe8TS1Xo8bSReYochZd9', '2018-12-10 06:43:48'),
(21, 'App\\Models\\Video\\Video', 1, '4RgtBUSHthd7u2WeyLZfc3q3A3RxhNzHKDRRVQnDm1hTmQVw3OfkCA2NB6IAAe8TS1Xo8bSReYochZd9', '2018-12-10 06:44:09'),
(22, 'App\\Models\\Selfdevelopment\\Selfdevelopment', 2, '4RgtBUSHthd7u2WeyLZfc3q3A3RxhNzHKDRRVQnDm1hTmQVw3OfkCA2NB6IAAe8TS1Xo8bSReYochZd9', '2018-12-10 07:06:51'),
(23, 'App\\Models\\Selfdevelopment\\Selfdevelopment', 2, '4RgtBUSHthd7u2WeyLZfc3q3A3RxhNzHKDRRVQnDm1hTmQVw3OfkCA2NB6IAAe8TS1Xo8bSReYochZd9', '2018-12-10 07:09:59'),
(24, 'App\\Models\\Selfdevelopment\\Selfdevelopment', 6, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 08:09:59'),
(25, 'App\\Models\\Selfenhancement\\Selfenhancement', 23, 'pk61Nf6JgAUrTmogWt9llqrHOKqUutYMS4yNw236zzzBIDD84U3z1WqsG71COh1SgIAZpsMIVASv1bOh', '2018-12-10 08:50:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_map_categories`
--
ALTER TABLE `blog_map_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_map_categories_blog_id_index` (`blog_id`),
  ADD KEY `blog_map_categories_category_id_index` (`category_id`);

--
-- Indexes for table `blog_map_tags`
--
ALTER TABLE `blog_map_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_map_tags_blog_id_index` (`blog_id`),
  ADD KEY `blog_map_tags_tag_id_index` (`tag_id`);

--
-- Indexes for table `blog_tags`
--
ALTER TABLE `blog_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_commentable_id_commentable_type_index` (`commentable_id`,`commentable_type`),
  ADD KEY `comments_commented_id_commented_type_index` (`commented_id`,`commented_type`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_type_id_foreign` (`type_id`),
  ADD KEY `history_user_id_foreign` (`user_id`);

--
-- Indexes for table `history_types`
--
ALTER TABLE `history_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kawanahlis`
--
ALTER TABLE `kawanahlis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kawan_ahli_user_id_foreign` (`user_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `networks`
--
ALTER TABLE `networks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_foreign` (`user_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_page_slug_unique` (`page_slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `selfdevelopments`
--
ALTER TABLE `selfdevelopments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selfenhancements`
--
ALTER TABLE `selfenhancements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `views`
--
ALTER TABLE `views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `views_viewable_type_viewable_id_index` (`viewable_type`,`viewable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_map_categories`
--
ALTER TABLE `blog_map_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_map_tags`
--
ALTER TABLE `blog_map_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_tags`
--
ALTER TABLE `blog_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `history_types`
--
ALTER TABLE `history_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kawanahlis`
--
ALTER TABLE `kawanahlis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `networks`
--
ALTER TABLE `networks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `selfdevelopments`
--
ALTER TABLE `selfdevelopments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `selfenhancements`
--
ALTER TABLE `selfenhancements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `views`
--
ALTER TABLE `views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
