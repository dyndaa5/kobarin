<?php
return [
	"pages" => [
	"table" => "pages",
	],
	"email_templates" => [
	"table" => "email_templates",
	"placeholders_table" => "email_template_placeholders",
	"types_table" => "email_template_types",
	],
	"blog_tags" => [
	"table" => "blog_tags",
	],
	"blog_categories" => [
	"table" => "blog_categories",
	],
	"blogs" => [
	"table" => "blogs",
	],
	"faqs" => [
	"table" => "faqs",
	],
	"selfdevelopments" => [
	"table" => "selfdevelopments",
	],
	"selfenhancements" => [
	"table" => "selfenhancements",
	],
	"kawanahlis" => [
	"table" => "kawanahlis",
	],
	"agendas" => [
	"table" => "agenda",
	],
	"banners" => [
	"table" => "banners",
	],
	"networks" => [
	"table" => "networks",
	],
	"categories" => [
	"table" => "categories",
	],
	"newsletters" => [
	"table" => "newsletters",
	],
	"videos" => [
	"table" => "videos",
	],
];