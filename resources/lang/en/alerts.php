<?php
return [
	"backend" => [
	"roles" => [
	"created" => "The role was successfully created.",
	"deleted" => "The role was successfully deleted.",
	"updated" => "The role was successfully updated.",
	],
	"permissions" => [
	"created" => "The permission was successfully created.",
	"deleted" => "The permission was successfully deleted.",
	"updated" => "The permission was successfully updated.",
	],
	"users" => [
	"confirmation_email" => "A new confirmation e-mail has been sent to the address on file.",
	"created" => "The user was successfully created.",
	"deleted" => "The user was successfully deleted.",
	"deleted_permanently" => "The user was deleted permanently.",
	"restored" => "The user was successfully restored.",
	"session_cleared" => "The user's session was successfully cleared.",
	"updated" => "The user was successfully updated.",
	"updated_password" => "The user's password was successfully updated.",
	],
	"pages" => [
	"created" => "The Page was successfully created.",
	"deleted" => "The Page was successfully deleted.",
	"updated" => "The Page was successfully updated.",
	],
	"blogcategories" => [
	"created" => "The Blog Category was successfully created.",
	"deleted" => "The Blog Category was successfully deleted.",
	"updated" => "The Blog Category was successfully updated.",
	],
	"blogtags" => [
	"created" => "The Blog Tag was successfully created.",
	"deleted" => "The Blog Tag was successfully deleted.",
	"updated" => "The Blog Tag was successfully updated.",
	],
	"blogs" => [
	"created" => "The Blog was successfully created.",
	"deleted" => "The Blog was successfully deleted.",
	"updated" => "The Blog was successfully updated.",
	],
	"settings" => [
	"updated" => "The Setting was successfully updated.",
	],
	"faqs" => [
	"created" => "The Faq was successfully created.",
	"deleted" => "The Faq was successfully deleted.",
	"updated" => "The Faq was successfully updated.",
	],
	"menus" => [
	"created" => "The Menu was successfully created.",
	"deleted" => "The Menu was successfully deleted.",
	"updated" => "The Menu was successfully updated.",
	],
	"selfdevelopments" => [
	"created" => "The Selfdevelopment was successfully created.",
	"deleted" => "The Selfdevelopment was successfully deleted.",
	"updated" => "The Selfdevelopment was successfully updated.",
	],
	"selfenhancements" => [
	"created" => "The Selfenhancement was successfully created.",
	"deleted" => "The Selfenhancement was successfully deleted.",
	"updated" => "The Selfenhancement was successfully updated.",
	],
	"kawanahlis" => [
	"created" => "The Kawanahli was successfully created.",
	"deleted" => "The Kawanahli was successfully deleted.",
	"updated" => "The Kawanahli was successfully updated.",
	],
	"agendas" => [
	"created" => "The Agenda was successfully created.",
	"deleted" => "The Agenda was successfully deleted.",
	"updated" => "The Agenda was successfully updated.",
	],
	"banners" => [
	"created" => "The Banner was successfully created.",
	"deleted" => "The Banner was successfully deleted.",
	"updated" => "The Banner was successfully updated.",
	],
	"networks" => [
	"created" => "The Network was successfully created.",
	"deleted" => "The Network was successfully deleted.",
	"updated" => "The Network was successfully updated.",
	],
	"categories" => [
	"created" => "The Category was successfully created.",
	"deleted" => "The Category was successfully deleted.",
	"updated" => "The Category was successfully updated.",
	],
	"newsletters" => [
	"created" => "The Newsletter was successfully created.",
	"deleted" => "The Newsletter was successfully deleted.",
	"updated" => "The Newsletter was successfully updated.",
	],
	"videos" => [
	"created" => "The Video was successfully created.",
	"deleted" => "The Video was successfully deleted.",
	"updated" => "The Video was successfully updated.",
	],
	"homepages" => [
	"created" => "The Homepage was successfully created.",
	"deleted" => "The Homepage was successfully deleted.",
	"updated" => "The Homepage was successfully updated.",
	],
	],
];