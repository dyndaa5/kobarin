(function($) { "use strict";
/*BEGIN  OF JS DOCUMENT*/


//DOCUMENT READY
  $(document).ready(function(){

    // lazyload
      $('.lazy').Lazy({effect: 'fadeIn'});

    // sticky stuff
      var sticky = new Sticky('.sticky-sidebar');

    // social icons
      weart_social();

    // video carosuel section
      weart_video_carosuel();

    // polualr carosuel section
      weart_popular_carosuel();

    // menu with expand button
      weart_expand_menu();

    // menu indicator
      weart_menu_indicator();

    // go to top button
      weart_goToTop();

    // embed hack
      weart_twitter_embed_hack();

    // featured-logo
      //weart_featuredLogo();

    // infinite scroll
      weart_infinitScroll();

    // desktop bootstrap menu
      weart_bootstrap_menu();

  }); //end of document ready


//FUNCTIONS
  function weart_social(){
    var vmi = '';
    $(".social-channels li a").on().each(function(){
      vmi = $(this).text();
      if(  vmi === "500px" || vmi === "adn" || vmi === "amazon" || vmi === "android" || vmi === "angellist" || vmi === "apple" || vmi === "bandcamp" || vmi === "behance" || vmi === "bitbucket" || vmi === "btc" || vmi === "black-tie" || vmi === "bluetooth" || vmi === "buysellads" || vmi === "cc-amex" || vmi === "cc-diners-club" || vmi === "cc-discover" || vmi === "cc-jcb" || vmi === "cc-mastercard" || vmi === "cc-paypal" || vmi === "cc-stripe" || vmi === "cc-visa" || vmi === "chrome" || vmi === "codepen" || vmi === "codiepie" || vmi === "connectdevelop" || vmi === "contao" || vmi === "css3" || vmi === "dashcube" || vmi === "delicious" || vmi === "deviantart" || vmi === "digg" || vmi === "dribbble" || vmi === "dropbox" || vmi === "drupal" || vmi === "edge" || vmi === "eercast" || vmi === "empire" || vmi === "envira" || vmi === "etsy" || vmi === "expeditedssl" || vmi === "font-awesome" || vmi === "facebook" || vmi === "firefox" || vmi === "first-order" || vmi === "flickr" || vmi === "font-awesome" || vmi === "fonticons" || vmi === "fort-awesome" || vmi === "forumbee" || vmi === "foursquare" || vmi === "free-code-camp" || vmi === "get-pocket" || vmi === "gg" || vmi === "git" || vmi === "github" || vmi === "gitlab" || vmi === "glide" || vmi === "google" || vmi === "google-plus" || vmi === "google-wallet" || vmi === "gratipay" || vmi === "grav" || vmi === "hacker-news" || vmi === "houzz" || vmi === "html5" || vmi === "imdb" || vmi === "instagram" || vmi === "internet-explorer" || vmi === "ioxhost" || vmi === "joomla" || vmi === "jsfiddle" || vmi === "lastfm" || vmi === "leanpub" || vmi === "linkedin" || vmi === "linode" || vmi === "linux" || vmi === "maxcdn" || vmi === "meanpath" || vmi === "medium" || vmi === "meetup" || vmi === "mixcloud" || vmi === "modx" || vmi === "odnoklassniki" || vmi === "opencart" || vmi === "openid" || vmi === "opera" || vmi === "optin-monster" || vmi === "pagelines" || vmi === "paypal" || vmi === "pied-piper" || vmi === "pinterest" || vmi === "product-hunt" || vmi === "qq" || vmi === "quora" || vmi === "ravelry" || vmi === "rebel" || vmi === "reddit" || vmi === "renren" || vmi === "rebel" || vmi === "safari" || vmi === "scribd" || vmi === "sellsy" || vmi === "shirtsinbulk" || vmi === "simplybuilt" || vmi === "skyatlas" || vmi === "skype" || vmi === "slack" || vmi === "slideshare" || vmi === "snapchat" || vmi === "soundcloud" || vmi === "spotify" || vmi === "stack-exchange" || vmi === "stack-overflow" || vmi === "steam" || vmi === "steam-square" || vmi === "stumbleupon" || vmi === "superpowers" || vmi === "telegram" || vmi === "tencent-weibo" || vmi === "themeisle" || vmi === "trello" || vmi === "tripadvisor " || vmi === "tumblr" || vmi === "twitch" || vmi === "twitter" || vmi === "usb" || vmi === "viacoin" || vmi === "viadeo" || vmi === "vimeo" || vmi === "vine" || vmi === "vk" || vmi === "wechat " || vmi === "weibo" || vmi === "whatsapp" || vmi === "wikipedia-w" || vmi === "windows" || vmi === "wordpress" || vmi === "wpbeginner" || vmi === "wpexplorer" || vmi === "wpforms" || vmi === "xing" || vmi === "yahoo" || vmi === "yelp" || vmi === "yoast" || vmi === "youtube" ){
        $(this).empty();
        $(this).prepend('<i class="fab fa-'+vmi+'"></i>');
      }else{
        $(this).parent().remove();
      }
    });
    $("#main-menu .social-channels").addClass('d-md-inline-block');
    return false;
  }// END OF weart_social()

  function weart_video_carosuel(){
    var video = $('.section-video');

    for(var i=0; i<=video.length; i++){
      video.eq(i).attr('id', 'video_'+i);
      video.eq(i).find('.carousel-indicators li').attr('data-target', '#video_'+i);
      video.eq(i).find('.carousel-control-prev,.carousel-control-next').attr('href', '#video_'+i);
    }

    return false;
  }// END OF weart_video_carosuel()

  function weart_popular_carosuel(){
    var popular = $('.section-popular');

    for(var i=0; i<=popular.length; i++){
      popular.eq(i).attr('id', 'popular_'+i);
      popular.eq(i).find('.carousel-control-prev,.carousel-control-next').attr('href', '#popular_'+i);
    }

    return false;
  }// END OF weart_popular_carosuel()

  function weart_expand_menu(){
    $('.widget_nav_menu ul ul, #mobile-menu ul ul').on().each(function() {
        if($(this).children().length){
            $(this,'li:first').parent().append('<a class="expand right" href="#"><i class="fa fa-plus" aria-hidden="true"></i></a>');
        }
    });
    $('.widget_nav_menu li a.expand, #mobile-menu ul li a.expand').on("click",function(e){
      e.preventDefault();
      if ($(this).hasClass("clicked")) {
          $(this).find('i').removeClass('fa-minus').addClass('fa-plus');
          $(this).prev('ul').slideUp(300, function(){});
          $(this).parent('li').removeClass('active');
      } else {
          $(this).find('i').removeClass('fa-plus').addClass('fa-minus');
          $(this).prev('ul').slideDown(300, function(){});
          $(this).parent('li').addClass('active');
      }
      $(this).toggleClass("clicked");
    });
    return false;
  }/*end of weart_expand_menu()*/

  function weart_menu_indicator(){
    $('#left-bars').on("click",function(){
      $('#mobile-menu').toggle(100);
      $('.main').toggleClass('blur');
      $(this).find('i').toggleClass('fa-bars fa-times');
    });
    $('#right-bars').on("click",function(){
      $('#right-mobile-menu').toggle(100);
      $('.main').toggleClass('blur');
      $(this).find('i').toggleClass('fa-bars fa-times');
    });
    $('#search').on("click",function(){
      $('#search-menu').toggle(100);
      $('.main').toggleClass('blur');
      $(this).find('i').toggleClass('fa-search fa-times');
    });
    return false;
  }/*end of weart_menu_indicator()*/

  function weart_goToTop(){
    $('body').append('<div id="toTop" class="btn bg-black bgh-1 c-white"><span class="fas fa-angle-up"></span></div>');
      $(window).scroll(function () {
      if ($(this).scrollTop() >= 200) {
        $('#toTop').fadeIn();
      } else {
        $('#toTop').fadeOut();
      }
    });
    $('#toTop').on("click",function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
  }/*end of weart_goToTop()*/

  function weart_twitter_embed_hack(){
    $(".post-content .twitter-tweet").parent().removeClass("embed-responsive-16by9");
    $(".post-content #fb-root").parent().removeClass("embed-responsive-16by9 embed-responsive").addClass("mw-100 pb-3");
    $(".post-content .instagram-media").parent().removeClass("embed-responsive-16by9 embed-responsive");
  }/*end of weart_twitter_embed_hack()*/

  function weart_featuredLogo(){
    // target the logo
    var headerhome = $("#main-menu .logo");
    if( $('.home .section-featured').hasClass('featured-logo') ){

      // hide from default
        headerhome.hide().removeClass('d-none');

      // show for scroll
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 130) {
          headerhome.slideDown(150);
        } else {
          headerhome.hide();
        }
      });//scroll

    }else{headerhome.removeClass('d-none');}
  }/* end of weart_featuredLogo() */

  function weart_infinitScroll(){
    $(".infinite-scroll").infinitescroll({
      navSelector: ".infinite-nav",
      nextSelector: ".infinite-nav a:first",
      itemSelector: ".infinite-post",
      errorCallback: function(){ $(".infinite-more").css("display", "none");  }
    });

    $(window).unbind(".infscr");
    $(".infinite-more").on('click',function(){
      $(".infinite-scroll").infinitescroll("retrieve");
      return false;
    });

    $(window).load(function(){
      if ($(".infinite-nav a").length) {
        $(".infinite-more").css("display","inline-block");
      } else {
        $(".infinite-more").css("display","none");
      }
    });
  }/* end of weart_infiniteScroll() */

  // bootstrap menu
  function weart_bootstrap_menu(){

    var navbar = $('#desktop-menu.navbar-nav');

    navbar.find('li').addClass('nav-item');
    navbar.find('li a').addClass('nav-link');

    // dropdown
      navbar.find('.menu-item-has-children').addClass('dropdown').children('a').addClass('dropdown-toggle');
      navbar.find('.sub-menu').addClass('dropdown-menu f-text');
      navbar.find('.sub-menu .menu-item').addClass('dropdown-item').removeClass('nav-item');

    // showing navbar
      navbar.addClass('d-lg-flex');

  }// end of FUNCTION

/*END OF JS DOCUMENT*/
})(jQuery);
