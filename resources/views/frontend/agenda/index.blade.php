@extends('frontend.layouts.app')
@section('content')
	<div class="section-featured section mb-5 pt-1">
		<div class="container-fluid main-sidebar mt-3">
			<div class="row">
				<div class="col-lg-12">
					<div class="itemInner zoom post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political" style="max-height: 350px;">
						<div class="bg lazy bg-30 pb-1" style="display: block; background-image: url({{ Storage::disk('public')->url('img/featurd-image/' . $banner->featured_image) }});"></div>
					</div>
				</div>
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div>
	<div class="section-featured section mb-5 pt-1 pb-5" style="height: auto;">
		<div class="container-fluid main-sidebar mt-3">
			<div class="row">
	      <div class="col-md-4">
	      </div>
	      <div class="col-md-4">
	        <ul class="nav nav-pills nav-fill navtop" style="max-width: 400px">
	              <li class="nav-item">
	                  <a class="nav-link active" href="#menu1" data-toggle="tab">Internal</a>
	              </li>
	              <li class="nav-item">
	                  <a class="nav-link" href="#menu2" data-toggle="tab">Eksternal</a>
	              </li>

	          </ul>
	      </div>
	      <div class="col-md-4">
	      </div>
	    </div>
		    <div class="tab-content">
		        <div class="tab-pane active" role="tabpanel" id="menu1">
							<div class="row pt-5">
								@foreach($items_internal as $item)
									<div class="col-md-4">
										<div class="itemInner zoom post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">
											<div class="bg lazy bg-100" style="display: block; background-image: url({{ Storage::disk('public')->url('img/featurd-image/' . $item->featured_image) }});"></div>
											<a class="coverLink" href="{{ url('/agenda') }}/{{ $item->id }}" title="{{ $item->title }}"></a>

										</div>
										<div class="title l-white c-white pt-4">
											<div class="d-none d-md-inline-block">
												<div class="cat-badge f-main border small bgh-1">
													<a href="#" rel="category tag">{{ date('d-M-Y', strtotime($item->created_at)) }} @if(isset($item->places)) @ {{$item->places}} @else  @endif</a>
												</div>
											</div>
											<div class="d-none d-md-inline-block">
												<div class="cat-badge f-main border small bgh-1">
													<a href="#" rel="category tag">
														<i class="fa fa-eye"></i> {{ $item->getViews() }}
													</a>
												</div>
											</div>

											<a class="" href="{{ url('/agenda') }}/{{ $item->id }}" title="{{ $item->name }}"><h6 class="c-black">{{ $item->name }}</h6></a>
										</div>
									</div>
								@endforeach
							</div>
						</div>
						<div class="tab-pane" role="tabpanel" id="menu2">
							<div class="row pt-5">
								@foreach($items_eksternal as $item)
									<div class="col-md-4">
										<div class="itemInner zoom post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">
											<div class="bg lazy bg-100" style="display: block; background-image: url({{ Storage::disk('public')->url('img/featurd-image/' . $item->featured_image) }});"></div>
											<a class="coverLink" href="{{ url('/agenda') }}/{{ $item->id }}" title="{{ $item->title }}"></a>

										</div>
										<div class="title l-white c-white pt-4">
											<div class="d-none d-md-inline-block">
												<div class="cat-badge f-main border small bgh-1">
													<a href="#" rel="category tag">{{ date('d-M-Y', strtotime($item->created_at)) }} @if(isset($item->places)) @ {{$item->places}} @else  @endif</a>
												</div>
											</div>
											<div class="d-none d-md-inline-block">
												<div class="cat-badge f-main border small bgh-1">
													<a href="#" rel="category tag">
														<i class="fa fa-eye"></i> {{ $item->getViews() }}
													</a>
												</div>
											</div>

											<a class="" href="{{ url('/agenda') }}/{{ $item->id }}" title="{{ $item->name }}"><h6 class="c-black">{{ $item->name }}</h6></a>
										</div>
									</div>
								@endforeach
							</div>

						</div>
		    </div>
		</div>
	</div>
@endsection
