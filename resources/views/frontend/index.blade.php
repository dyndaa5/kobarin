@extends('frontend.layouts.app')
@section('content')
	<div class="section-featured section mb-5 pt-1">
		<div class="container-fluid main-sidebar">
			<div class="row">
				<div class="col-lg-12">
					<div class="itemInner zoom post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political">
						<div class="bg lazy bg-55 pb-1" style="display: block; background-image: url(/storage/img/featurd-image/{{ $items_sd[0]->featured_image }});"></div>
						<div class="by-badge c-white l-white topLeft d-none d-lg-inline-block small d-flex align-items-center">
							<a href="/author/{{ $items_sd[0]->user->id }}" rel="author" title="Posts by {{ $items_sd[0]->user->first_name . ' ' . $items_sd[0]->user->last_name }}"><img alt="" style="width: 32px;" class="rounded-circle mr-1" src="/storage/img/photo/{{ $items_sd[0]->user->photo }}"></a> <span class="author">By <a href="/author/4" rel="author" title="Posts by {{ $items_sd[0]->user->first_name . ' ' . $items_sd[0]->user->last_name }}">{{ $items_sd[0]->user->first_name . ' ' . $items_sd[0]->user->last_name }}</a></span>
						</div><a class="coverLink" href="/self-development/{{ $items_sd[0]->id }}" title="{{ $items_sd[0]->name }}"></a>
						<div class="title l-white c-white">
							<div class="d-none d-md-inline-block">
								<div class="cat-badge f-main border small bgh-1">
									<a href="#" rel="category tag">Self Development</a>
								</div>
							</div>
							<h2 class="h1">{{ $items_sd[0]->name }}</h2>
						</div>
					</div>
				</div>
				<div class="row" id="sub-banner"></div>
				@for ($i = 0; $i <= 2; $i++)
					@if(!empty($items_se[$i]))
						<div class="col-lg-4 sub-banner-item">
							<div class="row">
								<div class="col-lg-12 col-md-6">
									<div class="itemInner zoom post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">
										<div class="bg lazy bg-55" style="display: block; background-image: url(/storage/img/featurd-image/{{$items_se[$i]->featured_image }});"></div><a class="coverLink" href="/self-enhancement/{{ $items_se[$i]->id }}" title="{{ $items_se[$i]->name }}"></a>
										<div class="title l-white c-white">
											<div class="d-none d-md-inline-block">
												<div class="cat-badge f-main border small bgh-1">
													<a href="#" rel="category tag">Self Empowerment</a>
												</div>
											</div>
											<h2 class="h4">{{ $items_se[$i]->name }}</h2>
										</div>
									</div>
								</div>
							</div><!-- .row -->
						</div><!-- .col-lg-3 -->
					@endif
				@endfor
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div>
	<div class="container mt-5">
		<div class="row justify-content-center">
			<div class="col-lg-12 c-black mb-2">
				<div class="sectionTitle">
					<a href="/self-development"><span class="bg-white h4">Self Development</span></a>
					<div class="sub-section">
						<div class="text-magenta">Mengembangkan diri untuk meraih mimpi indah</div>
					</div>
				</div>
				<div class="sectionKategori">
					<span class="text-blue h6  text-nowrap">Kategori Terkait:</span>
					<div class="sub-kategori">
						<div class="text-magenta">Inovasi, Produktivitas, Spiritual, Teknologi, Hubungan, Komunikasi, Gaya Hidup</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section-proposal container mb-5 mt-3 section">
		<!-- first row -->
		<div class="row">
			<div class="col-md-6">
				<div class="itemInner zoom post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">
					<div class="bg lazy bg-65" style="display: block; background-image: url(/storage/img/featurd-image/{{$items_sd[0]->featured_image }});"></div>
					<div class="by-badge c-white l-white topLeft d-none d-lg-inline-block small d-flex align-items-center">
						<a href="/author/{{ $items_sd[0]->user->id }}" rel="author" title="Posts by {{ $items_sd[0]->user->first_name . ' ' . $items_sd[0]->user->last_name }}"><img alt="" style="width: 32px;" class="rounded-circle mr-1" src="/storage/img/photo/{{ $items_sd[0]->user->photo }}"></a> <span class="author">By <a href="/author/{{ $items_sd[0]->user->id }}" rel="author" title="Posts by {{ $items_sd[0]->user->first_name . ' ' . $items_sd[0]->user->last_name }}">{{ $items_sd[0]->user->first_name . ' ' . $items_sd[0]->user->last_name }}</a>
						</span>
					</div>
					<a class="coverLink" href="/self-development/{{ $items_sd[0]->id }}" title="{{ $items_sd[0]->name }}"></a>
					<div class="title l-white c-white">
						<div class="d-none d-md-inline-block">
							<div class="cat-badge f-main border small bgh-1">
								<a href="#" rel="category tag">SELF DEVELOPMENT</a>
							</div>
						</div>
						<h2 class="h3">{{ $items_sd[0]->name }}</h2>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					@for ($i = 1; $i <= 2; $i++)
						@if(!empty($items_sd[$i]))
							<div class="col-lg-6 col-md-6">
								<div class="itemList row align-items-center post-399 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-ai tag-google tag-office">
									<div class="col-3 col-md-3">
										<div class="bg lazy bg-100" style="display: block; background-image: url(/storage/img/featurd-image/{{$items_sd[$i]->featured_image }});">
											<a class="coverLink" href="/self-development/{{ $items_sd[$i]->id }}" title="{{ $items_sd[$i]->name }}"></a>
										</div>
									</div>
									<div class="col">
										<h2 class="l-black lh-1 h6 f-main"><a href="/self-development/{{ $items_sd[$i]->id }}" title="{{ $items_sd[$i]->name }}">{{ $items_sd[$i]->name }}</a></h2>
									</div>
								</div>
							</div>
						@endif
					@endfor
				</div>
				<div class="row">
					@for ($i = 3; $i <= 4; $i++)
						@if(!empty($items_sd[$i]))
							<div class="col-lg-6 col-md-6">
								<div class="itemList row align-items-center post-399 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-ai tag-google tag-office">
									<div class="col-3 col-md-3">
										<div class="bg lazy bg-100" style="display: block; background-image: url(/storage/img/featurd-image/{{$items_sd[$i]->featured_image }});">
											<a class="coverLink" href="/self-development/{{ $items_sd[$i]->id }}" title="{{ $items_sd[$i]->name }}"></a>
										</div>
									</div>
									<div class="col">
										<h2 class="l-black lh-1 h6 f-main"><a href="/self-development/{{ $items_sd[$i]->id }}" title="{{ $items_sd[$i]->name }}">{{ $items_sd[$i]->name }}</a></h2>
									</div>
								</div>
							</div>
						@endif
					@endfor
				</div>
				<div class="row">
					@for ($i = 5; $i <= 6; $i++)
						@if(!empty($items_sd[$i]))
							<div class="col-lg-6 col-md-6">
								<div class="itemList row align-items-center post-399 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-ai tag-google tag-office">
									<div class="col-3 col-md-3">
										<div class="bg lazy bg-100" style="display: block; background-image: url(/storage/img/featurd-image/{{$items_sd[$i]->featured_image }});">
											<a class="coverLink" href="/self-development/{{ $items_sd[$i]->id }}" title="{{ $items_sd[$i]->name }}"></a>
										</div>
									</div>
									<div class="col">
										<h2 class="l-black lh-1 h6 f-main"><a href="/self-development/{{ $items_sd[$i]->id }}" title="{{ $items_sd[$i]->name }}">{{ $items_sd[$i]->name }}</a></h2>
									</div>
								</div>
							</div>
						@endif
					@endfor
				</div>
				<div class="row">
					@for ($i = 7; $i <= 8; $i++)
						@if(!empty($items_sd[$i]))
							<div class="col-lg-6 col-md-6">
								<div class="itemList row align-items-center post-399 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-ai tag-google tag-office">
									<div class="col-3 col-md-3">
										<div class="bg lazy bg-100" style="display: block; background-image: url(/storage/img/featurd-image/{{$items_sd[$i]->featured_image }});">
											<a class="coverLink" href="/self-development/{{ $items_sd[$i]->id }}" title="{{ $items_sd[$i]->name }}"></a>
										</div>
									</div>
									<div class="col">
										<h2 class="l-black lh-1 h6 f-main"><a href="/self-development/{{ $items_sd[$i]->id }}" title="{{ $items_sd[$i]->name }}">{{ $items_sd[$i]->name }}</a></h2>
									</div>
								</div>
							</div>
						@endif
					@endfor
				</div>
			</div>
		</div>
	</div>
	<div class="break-line mw-1080"></div>
	<div class="quotes">
		<h3 class="text-blue fw-700">Misi kami adalah Meningkatkan Kualitas Hidup Masyarakat<br />Melalui Edukasi dan Praktik Konseling</h3>
	</div>
  <div class="break-line mw-1080"></div>
	<div class="container mt-5">
		<div class="row justify-content-center">
			<div class="col-lg-12 c-black mb-2">
				<div class="sectionTitle">
					<a href="/self-enhancement"><span class="bg-white h4">Self Empowerment</span></a>
					<div class="sub-section">
						<div class="text-magenta">Meningkatkan diri dengan mengenali masalah psikologis</div>
					</div>
				</div>
				<div class="sectionKategori">
					<span class="text-blue h6 text-nowrap">Kategori Terkait:</span>
					<div class="sub-kategori">
						<div class="text-magenta">Psikologis, Solusi, Pencegahan, Rekomendasi Produk, Kutipan Favorit</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section-proposal container mb-5 mt-3 pb-5 section">
		<!-- first row -->
		<div class="row">
			<div class="col-md-6">
				<div class="itemInner zoom post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">
					<div class="bg lazy bg-65" style="display: block; background-image: url(/storage/img/featurd-image/{{ $items_se[0]->featured_image }});"></div>
					<div class="by-badge c-white l-white topLeft d-none d-lg-inline-block small d-flex align-items-center">
						<a href="/author/{{ $items_se[0]->user->id }}" rel="author" title="Posts by {{ $items_se[0]->user->first_name . ' ' . $items_se[0]->user->last_name }}"><img alt="" style="width: 32px;" class="rounded-circle mr-1" src="/storage/img/photo/{{ $items_se[0]->user->photo }}"></a> <span class="author">By <a href="/author/{{ $items_se[0]->user->id }}" rel="author" title="Posts by {{ $items_se[0]->user->first_name . ' ' . $items_se[0]->user->last_name }}">{{ $items_se[0]->user->first_name . ' ' . $items_se[0]->user->last_name }}</a></span>
					</div><a class="coverLink" href="/self-enhancement/{{ $items_se[0]->id }}" title="{{ $items_se[0]->name }}"></a>
					<div class="title l-white c-white">
						<div class="d-none d-md-inline-block">
							<div class="cat-badge f-main border small bgh-1">
								<a href="#" rel="category tag">SELF EMPOWERMENT</a>
							</div>
						</div>
						<h2 class="h3">{{ $items_se[0]->name }}</h2>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					@for ($i = 1; $i <= 10; $i++)
						@if(!empty($items_se[$i]))
							<div class="col-lg-6 col-md-6">
								<div class="itemList row align-items-center post-399 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-ai tag-google tag-office">
									<div class="col-3 col-md-3">
										<div class="bg lazy bg-100" style="display: block; background-image: url(/storage/img/featurd-image/{{$items_se[$i]->featured_image }});">
											<a class="coverLink" href="/self-enhancement/{{ $items_se[$i]->id }}" title="{{ $items_se[$i]->name }}"></a>
										</div>
									</div>
									<div class="col">
										<h2 class="l-black lh-1 h6 f-main"><a href="/self-enhancement/{{ $items_se[$i]->id }}" title="{{ $items_se[$i]->name }}">{{ $items_se[$i]->name }}</a></h2>
									</div>
								</div>
							</div>
						@endif
					@endfor
				</div>
			</div>
		</div>
	</div>

	<div class="container mt-5">
	  <div class="row justify-content-center">
	    <div class="col-lg-12 c-black mb-2">
	      <div class="sectionTitle">
	        <a href="/videos"><span class="bg-white h4">Videos</span></a>
	        <div class="sub-section">
	          <div class="text-magenta">Integrasi antara kekuatan ide dan gambar untuk pemahaman yang menyenangkan</div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="section-proposal container mb-5 mt-3 pb-5 section text-center">
		<div class="row">
			@for ($i = 0; $i <= 2; $i++)
				@if(!empty($items_videos[$i]))
					{{ $items_videos[$i]->video}}
					<div class="col-md-4">
						<div class="itemInner zoom post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">
							<div class="bg lazy bg-65" style="display: block; background-image: url(/storage/img/featurd-image/{{ $items_videos[$i]->featured_image}});"></div>
							<a class="coverLink" href="/videos/{{ $items_videos[$i]->id }}" title="{{ $items_videos[$i]->name }}"></a>
							<div class="title l-white c-white">
								<div class="d-none d-md-inline-block">

								</div>
								<h2 class="h3">{{ substr($items_videos[$i]->name, 0, 20) }}</h2>
							</div>
						</div>
					</div>
				@endif
			@endfor
		</div>
		<!-- <img src="/images/coming-soon.png" /> -->
	</div>

	<div class="container mt-5">
	  <div class="row justify-content-center">
	    <div class="col-lg-12 c-black mb-2">
	      <div class="sectionTitle">
					<a href="/daftar-ahli"><span class="bg-white h4">Kawan Ahli</span></a>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="section-proposal container mb-5 mt-3 pb-5 section">
	  <div class="row">
	    <div class="col-md-6">

	      <div class="sectionAhliTitle vl">
	        <span class="h4 text-magenta">Edukasi</span>
	        <div class="break-line width-25"></div>
	      </div>

	      <div class="ahli-container">
	        @foreach($ahli_edukasi as $item)
					<div class="col-md-12 text-center">
						<div class="row">
							<div class="col-md-4">
							</div>
							<div class="col-md-4">
								<div class="itemList profile-rounded row align-items-center  post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political">
			                        <div class="col-12">
			                <div class="bg-100 rounded-circle" style="display: block; background-image: url({{ Storage::disk('public')->url('img/photo/' . $item->photo) }});background-size: 190px;background-position: left 50% top 50%;">
			                      <a href="{{ url('/author')}}/{{ $item->id }}" title="{{ $item->fullname }}" class="coverLink"></a>
			                    </div>
			                  </div>
			                      <div class="col pt-2">
			                        <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="{{ url('/author')}}/{{ $item->title }}">{{ $item->first_name . ' ' . $item->last_name }}</a></h2>
			                        <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="Type">{{ $item->jabatan }}</a></h2>
			                        <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="Type">{{ $item->institusi }}</a></h2>
			                      </div>
			              </div>


							</div>
							<div class="col-md-4">
							</div>

	        </div>
				</div>

	        @endforeach
	      </div>
	    </div>
	    <div class="col-md-6">
	      <div class="sectionAhliTitle">
	        <span class="h4 text-magenta">Konseling</span>
	        <div class="break-line width-25"></div>
	      </div>

				<div class="ahli-container">
	        @foreach($ahli_konseling as $item)
					<div class="col-md-12 text-center">
						<div class="row">
							<div class="col-md-4">
							</div>
							<div class="col-md-4">
								<div class="itemList profile-rounded row align-items-center  post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political">
			                        <div class="col-12">
			                <div class="bg-100 rounded-circle" style="display: block; background-image: url({{ Storage::disk('public')->url('img/photo/' . $item->photo) }});background-size: 190px;background-position: left 50% top 50%;">
			                      <a href="{{ url('/author')}}/{{ $item->id }}" title="{{ $item->fullname }}" class="coverLink"></a>
			                    </div>
			                  </div>
			                      <div class="col pt-2">
			                        <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="{{ url('/author')}}/{{ $item->title }}">{{ $item->first_name . ' ' . $item->last_name }}</a></h2>
			                        <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="Type">{{ $item->jabatan }}</a></h2>
			                        <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="Type">{{ $item->institusi }}</a></h2>
			                      </div>
			              </div>


							</div>
							<div class="col-md-4">
							</div>

	        </div>
				</div>

	        @endforeach
	      </div>
	    </div>
	  </div>


		@if(count($ahli_edukasi) > 2 || count($ahli_konseling) > 2)
			<div class="sectionAhliTitle text-center">
				<div class="row">
					<div class="col-md-4">
					</div>

					<div class="col-md-4 pt-5">
						<a href="{{ url('/daftar-ahli')}}"><h5>Tampilkan Lebih</h5></a>
					</div>

					<div class="col-md-4">
					</div>

				</div>
			</div>
		@endif

	</div>

	<div class="container mt-5">
	  <div class="row justify-content-center">
	    <div class="col-lg-12 c-black mb-2">
	      <div class="sectionTitle">
					<a href="/agenda"><span class="bg-white h4">Agenda Kegiatan</span></a>
	        <div class="sub-section">
	          <div class="text-magenta">Berilmu dalam pertemuan untuk pengetahuan dan jaringan relasi</div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="section-proposal container mb-5 mt-3 pb-5 section">
	  <div class="row">
	    <div class="col-md-6">

	      <div class="sectionAhliTitle vl">
	        <span class="h4 text-magenta">Internal</span>
	        <div class="break-line width-25"></div>
	      </div>

	      <div class="agenda-container">
	        <div class='row'>
	          @foreach($agenda_internal as $item)
	          <div class="col-md-12">
	            <div class="itemList row align-items-center post-399 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-ai tag-google tag-office">
	              <div class="col-3 col-md-3">
	                <div class=" lazy bg-100" style="display: block; background-image: url({{ Storage::disk('public')->url('img/featurd-image/' . $item->featured_image) }});background-size: 141px;background-position: left 50% top 50%;">
	                  <a class="coverLink" href="{{ url('/agenda')}}/{{ $item->id }}" title="{{ $item->title }}"></a>
	                </div>
	              </div>
	              <div class="col">
	                <h2 class="l-black lh-1 h6 f-main"><a href="{{ url('/agenda')}}/{{ $item->id }}" title="{{ $item->name }}">{{ $item->name }}</a></h2>
	                <small class="text-muted"><i class="fa fa-eye"></i> {{ $item->getViews() }}</small>
	              </div>
	            </div>
	          </div>
	          @endforeach
	        </div>

	      </div>
	    </div>
	    <div class="col-md-6">
	      <div class="sectionAhliTitle">
	        <span class="h4 text-magenta">Eksternal</span>
	        <div class="break-line width-25"></div>
	      </div>

	      <div class="agenda-container">
					<div class='row'>
	          @foreach($agenda_eksternal as $item)
	          <div class="col-md-12">
	            <div class="itemList row align-items-center post-399 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-ai tag-google tag-office">
	              <div class="col-3 col-md-3">
	                <div class=" lazy bg-100" style="display: block; background-image: url({{ Storage::disk('public')->url('img/featurd-image/' . $item->featured_image) }});background-size: 141px;background-position: left 50% top 50%;">
	                  <a class="coverLink" href="{{ url('/agenda')}}/{{ $item->id }}" title="{{ $item->title }}"></a>
	                </div>
	              </div>
	              <div class="col">
	                <h2 class="l-black lh-1 h6 f-main"><a href="{{ url('/agenda')}}/{{ $item->id }}" title="{{ $item->name }}">{{ $item->name }}</a></h2>
	                <small class="text-muted"><i class="fa fa-eye"></i> {{ $item->getViews() }}</small>
	              </div>
	            </div>
	          </div>
	          @endforeach
	        </div>
	      </div>
	    </div>
	  </div>

		@if(count($agenda_internal) > 3 || count($agenda_eksternal) > 3)
			<div class="sectionAhliTitle text-center">
				<div class="row">
					<div class="col-md-4">
					</div>

					<div class="col-md-4">
						<a href="{{ url('/agenda')}}"><h5>Tampilkan Lebih</h5></a>
					</div>

					<div class="col-md-4">
					</div>

				</div>
			</div>
		@endif
	</div>

@endsection
