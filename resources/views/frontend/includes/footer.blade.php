<footer class="text-white mt-auto pt-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 text-left">
                <h3>Mari Berlangganan, Gratis!</h3>
                <small style="color:black;">
                  <b>
                    Tingkatkan dan Kembangkan Dirimu Bersama Kami
                  </b>
                </small>
            </div>
            <div class="col-sm-4 text-left">
              <form id="form-newsletter-subscribe" method="post" action="/api/newsletter/subscribe">
                  {!! csrf_field() !!}
                  <div class="row">
                      <div class="form-group col-6 col-sm-7">

                          <input type="text" name="email" class="form-control d-block" placeholder="E-mail">
                      </div>
                      <div class="form-group col-12 col-sm-2">
                          <button type="submit" style="background-color: #0288bd" class="btn btn-primary btn-ajax-submit">Berlangganan
                          </button>
                      </div>
                  </div>

              </form>
            </div>
            <div class="col-sm-4 text-left social-media-container">
                <a href="{{ url('/') }}" target="_blank"><h2 class="text-white">Kobarin.id</h2></a>
                <ul class="social-network social-circle">
                     <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                     <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                     <li><a href="#" class="icoGoogle" title="Youtube"><i class="fa fa-youtube"></i></a></li>
                 </ul>
            </div>
        </div>

        <div class="row">
          <div class="col-sm-12 text-left mt-4">
            <h3>Relasi Majalah</h3>

            <div class="majalah-container d-flex justify-content-center">
              <div class="row">
                <div class="col-12">
                  <img src="https://placehold.it/102x130/FFFFFF/6292bf/?text=Segera+Hadir" class="img-fluid mr-5" />
                  <img src="https://placehold.it/102x130/FFFFFF/6292bf/?text=Segera+Hadir" class="img-fluid mr-5" />
                  <img src="https://placehold.it/102x130/FFFFFF/6292bf/?text=Segera+Hadir" class="img-fluid mr-5" />
                  <img src="https://placehold.it/102x130/FFFFFF/6292bf/?text=Segera+Hadir" class="img-fluid mr-5" />
                  <img src="https://placehold.it/102x130/FFFFFF/6292bf/?text=Segera+Hadir" class="img-fluid mr-5" />
                  <img src="https://placehold.it/102x130/FFFFFF/6292bf/?text=Segera+Hadir" class="img-fluid" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid mt-3">
          <p class="text-right float-right text-muted small">
               Copyright &copy; {{config('app.name') . ' ' . date('Y')}}
           </p>
           <div class="text-left">
               <a class="small" href="{{ url('/tentang-kami')}}"><b>Tentang Kami</b></a>
               <a class="small" href="{{ url('/privasi')}}"><b>Privasi</b> </a>
               <a class="small" href="{{ url('/syarat-dan-ketentuan')}}"><b>Syarat dan Ketentuan</b></a>
           </div>
       </div>

    </div>

</footer>
