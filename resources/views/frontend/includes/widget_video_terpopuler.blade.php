<div class="sidebar-inner" data-margin-top="60" data-sticky-for="992" style="">
  <div class="sectionAhliTitle">
    <span class="h4 text-blue">Video Populer</span>
    <div class="break-line" style="width: 80%;border-color: #dc3966 !important; opacity: 0.6"></div>
  </div>

  <div class="artikel-container">
    <div class="widget_weart_latestposts box mt-4 mb-4 f-text l-black lh-1 c-black" id="weart_latestposts-2">

      @foreach($popular_video as $item)
      <div class="itemList row align-items-center post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political">

        <div class="col">
          <h2 class="l-black lh-1 h6 f-main"><a href="/videos/{{ $item->id}}" title="{{ $item->name }}">{{ $item->name }}</a></h2>
        </div>
      </div>
      <hr>
      @endforeach
      <hr>
    </div>
  </div>
</div>
