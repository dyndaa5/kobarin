<!-- MENU -->
<!-- MAIN MENU -->
  <div class="menu-divider"></div>
  <div id="main-menu" class="f-main sticky-header bg-white" style="z-index: 0">
    <div id="sub-main-menu" class="d-flex align-items-center position-relative">
      <div id="left-bars" class="bg-1 c-white ch-black" style="width: 50px;line-height: 50px;"><i class="fas fa-bars"></i></div>
              <div class="logo menu-logo f-main l-black lh-1" itemscope>
                <a href="{{ url('/') }}" class="custom-logo-link" rel="home" itemprop="url"><img width="400" height="92" src="{{ Storage::disk('public')->url('img/logo/' . $setting->logo) }}" class="custom-logo" alt="Kobarin" itemprop="logo" srcset="{{ Storage::disk('public')->url('img/logo/' . $setting->logo) }}" sizes="(max-width: 400px) 100vw, 400px" /></a>                  </div>
              </div>
      <div class="d-flex align-items-center
        mr-auto
        ">
              <nav class="navbar navbar-expand l-black lh-1 px-1 position-static" role="navigation">
          <ul id="desktop-menu" class="navbar-nav px-2 d-none"><li id="menu-item-55" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-55 pr-3"><a href="{{ url('/self-development') }}">Self<br />Development</a></li>
<li id="menu-item-240" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-240 pr-3"><a href="{{ url('/self-enhancement') }}">Self<br />Empowerment</a>
<li id="menu-item-77" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-77 pr-3 text-nowrap"><a href="{{ url('/daftar-ahli') }}">Kawan Ahli</a>
</li>
<li id="menu-item-34" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34 pr-3"><a href="{{ url('/agenda') }}">Agenda</a>
</li>
<li id="menu-item-424" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-424 pr-3 text-nowrap"><a href="{{ url('/tentang-kami') }}">Tentang Kami</a>
</li>
</ul>        </nav>
            </div>
      <div id="right-bars" class="bg-1 c-white ch-black" style="width: 50px;line-height: 50px;"><i class="fas fa-bars"></i></div>
      <div class="text-nowrap" id="right-sub-menu">
        <div id="networks" class="c-meta ch-black d-inline-block border-right">
          <a href="{{ url('/#') }}"><b>Networks</b></a>
        </div>
        <div id="sign-in-up" class="c-meta ch-black d-inline-block">

          @if (! $logged_in_user)
              <a href="{{route('frontend.auth.login')}}">
                <b>Login</b>
              </a>
          @else
            <li class="dropdown list-unstyled">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <b>{{ $logged_in_user->name }}</b> <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                  @permission('user-dashboard')
                    <li>
                      <a href="{{route('frontend.user.dashboard')}}">
                        <b>Dashboard</b>
                      </a>
                    </li>
                  @endauth
                    @permission('view-backend')
                      <li>
                        <a href="{{route('admin.dashboard')}}">
                          <b>Admin Panel</b>
                        </a>
                      </li>
                    @endauth
                    <li>
                      <a href="{{route('frontend.user.account')}}">
                        <b>Account</b>
                      </a>
                    </li>
                    <li>
                      <a href="{{route('frontend.auth.logout')}}">
                        <b>Logout</b>
                      </a>
                    </li>
                </ul>
            </li>
          @endif

        </div>
        <div id="search" class="c-meta border-left ch-black d-inline-block"><i class="fas fa-search"></i></div>
      </div>

    </div>
  </div>

  <div id="mobile-menu">
      <div class="moible-menu-inner container h-100 f-main l-white lh-white">
          <div class="menu w-100 text-uppercase">

            <ul id="menu-header" class=""><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-55"><a href="{{ url('/self-development') }}">Self Development</a></li>
  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-240"><a href="{{ url('/self-enhancement') }}">Self Enhancement</a>
  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-240"><a href="{{ url('/daftar-ahli') }}">Kawan Ahli</a>
  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-240"><a href="{{ url('/agenda') }}">Agenda</a>
  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-240"><a href="{{ url('/tentang-kami') }}">Tentang Kami</a></li>

  </ul>        </div>
      </div>
    </div>

    <div id="right-mobile-menu">
        <div class="moible-menu-inner container h-100 f-main l-white lh-white">
            <div class="menu w-100 text-uppercase">
              <ul id="menu-header" class="">
                <li id="menu-item-240" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-240 pr-3"><a href="#">Networks</a></li>
                @if (! $logged_in_user)
                  <li id="menu-item-77" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-77 pr-3 text-nowrap"><a href="{{route('frontend.auth.login')}}">Sign In</a>
                  </li>
                  @if (config('access.users.registration'))
                    <li id="menu-item-34" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34 pr-3"><a href="{{route('frontend.auth.register')}}">Sign up</a>
                    </li>
                  @endif
                @endif
                <li id="menu-item-424" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-424 pr-3 text-nowrap"><a href="{{ url('/search/?keywords=') }}">Search</a>
                </li>

              </ul>
            </div>
        </div>
      </div>


<!-- if is a bottom logo -->
<!-- search menu -->
  <div id="search-menu">
    <div class="moible-menu-inner container h-100 f-main l-white lh-white">
        <div class="menu w-100">
          <form role="search" method="get" class="search-form d-block " action="/search/">
  <div class="input-group">
    <input type="search" class="form-control search-field" placeholder="Search for..." aria-label="Search for..." name="keywords">
    <span class="input-group-btn">
      <button class="btn bg-1 c-white search-submit" type="submit"><i class="fas fa-search"></i></button>
    </span>
  </div>
</form>
        </div>
    </div>
  </div><!-- MAIN -->
