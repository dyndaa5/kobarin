@extends('frontend.layouts.app')

@section('content')

  <div class="section-featured section mb-5 pt-1">
    <div class="container-fluid main-sidebar mt-3">
      <div class="row">
        <div class="col-md-12">
          {!! $page->description !!}
        </div>
      </div>
    </div>
  </div>   
@endsection
