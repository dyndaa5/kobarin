@extends('frontend.layouts.app')

@section('content')
  <div class="section-featured section mb-5 pt-1">
    <div class="container-fluid main-sidebar mt-3">
      <div class="row">
        <div class="col-md-12">
          <form role="search" method="get" class="search-form d-block " action="/search/">
            <div class="input-group">
              <input type="search" class="form-control search-field" placeholder="Search for..." aria-label="Search for..." name="keywords">
              <span class="input-group-btn">
                <button class="btn bg-1 c-white search-submit" type="submit"><i class="fas fa-search"></i></button>
              </span>
            </div>
          </form>
          <br />

          <div class="sectionTitle">
          @if (count($sd_results) > 0)
            <span class="bg-white h4">Search results</span>
          @elseif(count($se_results) > 0)
            <span class="bg-white h4">Search results</span>

          @elseif(count($ahli_results) > 0)
            <span class="bg-white h4">Search results</span>
          @endif
          </div>
          <hr />
        </div>

        <div class="col-md-12">
          <div class="sectionTitle pt-2">
            <span class="bg-white h4" style="font-size: 20px !important;">Self Development</span>
          </div>

          <div class="row pt-5">
            @foreach($sd_results as $item)
            <div class="col-md-4">
              <div class="itemInner zoom post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">
                <div class="bg lazy bg-65" style="display: block; background-image: url({{ Storage::disk('public')->url('img/featurd-image/' . $item->featured_image) }});"></div>
                <a class="coverLink" href="{{ url('/self-development')}}/{{ $item->id }}" title="{{ $item->name }}"></a>

              </div>
              <div class="title l-white c-white  pt-4">
                <a class="" href="{{ url('/self-development')}}/{{ $item->id }}" title="{{ $item->name }}"><h6 class="c-black">{{ $item->name }}</h6></a>
                <div class="d-none d-md-inline-block">
                  <div class="cat-badge f-main border small bgh-1">
                    <a href="#" rel="category tag">{{ date('d-M-Y', strtotime($item->created_at)) }} @if(isset($item->places)) @ {{$item->places}} @else  @endif</a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>

          <div class="sectionTitle pt-2">
            <span class="bg-white h4" style="font-size: 20px !important;">Self Enhancement</span>
          </div>

          <div class="row pt-5">


            @foreach($se_results as $item)
            <div class="col-md-4">
              <div class="itemInner zoom post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">
                <div class="bg lazy bg-65" style="display: block; background-image: url({{ Storage::disk('public')->url('img/featurd-image/' . $item->featured_image) }});"></div>
                <a class="coverLink" href="{{ url('/self-enhancement') }}/{{ $item->id }}" title="{{ $item->name }}"></a>

              </div>
              <div class="title l-white c-white">
                <div class="d-none d-md-inline-block">
                  <div class="cat-badge f-main border small bgh-1">
                    <a href="#" rel="category tag">{{ date('d-M-Y', strtotime($item->created_at)) }} @if(isset($item->places)) @ {{$item->places}} @else  @endif</a>
                  </div>
                </div>
                <a class="" href="{{ url('/self-enhancement') }}/{{ $item->id }}" title="{{ $item->name }}"><h2 class="h3 c-black">{{ $item->name }}</h2></a>
              </div>
            </div>
            @endforeach
          </div>

          <div class="sectionTitle  pt-5">
            <span class="bg-white h4" style="font-size: 20px !important;">Ahli</span>
          </div>

          <div class="row pt-5">
            @foreach($ahli_results as $item)
            <div class="col-md-4">
              <div class="itemList profile-rounded row align-items-center  post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political">
                            <div class="col-12">
                    <div class="bg-100 rounded-circle" style="display: block; background-image: url(/images/ahli_default.png);background-position: left 50% top 50%;">
                          <a href="{{ url('/author')}}/{{ $item->id }}" title="{{ $item->first_name . ' ' . $item->last_name }}" class="coverLink"></a>
                        </div>
                      </div>
                          <div class="col pt-2 text-center">
                            <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="{{ url('/ahli')}}/{{ $item->id }}">{{ $item->first_name . ' ' . $item->last_name }}</a></h2>
                            <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="Type">Konselor</a></h2>
                            <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="Type">Institusi A</a></h2>
                          </div>
                  </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
