@php
    use Illuminate\Support\Facades\Route;
@endphp
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <link rel="icon" sizes="16x16" type="image/png" href="{{route('frontend.index')}}/storage/img/favicon/{{settings()->favicon}}">

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'KOBARIN.ID')">
        <meta name="author" content="@yield('meta_author', 'KOBARIN.ID - HS')">
        <meta property="og:type" name="og:type" content="website"/>
        <meta property="og:site_name" content="{{ config('app.name') }}"/>
        <meta property="og:url" name="og:url" content="{{ request()->url() }}"/>
        <meta property="og:caption" name="og:caption" content="{{ config('app.url') }}"/>
        <meta property="fb:app_id" name="fb:app_id" content="{{ config('app.facebook_id') }}"/>
        <meta property="og:title" name="og:title" content="{{ isset($title) ? $title : config('app.title') }}">
        <meta property="og:description" name="og:description" content="{{ isset($description) ? $description : config('app.description') }}">
        <meta property="og:image" name="og:image" content="{{ config('app.url') }}{{ isset($image) ? $image : '/images/logo.png' }}">


        @yield('meta')

        <!-- Styles -->
        @yield('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->

        {{ Html::style(mix('css/frontend.css')) }}
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        {!! Html::style('js/select2/select2.css') !!}
        {!! Html::style('css/plugin/datatables/jquery.dataTables.min.css') !!}
        {!! Html::style('css/backend/plugin/datatables/dataTables.bootstrap.min.css') !!}
        {!! Html::style('css/plugin/datatables/buttons.dataTables.min.css') !!}
        @yield('after-styles')

        <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
        <?php
            if(!empty($google_analytics)){
                echo $google_analytics;
            }
        ?>
    </head>
    <body id="top" class="home blog wp-custom-logo woocommerce-no-js">
      <div id="app">
          @include('frontend.includes.nav')
          <div id="main-content" class="c-black f-text main">
            @include('includes.partials.messages')
            @yield('content')
          </div>
         @include('frontend.includes.footer')
      </div>

        <!-- Scripts -->
        @yield('before-scripts')
        {!! Html::script('/js/jquery-ver=1.12.4.js') !!}
        {!! Html::script('/js/jquery-migrate.min-ver=1.4.1.js') !!}
        {!! Html::script(mix('js/frontend.js')) !!}
        @yield('after-scripts')
        {{ Html::script('js/jquerysession.js') }}
        {{ Html::script('js/frontend/frontend.js') }}
        {!! Html::script('js/select2/select2.js') !!}

        <script type="text/javascript">
            if("{{Route::currentRouteName()}}" !== "frontend.user.account")
            {
                jQuery.session.clear();
            }
        </script>

        <script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>
        @include('includes.partials.ga')
    </body>
</html>
