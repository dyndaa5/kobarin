@extends('frontend.layouts.app')

@section('content')
<div class="section-featured pb-2 mb-5 pt-1">
  <div class="container-fluid main-sidebar mt-3">
    <div class="row">

        <div class="col-md-6 col-sm-6 col-md-offset-2">

            <div class="panel panel-default" id="panel">
                <div class="panel-heading"></div>

                <div class="panel-body">

                    {{ Form::open(['route' => 'frontend.auth.login', 'class' => 'form-horizontal']) }}

                    <div class="form-group">
                        {{ Form::label('email', trans('validation.attributes.frontend.register-user.email'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-12">
                            {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.email')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('password', trans('validation.attributes.frontend.register-user.password'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-12">
                            {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    {{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}
                                </label>
                            </div>
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {{ Form::submit('Masuk', ['class' => 'btn btn-primary', 'style' => 'margin-right:15px']) }}
                            <br />
                            <br />
                            {{ link_to_route('frontend.auth.register', 'Daftar', ['class' => 'link']) }}
                            <br />
                            {{ link_to_route('frontend.auth.password.reset', 'Lupa password?') }}

                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    {{ Form::close() }}


                    <div class="row text-center">

                    </div>
                </div><!-- panel body -->

            </div><!-- panel -->

        </div><!-- col-md-8 -->

    </div><!-- row -->
  </div>
</div>
@endsection
