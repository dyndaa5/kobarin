@extends('frontend.layouts.app')

@section('after-styles')
<style>
.breadcrumb {
    padding: 0px;
	background: #D4D4D4;
	list-style: none;
	overflow: hidden;
}
.breadcrumb>li+li:before {
	padding: 0;
}
.breadcrumb li {
	float: left;
}
.breadcrumb li.active a {
	background: brown;                   /* fallback color */
	background: #dc3966!important;
}
.breadcrumb li.completed a {
	background: brown;                   /* fallback color */
	background: hsla(153, 57%, 51%, 1);
}
.breadcrumb li.active a:after {
	border-left: 30px solid #dc3966!important ;
}
.breadcrumb li.completed a:after {
	border-left: 30px solid hsla(153, 57%, 51%, 1);
}

.breadcrumb li a {
	color: white;
	text-decoration: none;
	padding: 10px 0 10px 45px;
	position: relative;
	display: block;
	float: left;
}
.breadcrumb li a:after {
	content: " ";
	display: block;
	width: 0;
	height: 0;
	border-top: 50px solid transparent;           /* Go big on the size, and let overflow hide */
	border-bottom: 50px solid transparent;
	border-left: 30px solid hsla(0, 0%, 83%, 1);
	position: absolute;
	top: 50%;
	margin-top: -50px;
	left: 100%;
	z-index: 2;
}
.breadcrumb li a:before {
	content: " ";
	display: block;
	width: 0;
	height: 0;
	border-top: 50px solid transparent;           /* Go big on the size, and let overflow hide */
	border-bottom: 50px solid transparent;
	border-left: 30px solid white;
	position: absolute;
	top: 50%;
	margin-top: -50px;
	margin-left: 1px;
	left: 100%;
	z-index: 1;
}
.breadcrumb li:first-child a {
	padding-left: 15px;
}


/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}

.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}

/* Random Post Widget */
#twisted-random ul {
  list-style: none;
  margin: 0;
  padding: 0
}

#twisted-random li {
  display: block;
  clear: both;
  overflow: hidden;
  list-style: none;
  border-bottom: 1px solid #e3e3e3;
  word-break: break-word;
  padding: 10px 0;
  margin: 0;
}

#twisted-random li:last-child {
  border-bottom: 0;
}

#twisted-random li a {
  color: #444;
}

#twisted-random li a:hover {
  color: #444;
}

#twisted-random li.active a {
  color: #dc3966!important;
}

a:link {
  text-decoration: none;
  outline: none;
  transition: all 0.25s;
}

a:visited,
a:link:hover,
a:visited:hover {
  text-decoration: none;
}


.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
  z-index: 1;
}

.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 70px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 30%;
  position: relative;
  transition: all 5s ease-in-out;
}

.popup h2 {
  margin-top: 0;
  color: #333;
  font-family: Tahoma, Arial, sans-serif;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

</style>
@endsection

@section('content')
    <div class="container">

      <ul class="breadcrumb">
  			<li class="completed"><a href="{{ url('/') }}">Home</a></li>
        <li class="completed"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
  			<li class="active"><a href="javascript:void(0);">Self Empowering</a></li>
  		</ul>
      <div class="row c-black f-text l-black lh-1 c-black">
          <div class="col">
            <div class="clearfix content">

              <section>
        	        <h1 class="text-center text-secondary mb-5 text-blue">Self Empowering</h1>
        		<div class="row">
              <!-- Shipping -->
        			<div class="col-sm-4 col-md-3 col-lg-3 col-xl-3 qualities">
        				<div class="border border-secondary p-4 mb-3 shipping rounded">
        					<ul class="list-inline list-unstyled m-0">
        						  <li class="list-inline-item text-secondary mb-0"><h5>{{ $active_posts }}</h5> Published posts</li>
        					</ul>
        				</div>
        			</div>
        			<!-- ../Shipping -->
        			<!-- exchange -->
        			<div class="col-sm-4 col-md-3 col-lg-3 col-xl-3 qualities">
        				<div class="border border-secondary p-4 mb-3 exchange rounded">
        					<ul class="list-inline list-unstyled m-0">
        						  <li class="list-inline-item text-secondary mb-0"><h5>{{ $pending_posts }}</h5>Pending posts</li>
        					</ul>
        				</div>
        			</div>
        			<!-- ../exchange -->
        			<!-- support -->
        			<div class="col-sm-4 col-md-3 col-lg-3 col-xl-3 qualities">
        				<div class="border border-secondary p-4 mb-3 support rounded">
        					<ul class="list-inline list-unstyled m-0">
        						  <li class="list-inline-item text-secondary mb-0"><h5>{{ $draft_posts }}</h5> Draft posts</li>
        					</ul>
        				</div>
        			</div>
        			<!-- ../support -->
        		</div>
            <!-- <a href="#" class="btn btn-info btn-xs pull-right"><b>+</b> Add Post</a> -->
            <br />
            <br>
            <div class="table-responsive">
              <table class="table table-striped custab">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Thumbnail</th>
                        <th>Judul</th>
                        <th>Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($items as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td><img src="/storage/img/featurd-image/{{ $item->featured_image }}" width="100" height="100" class="img-responsive"/> </td>
                        <td><a href="{{ url('/self-enhancement')}}/{{ $item->id }}">{{ $item->name}}</a></td>
                        <td>{{ $item->status }}</td>
                        <td class="text-center">
                            <!-- <a class='btn btn-info btn-xs' href="#edit-{{$item->id}}"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a> -->
                        </td>
                    </tr>

                    <!-- <div id="edit-{{ $item->id }}" class="overlay">
                    	<div class="popup">
                    		<h4>{{ $item->name}}</h4>
                    		<a class="close" href="#">×</a>
                    		<div class="content">
                          {{ Form::model($item, ['route' => ['admin.selfdevelopments.update', $item], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-selfdevelopment', 'files' => true]) }}

                          <div class="form-group">
                              {{ Form::label('featured_image', trans('validation.attributes.backend.blogs.image'), ['class' => 'col-lg-2 control-label']) }}
                              @if(!empty($item->featured_image))
                                  <div class="col-lg-1">
                                      <img src="{{ Storage::disk('public')->url('img/featurd-image/' . $item->featured_image) }}" height="80" width="80">
                                  </div>
                                  <div class="col-lg-5">
                                      <div class="custom-file-input">
                                          <input type="file" name="featured_image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                                          <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
                                      </div>
                                  </div>
                              @else
                                  <div class="col-lg-5">
                                      <div class="custom-file-input">
                                              <input type="file" name="featured_image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                                              <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
                                      </div>
                                  </div>
                              @endif
                          </div><!--form control

                          <div class="form-group">
                              {{ Form::label('content', trans('validation.attributes.backend.blogs.content'), ['class' => 'col-lg-2 control-label']) }}

                              <div class="col-lg-10 mce-box">
                                  {{ Form::textarea('content', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.blogs.content')]) }}
                              </div><!--col-lg-10-->
                          </div><!--form control

                          <div class="form-group">
                              {{ Form::label('status', trans('validation.attributes.backend.pages.is_active'), ['class' => 'col-lg-2 control-label']) }}

                              <div class="col-lg-10">
                                  <div class="control-group">
                                      <label class="control control--checkbox">
                                          {{ Form::checkbox('status', 1, true) }}
                                          <div class="control__indicator"></div>
                                      </label>
                                  </div>
                              </div><!--col-lg-3-->
                          </div><!--form contro

                          <a class="btn btn-danger btn-md" href="#">Cancel</a>
                          {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                          {{ Form::close() }}

                    		</div>
                    	</div>
                    </div> -->
                  @endforeach
                </tbody>
                </table>
              </div>
        		</section>
      </div>
            <div class="clearfix link-pages"></div>
          </div>
            <div class="sidebar col-lg-3 mt-5 mt-lg-0 ml-lg-4 border-left" data-sticky-container="">
          <div id="woocommerce_widget_cart-2" class="woocommerce widget_shopping_cart box mb-4 mt-2 f-text l-black lh-1 c-black"><h4 class="text-uppercase mb-3 h5 pb-1 border-bottom">{{ $logged_in_user->name }}</h4><div class="widget_shopping_cart_content">

      	<p class="woocommerce-mini-cart__empty-message">
          <img class="media-object" style="display: block;margin-left: auto;margin-right: auto " src="{{ $logged_in_user->picture }}" alt="Profile picture">
          {{ $logged_in_user->email }}<br/>
          Bergabung sejak {{ $logged_in_user->created_at->format('F jS, Y') }}
          {{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => 'btn btn-info btn-sm' , 'style' => 'color: white !important']) }}
          {{ link_to_route('frontend.auth.logout', 'Logout', [], ['class' => 'btn btn-danger btn-sm text-white' , 'style' => 'color: white !important']) }}
        </p>



      </div></div><div id="woocommerce_product_categories-2" class="woocommerce widget_product_categories box mb-4 mt-2 f-text l-black lh-1 c-black"><h4 class="text-uppercase mb-3 h5 pb-1 border-bottom">Menu</h4><ul class="product-categories" id="twisted-random"><li class=" cat-item cat-item-118 cat-parent current-cat-parent"><a href="{{ route('frontend.user.dashboard')}}">Dashboard</a>
      </li>
      <li class=" cat-item cat-item-121 "><a href="{{ route('frontend.user.sd')}}">Self Development</a></li>
      <li class="active cat-item cat-item-122"><a href="{{ route('frontend.user.se')}}">Self Empowering</a></li>
      <li class="cat-item cat-item-123"><a href="{{ route('frontend.user.agenda')}}">Agenda</a></li>
      <li class="cat-item cat-item-124"><a href="{{ route('frontend.user.video')}}">Video</a></li>
      </ul></div>  </div>
        </div>
    </div><!-- row -->
@endsection
