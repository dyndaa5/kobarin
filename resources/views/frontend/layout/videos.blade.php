@extends('frontend.layouts.app')

@section('content')
  <div class="section-featured pb-2 mb-5 pt-1">
    <div class="container-fluid main-sidebar mt-3">
      <div class="row">
        <div class="col-lg-12">
          <div class="sectionTitle">
              <span class="bg-white h4">Video Terkini</span>
          </div>
          <div class="sectionContent">
            <br />
            <div class="col-md-3">
              <div class="row">
                <div class="col-md-2">
                  <div class="itemList profile-rounded row align-items-center  post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political">

                      <div class="rounded-circle" style="display: block; background-image: url({{ Storage::disk('public')->url('img/photo/' . $item->user->photo) }});background-size: 47px;width: 48px;height: 48px;background-position: left 50% top 50%;">


                            <a href="{{ url('/author')}}/{{ $item->user->id }}" class="coverLink"></a>
                          </div>
                  </div>
                </div>
                <div class="col-md-10">
                  <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->user->id }}">{{ $item->user->first_name . ' ' . $item->user->last_name }}</a></h2>
                  <div class="break-line mw-1080"></div>
                  <div class="text-muted">
                    {{ date('d-M-Y', strtotime($item->created_at)) }} <span>&#9679;</span> <i class="fa fa-eye"></i> {{ $item->getViews() }}
                  </div>
                </div>
              </div>
            </div>

            <div class="item pt-1">
              <br />
              <h4>{{ $item->name }}</h4>

              <br />
              <div class="row">
                <div class="col-md-8">

                  {!! $item->content !!}
                </div>

                <div class="col-md-4">
                  @if(Request::is('videos/*'))
                    @include('frontend.includes.widget_video_terpopuler')
                  @endif
                </div>

            </div>
            <div class="break-line mw-1080"></div>
            <div id="disqus_thread"></div>
            <!-- <div class="break-line mw-1080"></div>
            <h5 class="text-muted">Bagikan</h5>
            <div class="social">
                <a class="link-share text-primary" data-social="facebook">
                    <i class="fa fa-facebook"></i>
                </a>
                <a class="link-share text-primary" data-social="twitter">
                    <i class="fa fa-twitter"></i>
                </a>
            </div> -->
          </div>
        </div>
      </div><!-- .row -->
    </div><!-- .container-fluid -->
  </div>
</div>
@endsection

@section("after-scripts")
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://kobarin.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection
