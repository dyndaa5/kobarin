@extends('frontend.layouts.app')
@section('content')

<div class="section-featured section mb-5 pt-1">
  <div class="container-fluid main-sidebar mt-3">
    <div class="row">
      <div class="col-lg-8">
        <div class="sectionTitle">
          <span class="bg-white h4">Panduan Logo</span>
        </div>

        <div class="sectionContent">
          {!! $activePage->description !!}
        </div>
      </div>
      <div class="col-lg-4">
        <div class="sidebar col mt-5 mt-lg-0 ml-lg-4" data-sticky-container="">
          <div class="sidebar-inner sticky-sidebar" data-margin-top="60" data-sticky-for="992" style="">
            <div class="widget_weart_latestposts box mb-4 f-text l-black lh-1 c-black" id="weart_latestposts-2">
              <h4 class="text-uppercase mb-3 h5 pb-1 border-bottom brd-width-bold"></h4>
              <div class="itemList row align-items-center post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political">

                <div class="col">
                  <h2 class="l-black lh-1 h6 f-main"><a href="/tentang-kami/sejarah-kobarin" title="Sejarah Kobarin.id">Sejarah Kobarin.id</a></h2>
                </div>
              </div>
              <hr>
              <div class="itemList row align-items-center post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">

                <div class="col">
                  <h2 class="l-black lh-1 h6 f-main"><a href="/tentang-kami/panduan-logo" title="Panduan Logo">Panduan Logo</a></h2>
                </div>
              </div>
              <hr>
              <div class="itemList row align-items-center post-578 post type-post status-publish format-standard has-post-thumbnail hentry category-culture tag-gun tag-school tag-shooting">
                <div class="col">
                  <h2 class="l-black lh-1 h6 f-main"><a href="/tentang-kami" title="Sekilas tentang kobarin.id">Sekilas tentang kobarin.id</a></h2>
                </div>
              </div>
              <hr>
              <h4 class="text-uppercase mb-3 h5 pb-1 border-bottom brd-width-bold"></h4>
            </div>
          </div>
        </div>
      </div>
    </div><!-- .row -->
  </div><!-- .container-fluid -->
</div>

@endsection
