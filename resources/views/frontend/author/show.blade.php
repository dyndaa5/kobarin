@extends('frontend.layouts.app')
@section('content')
<div class="section-featured section mb-5 pt-1">
  <div class="container-fluid main-sidebar mt-3">
    <div class="row">
      <div class="col-lg-12">
        <div class="span4 card">
          <div class="row">
            <div class="col-md-3">
              <div class="itemList profile-rounded row align-items-center  post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political">
                <div class="col-md-12">
                  <div class="rounded-circle" style="display: block; background-image: url({{ Storage::disk('public')->url('img/photo/' . $user->photo) }});background-size: 190px;width: 200px;height: 200px;background-position: left 50% top 50%;">
                    <a href="{{ url('/author')}}/{{ $user->id }}" class="coverLink"></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="pt-5 text-left text-white">
                <div class="title" style="font-size: 30px;font-weight: bold">
                  {{ $user->first_name . ' ' . $user->last_name }}
                </div>

                <div class="content text-left" style="font-size: 17px;margin-left: -19px;">
                  {!! $user->about !!}
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="pt-5 text-center text-white">
                <div class="title" style="font-size: 30px">
                  Lisensi Profesional
                </div>

                <div class="content text-left" style="font-size: 17px;margin-left:47px;">
                  {!! $user->lisensi_professional !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- .row -->
  </div><!-- .container-fluid -->
</div>
<div class="section-featured section mb-5 pt-1 pb-5" style="height: auto;">
  <div class="container-fluid main-sidebar mt-3">
    <div class="row">
      <div class="col-md-4">
      </div>
      <div class="col-md-4">
        <ul class="nav nav-pills nav-fill navtop" style="max-width: 400px">
              <li class="nav-item">
                  <a class="nav-link active" href="#menu1" data-toggle="tab">Artikel</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#menu2" data-toggle="tab">Tautan Informasi</a>
              </li>

          </ul>
      </div>
      <div class="col-md-4">
      </div>
    </div>
      <div class="tab-content" >
          <div class="tab-pane active" role="tabpanel" id="menu1">
            <div class="row pt-5">
              @foreach($sd_posts as $item)
                @if($item != '')
                  <div class="col-md-4">
                    <div class="itemInner zoom post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">
                      <div class="bg lazy bg-65" style="display: block; background-image: url({{ Storage::disk('public')->url('img/featurd-image/' . $item->featured_image) }});"></div>
                      <a class="coverLink" href="{{ url('/self-development')}}/{{ $item->id }}" title="{{ $item->name }}"></a>
                      <div class="title l-white c-white">
                        <div class="d-none d-md-inline-block">
                          <div class="cat-badge f-main border small bgh-1">
                            <a href="#" rel="category tag">{{ date('d-M-Y', strtotime($item->created_at)) }}</a>
                          </div>
                        </div>
                        <h2 class="h3">{{ $item->name }}</h2>
                      </div>
                    </div>
                  </div>
                @endif
              @endforeach
              @foreach($se_posts as $item)
                @if($item != '')
                  <div class="col-md-4">
                    <div class="itemInner zoom post-581 post type-post status-publish format-standard has-post-thumbnail hentry category-tech tag-car tag-raise tag-tesla">
                      <div class="bg lazy bg-65" style="display: block; background-image: url({{ Storage::disk('public')->url('img/featurd-image/' . $item->featured_image) }});"></div>
                      <a class="coverLink" href="{{ url('/self-enhancement')}}/{{ $item->id }}" title="{{ $item->name }}"></a>
                      <div class="title l-white c-white">
                        <div class="d-none d-md-inline-block">
                          <div class="cat-badge f-main border small bgh-1">
                            <a href="#" rel="category tag">{{ date('d-M-Y', strtotime($item->created_at)) }}</a>
                          </div>
                        </div>
                        <h2 class="h3">{{ substr($item->name, 0, 50) }}</h2>
                      </div>
                    </div>
                  </div>
                @endif
              @endforeach

            </div>
          </div>
          <div class="tab-pane" role="tabpanel" id="menu2">
              <!-- <h2 class="text-center pt-4 text-blue"> Comming Soon </h2> -->
            <div class="row pt-5">
              <div class="col-md-6">
                ResearchGate : <a href="{!! $user->researchgate !!}" target="_blank">{!! $user->researchgate !!}</a>
                <br />
                Website : <a href="{!! $user->website !!}" target="_blank">{!! $user->website !!}</a>
                <br />
                Twitter : <a href="{!! $user->twitter !!}" target="_blank">{!! $user->twitter !!}</a>
              </div>

              <div class="col-md-6">
                Linkedin : <a href="{!! $user->linkedin !!}" target="_blank">{!! $user->linkedin !!}</a>
                <br />
                Youtube : <a href="{!! $user->instagram !!}" target="_blank">{!! $user->instagram !!}</a>
                <br />
                Facebook : <a href="{!! $user->facebook !!}" target="_blank">{!! $user->facebook !!}</a>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>

@endsection
