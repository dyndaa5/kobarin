@extends('frontend.layouts.app')
@section('content')
	<div class="section-featured section mb-5 pt-1">
		<div class="container-fluid main-sidebar mt-3">
			<div class="row">
				<div class="col-lg-12">
					<div class="itemInner zoom post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political" style="max-height: 350px;">
						<div class="bg lazy bg-30 pb-1" style="display: block; background-image: url({{ Storage::disk('public')->url('img/featurd-image/' . $banner->featured_image) }});"></div>
					</div>
				</div>
			</div><!-- .row -->
		</div><!-- .container-fluid -->
	</div>
	<div class="section-featured section mb-5 pt-1 pb-5" style="height: auto;">
		<div class="container-fluid main-sidebar mt-3">
			<div class="row">
	      <div class="col-md-4">
	      </div>
	      <div class="col-md-4">
	        <ul class="nav nav-pills nav-fill navtop" style="max-width: 400px">
	              <li class="nav-item">
	                  <a class="nav-link active" href="#menu1" data-toggle="tab">Edukasi</a>
	              </li>
	              <li class="nav-item">
	                  <a class="nav-link" href="#menu2" data-toggle="tab">Konseling</a>
	              </li>

	          </ul>
	      </div>
	      <div class="col-md-4">
	      </div>
	    </div>
		    <div class="tab-content">
		        <div class="tab-pane active" role="tabpanel" id="menu1">
							<div class="row pt-5">
								@foreach($items_edukasi as $item)
									<div class="col-md-4">
										<div class="itemList profile-rounded row align-items-center  post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political">
										              <div class="col-12">
										      <div class="bg-100 rounded-circle" style="display: block; background-image: url({{ Storage::disk('public')->url('img/photo/' . $item->photo) }});background-size: 190px;background-position: left 50% top 50%;">
										            <a href="{{ url('/author')}}/{{ $item->id }}" title="{{ $item->first_name . ' ' . $item->last_name }}" class="coverLink"></a>
										          </div>
										        </div>
										            <div class="col pt-2  text-center">
										              <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="{{ url('/author')}}/{{ $item->title }}">{{ $item->first_name . ' ' . $item->last_name }}</a></h2>
										              <h2 class="l-black lh-1  h6 f-main"><a href="#" title="Type">{{ $item->jabatan }}</a></h2>
																	<h2 class="l-black lh-1  h6 f-main"><a href="#" title="Type">{{ $item->institusi }}</a></h2>
										      			</div>
										    </div>
									</div>
								@endforeach

							</div>
						</div>
						<div class="tab-pane" role="tabpanel" id="menu2">
							<div class="row pt-5">
								@foreach($items_konseling as $item)
									<div class="col-md-4">
										<div class="itemList profile-rounded row align-items-center  post-584 post type-post status-publish format-standard has-post-thumbnail hentry category-politics tag-correctness tag-political">
										              <div class="col-12">
										      <div class="bg-100 rounded-circle" style="display: block; background-image: url({{ Storage::disk('public')->url('img/photo/' . $item->photo) }});background-size: 190px;background-position: left 50% top 50%;">
										            <a href="{{ url('/author')}}/{{ $item->id }}" title="{{ $item->first_name . ' ' . $item->last_name }}" class="coverLink"></a>
										          </div>
										        </div>
										            <div class="col pt-2  text-center">
										              <h2 class="l-black lh-1  h6 f-main"><a href="{{ url('/author')}}/{{ $item->id }}" title="{{ url('/author')}}/{{ $item->title }}">{{ $item->first_name . ' ' . $item->last_name }}</a></h2>
										              <h2 class="l-black lh-1  h6 f-main"><a href="#" title="Type">{{ $item->jabatan }}</a></h2>
																	<h2 class="l-black lh-1  h6 f-main"><a href="#" title="Type">{{ $item->institusi }}</a></h2>
										      			</div>
										    </div>
									</div>
								@endforeach

							</div>

						</div>
					</div>

		    </div>
		</div>
	</div>
@endsection
