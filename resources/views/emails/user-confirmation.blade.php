@extends('emails.layouts.app')

@section('content')
<div class="content">
    <td align="left">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="m_-7184198754800118032bodyTable" style="height:100%;border-collapse:collapse;width:100%;background-color:#ebebeb;margin:0;padding:0" bgcolor="#ebebeb">
        <tbody><tr>
          <td align="center" valign="top" id="m_-7184198754800118032bodyCell" style="height:100%;width:100%;border-top-width:0;margin:0;padding:10px">
            
            
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-7184198754800118032templateContainer" style="border-collapse:collapse;max-width:600px!important;border:0">
              <tbody><tr>
                <td style="line-height:32px">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" id="m_-7184198754800118032templatePreheader" style="border-top-color:#5388bb;border-top-style:solid;border-top-width:6px;border-bottom-width:0;padding-top:9px;padding-bottom:9px;background:#ffffff none no-repeat center/cover" bgcolor="#ffffff">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-7184198754800118032mcnTextBlock" style="min-width:100%;border-collapse:collapse">
                    <tbody class="m_-7184198754800118032mcnTextBlockOuter">
                      <tr>
                        <td valign="top" class="m_-7184198754800118032mcnTextBlockInner" style="padding-top:9px">
                          
            
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top" id="m_-7184198754800118032templateBody" style="border-top-width:0;border-bottom-color:#eaeaea;border-bottom-width:2px;border-bottom-style:none;padding-top:0;padding-bottom:9px;background:#ffffff none no-repeat center/cover" bgcolor="#ffffff">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-7184198754800118032mcnTextBlock" style="min-width:100%;border-collapse:collapse">
                    <tbody class="m_-7184198754800118032mcnTextBlockOuter">
                      <tr>
                        <td valign="top" class="m_-7184198754800118032mcnTextBlockInner" style="padding-top:9px">
                          
                          
                          
                          <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%" class="m_-7184198754800118032mcnTextContentContainer">
                            <tbody>
                              <tr>
                                <td valign="top" style="font-family:'Open Sans',Helvetica,Arial,sans-serif;font-size:16px;padding:0 32px 9px" class="m_-7184198754800118032defaultText">
                                  
    




<p style="color:#333333;margin:10px 0;padding:0">Hai! Terima kasih telah mendaftar di <span style="color:#5388bb">KOBAR</span><span style="color:#dc3966">IN</span><span style="color:#5388bb">.ID</span></p>

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-7184198754800118032mcnButtonBlock" style="min-width:100%;border-collapse:collapse">
    <tbody class="m_-7184198754800118032mcnButtonBlockOuter">
        <tr>
            <td style="padding:30px" valign="top" align="center" class="m_-7184198754800118032mcnButtonBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" class="m_-7184198754800118032mcnButtonContentContainer" style="border-collapse:separate!important;border-radius:5px;background-color:#5388bb" bgcolor="#5388bb">
                    <tbody>
                        <tr>
                            <td align="center" valign="middle" class="m_-7184198754800118032mcnButtonContent" style="font-family:Roboto,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:16px;padding:15px">
                                <a class="m_-7184198754800118032mcnButton" title="Konfirmasi Email" href="{{ $confirmation_url}}" style="font-weight:normal;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#ffffff;display:block" target="_blank">Konfirmasi email</a>
                            </td>
                         </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<p style="color:#333333;max-width:600px;margin:10px 0;padding:0 0 15px">
    atau kunjungi link dibawah ini:<br>
    <a style="color:#2c7cb0;font-size:12px" href="{{ $confirmation_url}}" target="_blank">{{ $confirmation_url}}</a>
</p>
<p style="color:#333333;margin:10px 0;padding:0;float:right;">
    Terima kasih,<br>
    Kobarin.id Team
</p>

                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            
                            
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                </tbody></table>
                
                
              </td>
            </tr>
          </tbody></table>
    </td>
</div>
@endsection
                        