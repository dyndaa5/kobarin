<div class="box-body">
  <div class="form-group">
      <!-- Create Your Field Label Here -->
      <!-- Look Below Example for reference -->
      {{ Form::label('name', trans('labels.backend.categories.table.name'), ['class' => 'col-lg-2 control-label required']) }}

      <div class="col-lg-10">
          <!-- Create Your Input Field Here -->
          <!-- Look Below Example for reference -->
          {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.categories.table.name'), 'required' => 'required']) }}
      </div><!--col-lg-10-->
  </div><!--form-group-->

  <div class="form-group">
      {{ Form::label('publish_datetime', trans('validation.attributes.backend.blogs.publish'), ['class' => 'col-lg-2 control-label required']) }}

      <div class="col-lg-10">
          @if(!empty($selfdevelopments->publish_datetime))
              {{ Form::text('publish_datetime', \Carbon\Carbon::parse($selfdevelopments->publish_datetime)->format('m/d/Y h:i a'), ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.blogs.publish'), 'required' => 'required', 'id' => 'datetimepicker1']) }}
          @else
              {{ Form::text('publish_datetime', null, ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.blogs.publish'), 'required' => 'required', 'id' => 'datetimepicker1']) }}
          @endif
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('featured_image', trans('validation.attributes.backend.blogs.image'), ['class' => 'col-lg-2 control-label']) }}
      @if(!empty($selfdevelopments->featured_image))
          <div class="col-lg-1">
              <img src="{{ Storage::disk('public')->url('img/featurd-image/' . $selfdevelopments->featured_image) }}" height="80" width="80">
          </div>
          <div class="col-lg-5">
              <div class="custom-file-input">
                  <input type="file" name="featured_image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                  <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
              </div>
          </div>
      @else
          <div class="col-lg-5">
              <div class="custom-file-input">
                      <input type="file" name="featured_image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                      <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
              </div>
          </div>
      @endif
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('content', trans('validation.attributes.backend.blogs.content'), ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10 mce-box">
          {{ Form::textarea('content', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.blogs.content')]) }}
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('meta_title', trans('validation.attributes.backend.blogs.meta-title'), ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('meta_title', null, ['class' => 'form-control box-size ', 'placeholder' => trans('validation.attributes.backend.blogs.meta-title')]) }}
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('slug', trans('validation.attributes.backend.blogs.slug'), ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('slug', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.slug')]) }}
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('cannonical_link', trans('validation.attributes.backend.blogs.cannonical_link'), ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('cannonical_link', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.cannonical_link')]) }}
      </div><!--col-lg-10-->
  </div><!--form control-->


  <div class="form-group">
      {{ Form::label('meta_keywords', trans('validation.attributes.backend.blogs.meta_keyword'), ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('meta_keywords', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.meta_keyword')]) }}
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('meta_description', trans('validation.attributes.backend.blogs.meta_description'), ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10 mce-box">
          {{ Form::textarea('meta_description', null,['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.blogs.meta_description')]) }}
      </div><!--col-lg-3-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('created_by', 'Post owner', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::select('created_by', \App\Models\Access\User\User::all()->pluck('first_name', 'id'), ['class' => 'form-control tags box-size', 'required' => 'required']) }}
      </div><!--col-lg-3-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('status', trans('validation.attributes.backend.pages.is_active'), ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          <div class="control-group">
              <label class="control control--checkbox">
                  {{ Form::checkbox('status', 1, true) }}
                  <div class="control__indicator"></div>
              </label>
          </div>
      </div><!--col-lg-3-->
  </div><!--form control-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">

        Backend.Blog.selectors.GenerateSlugUrl = "{{route('admin.generate.slug')}}";
        Backend.Blog.selectors.SlugUrl = "{{url('/self-development/')}}";
        Backend.Blog.init();

    </script>
@endsection
