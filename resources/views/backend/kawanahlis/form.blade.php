<div class="box-body">

  <div class="form-group">
      {{ Form::label('institusi', 'Institusi', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('institusi', null, ['class' => 'form-control box-size', 'placeholder' => 'Institusi']) }}
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('researchgate', 'ResearchGate', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('researchgate', null, ['class' => 'form-control box-size', 'placeholder' => 'ResearchGate']) }}
      </div><!--col-lg-10-->
  </div><!--form control-->


  <div class="form-group">
      {{ Form::label('linkedin', 'Linkedin', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('linkedin', null, ['class' => 'form-control box-size', 'placeholder' => 'Linkedin']) }}
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('twitter', 'Twitter', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('twitter', null, ['class' => 'form-control box-size', 'placeholder' => 'Twitter']) }}
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('facebook', 'Facebook', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('facebook', null, ['class' => 'form-control box-size', 'placeholder' => 'Facebook']) }}
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('website', 'Website', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('website', null, ['class' => 'form-control box-size', 'placeholder' => 'Website']) }}
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('youtube', 'Youtube', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::text('youtube', null, ['class' => 'form-control box-size', 'placeholder' => 'Youtube']) }}
      </div><!--col-lg-10-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('lisensi_professional', 'Lisensi Profesinal', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
        {{ Form::textarea('lisensi_professional', null,['class' => 'form-control box-size', 'placeholder' => 'Lisensi Profesinal']) }}
      </div><!--col-lg-3-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('about', 'About', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
        {{ Form::textarea('about', null,['class' => 'form-control box-size', 'placeholder' => 'About']) }}
      </div><!--col-lg-3-->
  </div><!--form control-->

  <div class="form-group">
      {{ Form::label('status', trans('validation.attributes.backend.pages.is_active'), ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          <div class="control-group">
              <label class="control control--checkbox">
                  {{ Form::checkbox('status', 1, true) }}
                  <div class="control__indicator"></div>
              </label>
          </div>
      </div><!--col-lg-3-->
  </div><!--form control-->

    <div class="form-group">
        <!-- Create Your Field Label Here -->
        <!-- Look Below Example for reference -->
        {{-- {{ Form::label('name', trans('labels.backend.blogs.title'), ['class' => 'col-lg-2 control-label required']) }} --}}

        <div class="col-lg-10">
            <!-- Create Your Input Field Here -->
            <!-- Look Below Example for reference -->
            {{-- {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.blogs.title'), 'required' => 'required']) }} --}}
        </div><!--col-lg-10-->
    </div><!--form-group-->
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
