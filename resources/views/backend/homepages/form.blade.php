<div class="box-body">
  <div class="form-group">
      {{ Form::label('model_name', 'Nama Kategori', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::select('model_name', ['' => 'Pilih Nama kategori', 'Agenda' => 'Agenda', 'Video' => 'Video', 'SelfDevelopment' => 'SelfDevelopment', 'SelfEnhancement' => 'SelfEnhancement'], ['class' => 'form-control tags box-size', 'required' => 'required'], ['class' => 'form-control']) }}
      </div><!--col-lg-3-->
  </div><!--form control-->


  <div class="form-group">
      {{ Form::label('post_id', 'Post', ['class' => 'col-lg-2 control-label']) }}

      <div class="col-lg-10">
          {{ Form::select('post_id', [], ['class' => 'form-control tags box-size', 'required' => 'required'], ['class' => 'form-control']) }}
      </div><!--col-lg-3-->
  </div><!--form control-->

  <div class="form-group">
      <!-- Create Your Field Label Here -->
      <!-- Look Below Example for reference -->
      {{ Form::label('sort', 'Urutan', ['class' => 'col-lg-2 control-label required']) }}

      <div class="col-lg-10">
          <!-- Create Your Input Field Here -->
          <!-- Look Below Example for reference -->
          {{ Form::text('sort', null, ['class' => 'form-control box-size', 'placeholder' => 'Urutan (berisi angka 0 dan seterusnya)', 'required' => 'required']) }}
      </div><!--col-lg-10-->
  </div><!--form-group-->

</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $( document ).ready( function() {
          if(window.location.pathname.split('/')[4] == 'edit'){
            $('#model_name').val('{{ $homepages->model_name }}');
              //Everything in here would execute after the DOM is ready to manipulated.
            $.getJSON("{{ url('admin/getposts/')}}/" + $('#model_name').val() ,
            function(data) {
                var model = $('#post_id');
                model.empty();
                $.each(data, function(index, element) {
                    if('{{ $homepages->post_id }}' == element.id){
                    model.append("<option value='"+element.id+"' selected>" + element.name + "</option>");

                  }else{
                    model.append("<option value='"+element.id+"'>" + element.name + "</option>");

                  }
                });
            });

          }else{
            //Everything in here would execute after the DOM is ready to manipulated.
            $('#model_name').change(function(){
              if($(this).val() != ''){
                $.getJSON("{{ url('admin/getposts/')}}/" + $(this).val() ,
                function(data) {
                    var model = $('#post_id');
                    model.empty();
                    $.each(data, function(index, element) {
                        model.append("<option value='"+element.id+"'>" + element.name + "</option>");
                    });
                });
              }else{
                var model = $('#post_id');
                model.empty();
              }
            });            
          }


        });
    </script>
@endsection
