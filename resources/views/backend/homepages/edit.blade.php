@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.homepages.management') . ' | ' . trans('labels.backend.homepages.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.homepages.management') }}
        <small>{{ trans('labels.backend.homepages.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($homepages, ['route' => ['admin.homepages.update', $homepages], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-homepage']) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.homepages.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.homepages.partials.homepages-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.homepages.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.homepages.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!--form-group-->
            </div><!--box-body-->
        </div><!--box box-success -->
    {{ Form::close() }}
@endsection
