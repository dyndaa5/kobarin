@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.selfenhancements.management') . ' | ' . trans('labels.backend.selfenhancements.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.selfenhancements.management') }}
        <small>{{ trans('labels.backend.selfenhancements.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($selfenhancements, ['route' => ['admin.selfenhancements.update', $selfenhancements], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-selfenhancement', 'files' => true]) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.selfenhancements.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.selfenhancements.partials.selfenhancements-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.selfenhancements.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.selfenhancements.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!--form-group-->
            </div><!--box-body-->
        </div><!--box box-success -->
    {{ Form::close() }}
@endsection
