<?php

namespace App\Repositories\Backend\Network;

use DB;
use Carbon\Carbon;
use App\Models\Network\Network;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NetworkRepository.
 */
class NetworkRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Network::class;

    /*
    * Find page by page_slug
    */
    public function findBySlug($page_slug)
    {
        if (!is_null($this->query()->wherePage_slug($page_slug)->firstOrFail())) {
            return $this->query()->wherePage_slug($page_slug)->firstOrFail();
        }

        throw new GeneralException(trans('exceptions.backend.access.pages.not_found'));
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.networks.table').'.id',
                config('module.networks.table').'.created_at',
                config('module.networks.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Network::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.networks.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Network $network
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Network $network, array $input)
    {
    	if ($network->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.networks.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Network $network
     * @throws GeneralException
     * @return bool
     */
    public function delete(Network $network)
    {
        if ($network->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.networks.delete_error'));
    }
}
