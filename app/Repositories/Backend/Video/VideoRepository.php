<?php

namespace App\Repositories\Backend\Video;

use DB;
use Carbon\Carbon;
use App\Models\Video\Video;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


/**
 * Class VideoRepository.
 */
class VideoRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Video::class;

    public function __construct()
    {
        $this->featured_image_path = 'img'.DIRECTORY_SEPARATOR.'featurd-image'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.videos.table').'.id',
                config('module.videos.table').'.name',
                config('module.videos.table').'.slug',
                config('module.videos.table').'.status',
                config('module.videos.table').'.created_at',
                config('module.videos.table').'.updated_at',
            ]);
    }

    // /**
    //  * For Creating the respective model in storage
    //  *
    //  * @param array $input
    //  * @throws GeneralException
    //  * @return bool
    //  */
    // public function create(array $input)
    // {
    //     if (Video::create($input)) {
    //         return true;
    //     }
    //     throw new GeneralException(trans('exceptions.backend.videos.create_error'));
    // }
    //
    // /**
    //  * For updating the respective Model in storage
    //  *
    //  * @param Video $video
    //  * @param  $input
    //  * @throws GeneralException
    //  * return bool
    //  */
    // public function update(Video $video, array $input)
    // {
    // 	if ($video->update($input))
    //         return true;
    //
    //     throw new GeneralException(trans('exceptions.backend.videos.update_error'));
    // }

    /**
     * @param \App\Models\Settings\Setting $setting
     * @param array                        $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

      $video = Video::create($input);

      if (!empty($input['featured_image'])) {
          $input['featured_image'] = $this->uploadLogo($video, $input['featured_image'], 'featured_image');
      }

      if ($video->update($input)) {
          return true;
      }

      throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }

    /**
     * @param \App\Models\Settings\Setting $setting
     * @param array                        $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function update(Video $video, array $input)
    {
        if (!empty($input['featured_image'])) {
            $this->removeLogo($video, 'featured_image');

            $input['featured_image'] = $this->uploadLogo($video, $input['featured_image'], 'featured_image');
        }

        if ($video->update($input)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }

    /*
     * Upload logo image
     */
    public function uploadLogo(Video $video, $image, $type)
    {
        $path = $this->featured_image_path;

        $image_name = time().'.'.$image->extension();

        $this->storage->put($path.$image_name, file_get_contents($image->getRealPath()));

        return $image_name;
    }

    /*
     * remove logo or favicon icon
     */
    public function removeLogo(Video $video, $type)
    {
        $path = $this->featured_image_path;

        if ($video->$type && $this->storage->exists($path.$video->$type)) {
            $this->storage->delete($path.$video->$type);
        }

        $result = $video->update([$type => null]);

        if ($result) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }


    /**
     * For deleting the respective model from storage
     *
     * @param Video $video
     * @throws GeneralException
     * @return bool
     */
    public function delete(Video $video)
    {
        if ($video->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.videos.delete_error'));
    }
}
