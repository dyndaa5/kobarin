<?php

namespace App\Repositories\Backend\Agenda;

use DB;
use Carbon\Carbon;
use App\Models\Agenda\Agenda;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class AgendaRepository.
 */
class AgendaRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Agenda::class;

    public function __construct()
    {
        $this->featured_image_path = 'img'.DIRECTORY_SEPARATOR.'featurd-image'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }


    /*
    * Find page by page_slug
    */
    public function findBySlug($page_slug)
    {
        if (!is_null($this->query()->wherePage_slug($page_slug)->firstOrFail())) {
            return $this->query()->wherePage_slug($page_slug)->firstOrFail();
        }

        throw new GeneralException(trans('exceptions.backend.access.pages.not_found'));
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.agendas.table').'.id',
                config('module.agendas.table').'.name',
                config('module.agendas.table').'.slug',
                config('module.agendas.table').'.status',
                config('module.agendas.table').'.created_at',
                config('module.agendas.table').'.updated_at',
            ]);
    }

    // /**
    //  * For Creating the respective model in storage
    //  *
    //  * @param array $input
    //  * @throws GeneralException
    //  * @return bool
    //  */
    // public function create(array $input)
    // {
    //     if (Agenda::create($input)) {
    //         return true;
    //     }
    //     throw new GeneralException(trans('exceptions.backend.agendas.create_error'));
    // }

    // /**
    //  * For updating the respective Model in storage
    //  *
    //  * @param Agenda $agenda
    //  * @param  $input
    //  * @throws GeneralException
    //  * return bool
    //  */
    // public function update(Agenda $agenda, array $input)
    // {
    // 	if ($agenda->update($input))
    //         return true;
    //
    //     throw new GeneralException(trans('exceptions.backend.agendas.update_error'));
    // }

    /**
     * @param \App\Models\Settings\Setting $setting
     * @param array                        $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function update(Agenda $agenda, array $input)
    {
        if (!empty($input['featured_image'])) {
            $this->removeLogo($agenda, 'featured_image');

            $input['featured_image'] = $this->uploadLogo($agenda, $input['featured_image'], 'featured_image');
        }

        if ($agenda->update($input)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }

    /**
     * @param \App\Models\Settings\Setting $setting
     * @param array                        $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
        $agenda = Agenda::create($input);

        if (!empty($input['featured_image'])) {
            $input['featured_image'] = $this->uploadLogo($agenda, $input['featured_image'], 'featured_image');
        }

        if ($agenda->update($input)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }

    /*
     * Upload logo image
     */
    public function uploadLogo(Agenda $agenda, $image, $type)
    {
        $path = $this->featured_image_path;

        $image_name = time().'.'.$image->getClientOriginalExtension();
        $this->storage->put($path.$image_name, file_get_contents($image->getRealPath()));

        return $image_name;
    }

    /*
     * remove logo or favicon icon
     */
    public function removeLogo(Agenda $agenda, $type)
    {
        $path = $this->featured_image_path;

        if ($agenda->$type && $this->storage->exists($path.$agenda->$type)) {
            $this->storage->delete($path.$agenda->$type);
        }

        $result = $agenda->update([$type => null]);

        if ($result) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }



    /**
     * For deleting the respective model from storage
     *
     * @param Agenda $agenda
     * @throws GeneralException
     * @return bool
     */
    public function delete(Agenda $agenda)
    {
        if ($agenda->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.agendas.delete_error'));
    }
}
