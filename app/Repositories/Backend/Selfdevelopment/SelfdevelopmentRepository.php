<?php

namespace App\Repositories\Backend\Selfdevelopment;

use DB;
use Carbon\Carbon;
use App\Models\Selfdevelopment\Selfdevelopment;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class SelfdevelopmentRepository.
 */
class SelfdevelopmentRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Selfdevelopment::class;

    public function __construct()
    {
        $this->featured_image_path = 'img'.DIRECTORY_SEPARATOR.'featurd-image'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /*
    * Find page by page_slug
    */
    public function findBySlug($page_slug)
    {
        if (!is_null($this->query()->wherePage_slug($page_slug)->firstOrFail())) {
            return $this->query()->wherePage_slug($page_slug)->firstOrFail();
        }

        throw new GeneralException(trans('exceptions.backend.access.pages.not_found'));
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.selfdevelopments.table').'.id',
                config('module.selfdevelopments.table').'.name',
                config('module.selfdevelopments.table').'.slug',
                config('module.selfdevelopments.table').'.status',
                config('module.selfdevelopments.table').'.created_at',
                config('module.selfdevelopments.table').'.updated_at',
            ]);
    }

    /**
     * @param \App\Models\Settings\Setting $setting
     * @param array                        $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

      $selfdevelopment = Selfdevelopment::create($input);

      if (!empty($input['featured_image'])) {
          $input['featured_image'] = $this->uploadLogo($selfdevelopment, $input['featured_image'], 'featured_image');
      }

      if ($selfdevelopment->update($input)) {
          return true;
      }

      throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }

    /**
     * @param \App\Models\Settings\Setting $setting
     * @param array                        $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function update(Selfdevelopment $selfdevelopment, array $input)
    {
        if (!empty($input['featured_image'])) {
            $this->removeLogo($selfdevelopment, 'featured_image');

            $input['featured_image'] = $this->uploadLogo($selfdevelopment, $input['featured_image'], 'featured_image');
        }

        if ($selfdevelopment->update($input)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }

    /*
     * Upload logo image
     */
    public function uploadLogo(Selfdevelopment $selfdevelopment, $image, $type)
    {

        $path = $this->featured_image_path;

        $image_name = time().'.'.$image->getClientOriginalExtension();

        $this->storage->put($path.$image_name, file_get_contents($image->getRealPath()));

        return $image_name;
    }

    /*
     * remove logo or favicon icon
     */
    public function removeLogo(Selfdevelopment $selfdevelopment, $type)
    {
        $path = $this->featured_image_path;

        if ($selfdevelopment->$type && $this->storage->exists($path.$selfdevelopment->$type)) {
            $this->storage->delete($path.$selfdevelopment->$type);
        }

        $result = $selfdevelopment->update([$type => null]);

        if ($result) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }
    /**
     * For deleting the respective model from storage
     *
     * @param Selfdevelopment $selfdevelopment
     * @throws GeneralException
     * @return bool
     */
    public function delete(Selfdevelopment $selfdevelopment)
    {
        if ($selfdevelopment->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.selfdevelopments.delete_error'));
    }
}
