<?php

namespace App\Repositories\Backend\Newsletter;

use DB;
use Carbon\Carbon;
use App\Models\Newsletter\Newsletter;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NewsletterRepository.
 */
class NewsletterRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Newsletter::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.newsletters.table').'.id',
                config('module.newsletters.table').'.created_at',
                config('module.newsletters.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Newsletter::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.newsletters.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Newsletter $newsletter
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Newsletter $newsletter, array $input)
    {
    	if ($newsletter->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.newsletters.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Newsletter $newsletter
     * @throws GeneralException
     * @return bool
     */
    public function delete(Newsletter $newsletter)
    {
        if ($newsletter->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.newsletters.delete_error'));
    }
}
