<?php

namespace App\Repositories\Backend\Homepage;

use DB;
use Carbon\Carbon;
use App\Models\Homepage\Homepage;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class HomepageRepository.
 */
class HomepageRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Homepage::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.homepages.table').'.id',
                config('module.homepages.table').'.model_name',
                config('module.homepages.table').'.post_id',
                config('module.homepages.table').'.sort',
                config('module.homepages.table').'.created_at',
                config('module.homepages.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Homepage::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.homepages.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Homepage $homepage
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Homepage $homepage, array $input)
    {
    	if ($homepage->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.homepages.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Homepage $homepage
     * @throws GeneralException
     * @return bool
     */
    public function delete(Homepage $homepage)
    {
        if ($homepage->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.homepages.delete_error'));
    }
}
