<?php

namespace App\Repositories\Backend\Banner;

use DB;
use Carbon\Carbon;
use App\Models\Banner\Banner;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class BannerRepository.
 */
class BannerRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Banner::class;


    public function __construct()
    {
        $this->featured_image_path = 'img'.DIRECTORY_SEPARATOR.'featurd-image'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.banners.table').'.id',
                config('module.banners.table').'.name',
                config('module.banners.table').'.featured_image',
                config('module.banners.table').'.created_at',
                config('module.banners.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Banner::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.banners.create_error'));
    }

    /**
     * @param \App\Models\Settings\Setting $setting
     * @param array                        $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function update(Banner $banner, array $input)
    {
        if (!empty($input['featured_image'])) {
            $this->removeLogo($banner, 'featured_image');

            $input['featured_image'] = $this->uploadLogo($banner, $input['featured_image'], 'featured_image');
        }

        if ($banner->update($input)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }

    /*
     * Upload logo image
     */
    public function uploadLogo(Banner $banner, $image, $type)
    {
        $path = $this->featured_image_path;

        $image_name = time();

        $this->storage->put($path.$image_name, file_get_contents($image->getRealPath()));

        return $image_name;
    }

    /*
     * remove logo or favicon icon
     */
    public function removeLogo(Banner $banner, $type)
    {
        $path = $this->featured_image_path;

        if ($banner->$type && $this->storage->exists($path.$banner->$type)) {
            $this->storage->delete($path.$banner->$type);
        }

        $result = $banner->update([$type => null]);

        if ($result) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }

    // /**
    //  * For updating the respective Model in storage
    //  *
    //  * @param Banner $banner
    //  * @param  $input
    //  * @throws GeneralException
    //  * return bool
    //  */
    // public function update(Banner $banner, array $input)
    // {
    // 	if ($banner->update($input))
    //         return true;
    //
    //     throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    // }

    /**
     * For deleting the respective model from storage
     *
     * @param Banner $banner
     * @throws GeneralException
     * @return bool
     */
    public function delete(Banner $banner)
    {
        if ($banner->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.delete_error'));
    }
}
