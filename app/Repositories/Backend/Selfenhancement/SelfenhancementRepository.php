<?php

namespace App\Repositories\Backend\Selfenhancement;

use DB;
use Carbon\Carbon;
use App\Models\Selfenhancement\Selfenhancement;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


/**
 * Class SelfenhancementRepository.
 */
class SelfenhancementRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Selfenhancement::class;

    public function __construct()
    {
        $this->featured_image_path = 'img'.DIRECTORY_SEPARATOR.'featurd-image'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }


    /*
    * Find page by page_slug
    */
    public function findBySlug($page_slug)
    {
        if (!is_null($this->query()->whereSlug($page_slug)->firstOrFail())) {
            return $this->query()->whereSlug($page_slug)->firstOrFail();
        }

        throw new GeneralException(trans('exceptions.backend.access.pages.not_found'));
    }


    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.selfenhancements.table').'.id',
                config('module.selfenhancements.table').'.name',
                config('module.selfenhancements.table').'.slug',
                config('module.selfenhancements.table').'.status',
                config('module.selfenhancements.table').'.created_at',
                config('module.selfenhancements.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {

      $selfenhancement = Selfenhancement::create($input);

      if (!empty($input['featured_image'])) {
          $input['featured_image'] = $this->uploadLogo($selfenhancement, $input['featured_image'], 'featured_image');
      }

      if ($selfenhancement->update($input)) {
          return true;
      }

        throw new GeneralException(trans('exceptions.backend.selfenhancements.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Selfenhancement $selfenhancement
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Selfenhancement $selfenhancement, array $input)
    {

        if (!empty($input['featured_image'])) {
            $this->removeLogo($selfenhancement, 'featured_image');

            $input['featured_image'] = $this->uploadLogo($selfenhancement, $input['featured_image'], 'featured_image');
        }

        if ($selfenhancement->update($input)) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.selfenhancements.update_error'));
    }


    /*
     * Upload logo image
     */
    public function uploadLogo(Selfenhancement $selfenhancement, $image, $type)
    {

        $path = $this->featured_image_path;

        $image_name = time().'.'.$image->getClientOriginalExtension();

        $this->storage->put($path.$image_name, file_get_contents($image->getRealPath()));

        return $image_name;
    }

    /*
     * remove logo or favicon icon
     */
    public function removeLogo(Selfenhancement $selfenhancement, $type)
    {
        $path = $this->featured_image_path;

        if ($selfenhancement->$type && $this->storage->exists($path.$selfenhancement->$type)) {
            $this->storage->delete($path.$selfenhancement->$type);
        }

        $result = $selfenhancement->update([$type => null]);

        if ($result) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.banners.update_error'));
    }


    /**
     * For deleting the respective model from storage
     *
     * @param Selfenhancement $selfenhancement
     * @throws GeneralException
     * @return bool
     */
    public function delete(Selfenhancement $selfenhancement)
    {
        if ($selfenhancement->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.selfenhancements.delete_error'));
    }
}
