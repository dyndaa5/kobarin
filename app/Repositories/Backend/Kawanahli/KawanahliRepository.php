<?php

namespace App\Repositories\Backend\Kawanahli;

use DB;
use Carbon\Carbon;
use App\Models\Kawanahli\Kawanahli;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class KawanahliRepository.
 */
class KawanahliRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Kawanahli::class;

    /*
    * Find page by page_slug
    */
    public function findBySlug($page_slug)
    {
        if (!is_null($this->query()->wherePage_slug($page_slug)->firstOrFail())) {
            return $this->query()->wherePage_slug($page_slug)->firstOrFail();
        }

        throw new GeneralException(trans('exceptions.backend.access.pages.not_found'));
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->leftjoin(config('access.users_table'), config('access.users_table').'.id', '=', config('module.kawanahlis.table').'.user_id')
            ->select([
                config('module.kawanahlis.table').'.id',
                config('access.users_table').'.first_name',
                config('access.users_table').'.last_name',
                config('access.users_table').'.email',
                config('module.kawanahlis.table').'.created_at',
                config('module.kawanahlis.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (Kawanahli::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.kawanahlis.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Kawanahli $kawanahli
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Kawanahli $kawanahli, array $input)
    {
    	if ($kawanahli->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.kawanahlis.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Kawanahli $kawanahli
     * @throws GeneralException
     * @return bool
     */
    public function delete(Kawanahli $kawanahli)
    {
        if ($kawanahli->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.kawanahlis.delete_error'));
    }
}
