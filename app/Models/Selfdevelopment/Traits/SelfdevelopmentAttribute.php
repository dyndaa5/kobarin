<?php

namespace App\Models\Selfdevelopment\Traits;

/**
 * Class SelfdevelopmentAttribute.
 */
trait SelfdevelopmentAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/5.4/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                '.$this->getEditButtonAttribute("edit-selfdevelopment", "admin.selfdevelopments.edit").'
                '.$this->getDeleteButtonAttribute("delete-selfdevelopment", "admin.selfdevelopments.destroy").'
                </div>';
    }
}
