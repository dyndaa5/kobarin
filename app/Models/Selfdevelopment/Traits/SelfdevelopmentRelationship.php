<?php

namespace App\Models\Selfdevelopment\Traits;

use App\Models\Access\User\User;
/**
 * Class SelfdevelopmentRelationship
 */
trait SelfdevelopmentRelationship
{
    /*
    * put you model relationships here
    * Take below example for reference
    */
    /*
    public function users() {
        //Note that the below will only work if user is represented as user_id in your table
        //otherwise you have to provide the column name as a parameter
        //see the documentation here : https://laravel.com/docs/5.4/eloquent-relationships
        $this->belongsTo(User::class);
    }
     */

     public function user() {
         //Note that the below will only work if user is represented as user_id in your table
         //otherwise you have to provide the column name as a parameter
         //see the documentation here : https://laravel.com/docs/5.4/eloquent-relationships
        return $this->belongsTo(User::class, 'created_by');
     }

    /**
    * Blogs has many relationship with categories.
    */
    public function categories()
    {
       return $this->hasMany(Category::class);
    }

}
