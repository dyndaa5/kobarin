<?php

namespace App\Models\Network\Traits;

/**
 * Class NetworkAttribute.
 */
trait NetworkAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/5.4/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                '.$this->getEditButtonAttribute("edit-network", "admin.networks.edit").'
                '.$this->getDeleteButtonAttribute("delete-network", "admin.networks.destroy").'
                </div>';
    }
}
