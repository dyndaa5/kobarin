<?php

namespace App\Models\Agenda\Traits;

/**
 * Class AgendaAttribute.
 */
trait AgendaAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/5.4/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                '.$this->getEditButtonAttribute("edit-agenda", "admin.agendas.edit").'
                '.$this->getDeleteButtonAttribute("delete-agenda", "admin.agendas.destroy").'
                </div>';
    }
}
