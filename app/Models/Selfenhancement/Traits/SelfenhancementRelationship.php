<?php

namespace App\Models\Selfenhancement\Traits;

use App\Models\Access\User\User;

/**
 * Class SelfenhancementRelationship
 */
trait SelfenhancementRelationship
{
    /*
    * put you model relationships here
    * Take below example for reference
    */
    /*

     */
     public function user() {
         //Note that the below will only work if user is represented as user_id in your table
         //otherwise you have to provide the column name as a parameter
         //see the documentation here : https://laravel.com/docs/5.4/eloquent-relationships
        return $this->belongsTo(User::class, 'created_by');
     }

}
