<?php

namespace App\Models\Selfenhancement;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Selfenhancement\Traits\SelfenhancementAttribute;
use App\Models\Selfenhancement\Traits\SelfenhancementRelationship;
use KevinPijning\LaravelSearchable\Searchable;
use CyrildeWit\EloquentViewable\Viewable;

class Selfenhancement extends Model
{
    use ModelTrait,
        SelfenhancementAttribute,
        Searchable,
        Viewable,
    	SelfenhancementRelationship {
            // SelfenhancementAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/5.4/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'selfenhancements';

    public $searchable = ['id',
  	                    'name',
  	                    'content',
  	                    'created_at',
  	                    'updated_at'];

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
      'name', 'publish_datetime', 'featured_image', 'content', 'meta_title', 'cannonical_link', 'slug', 'meta_description', 'meta_keywords', 'status', 'created_by', 'updated_by'
    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
