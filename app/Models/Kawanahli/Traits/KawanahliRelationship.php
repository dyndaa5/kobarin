<?php

namespace App\Models\Kawanahli\Traits;

/**
 * Class KawanahliRelationship
 */
trait KawanahliRelationship
{
    /*
    * put you model relationships here
    * Take below example for reference
    */
    /*
    public function users() {
        //Note that the below will only work if user is represented as user_id in your table
        //otherwise you have to provide the column name as a parameter
        //see the documentation here : https://laravel.com/docs/5.4/eloquent-relationships
        $this->belongsTo(User::class);
    }
     */


    /**
    * Blogs belongsTo with User.
    */
    public function user()
    {
       return $this->belongsTo(User::class, 'user_id');
    }
}
