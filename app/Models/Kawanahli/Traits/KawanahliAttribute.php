<?php

namespace App\Models\Kawanahli\Traits;

/**
 * Class KawanahliAttribute.
 */
trait KawanahliAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/5.4/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                '.$this->getEditButtonAttribute("edit-kawanahli", "admin.kawanahlis.edit").'
                '.$this->getDeleteButtonAttribute("delete-kawanahli", "admin.kawanahlis.destroy").'
                </div>';
    }
}
