<?php

namespace App\Models\Kawanahli;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Kawanahli\Traits\KawanahliAttribute;
use App\Models\Kawanahli\Traits\KawanahliRelationship;

class Kawanahli extends Model
{
    use ModelTrait,
        KawanahliAttribute,
    	KawanahliRelationship {
            // KawanahliAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/5.4/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'kawanahlis';

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
      'user_id', 'user_type', 'institusi', 'researchgate', 'linkedin', 'twitter', 'facebook', 'website', 'youtube', 'lisensi_professional', 'about', 'featured_image'
    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
