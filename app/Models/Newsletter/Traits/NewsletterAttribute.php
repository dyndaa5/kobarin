<?php

namespace App\Models\Newsletter\Traits;

/**
 * Class NewsletterAttribute.
 */
trait NewsletterAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/5.4/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                '.$this->getEditButtonAttribute("edit-newsletter", "admin.newsletters.edit").'
                '.$this->getDeleteButtonAttribute("delete-newsletter", "admin.newsletters.destroy").'
                </div>';
    }
}
