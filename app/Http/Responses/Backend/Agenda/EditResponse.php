<?php

namespace App\Http\Responses\Backend\Agenda;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Agenda\Agenda
     */
    protected $agendas;

    /**
     * @param App\Models\Agenda\Agenda $agendas
     */
    public function __construct($agendas)
    {
        $this->agendas = $agendas;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.agendas.edit')->with([
            'agendas' => $this->agendas
        ]);
    }
}