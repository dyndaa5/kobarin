<?php

namespace App\Http\Responses\Backend\Network;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Network\Network
     */
    protected $networks;

    /**
     * @param App\Models\Network\Network $networks
     */
    public function __construct($networks)
    {
        $this->networks = $networks;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.networks.edit')->with([
            'networks' => $this->networks
        ]);
    }
}