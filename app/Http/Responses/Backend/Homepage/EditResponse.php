<?php

namespace App\Http\Responses\Backend\Homepage;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Homepage\Homepage
     */
    protected $homepages;

    /**
     * @param App\Models\Homepage\Homepage $homepages
     */
    public function __construct($homepages)
    {
        $this->homepages = $homepages;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.homepages.edit')->with([
            'homepages' => $this->homepages
        ]);
    }
}