<?php

namespace App\Http\Responses\Backend\Newsletter;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Newsletter\Newsletter
     */
    protected $newsletters;

    /**
     * @param App\Models\Newsletter\Newsletter $newsletters
     */
    public function __construct($newsletters)
    {
        $this->newsletters = $newsletters;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.newsletters.edit')->with([
            'newsletters' => $this->newsletters
        ]);
    }
}