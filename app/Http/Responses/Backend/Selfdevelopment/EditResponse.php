<?php

namespace App\Http\Responses\Backend\Selfdevelopment;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Selfdevelopment\Selfdevelopment
     */
    protected $selfdevelopments;

    /**
     * @param App\Models\Selfdevelopment\Selfdevelopment $selfdevelopments
     */
    public function __construct($selfdevelopments)
    {
        $this->selfdevelopments = $selfdevelopments;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.selfdevelopments.edit')->with([
            'selfdevelopments' => $this->selfdevelopments
        ]);
    }
}