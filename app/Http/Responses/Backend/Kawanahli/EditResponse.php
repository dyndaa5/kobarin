<?php

namespace App\Http\Responses\Backend\Kawanahli;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Kawanahli\Kawanahli
     */
    protected $kawanahlis;

    /**
     * @param App\Models\Kawanahli\Kawanahli $kawanahlis
     */
    public function __construct($kawanahlis)
    {
        $this->kawanahlis = $kawanahlis;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.kawanahlis.edit')->with([
            'kawanahlis' => $this->kawanahlis
        ]);
    }
}