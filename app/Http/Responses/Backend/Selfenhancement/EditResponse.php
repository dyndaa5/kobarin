<?php

namespace App\Http\Responses\Backend\Selfenhancement;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Selfenhancement\Selfenhancement
     */
    protected $selfenhancements;

    /**
     * @param App\Models\Selfenhancement\Selfenhancement $selfenhancements
     */
    public function __construct($selfenhancements)
    {
        $this->selfenhancements = $selfenhancements;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.selfenhancements.edit')->with([
            'selfenhancements' => $this->selfenhancements
        ]);
    }
}