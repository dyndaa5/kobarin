<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Elite50\E50MailLaravel\Facades\E50Mail;
use Newsletter;
use App\Mail\SubscribeWelcome;
use App\Models\Newsletter\Newsletter as NewsletterModel;
use App\Repositories\Backend\Newsletter\NewsletterRepository;
use App\Models\Access\User\User;
use App\Http\Responses\RedirectResponse;

/**
 * Class FrontendController.
 */
class SubscribeController extends Controller
{

    /**
     * variable to store the repository object
     * @var NewsletterRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param NewsletterRepository $repository;
     */
    public function __construct(NewsletterRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * show page by $page_slug.
     */
    public function create(Request $input)
    {
      //Create the model using repository create method

      if($input->email){
        if ( ! Newsletter::isSubscribed($input->email) ) {
            $user = User::where('email', $input->email)->first();
            $subscribed_user = NewsletterModel::where('email', $input->email)->first();
            if(!is_null($user)){
              if(is_null($subscribed_user)){
                $this->repository->create(array('email' => $input->email, 'user_id' => $user->id));
                 Newsletter::subscribe($input->email);
                 \Mail::to($input->email)->send(new SubscribeWelcome);
                return new RedirectResponse(route('frontend.index'), ['flash_success' => 'Sukses subscribe newsletter!']);
              }else{
                return new RedirectResponse(route('frontend.index'), ['flash_success' => 'Maaf anda telah terdaftar di subscribe list!']);
              }

            }  else{
              return new RedirectResponse(route('frontend.index'), ['flash_success' => 'Maaf anda belum terdaftar di situs kami, silahkan anda daftar terlebih dahulu']);
            }
         }
      }else{
        return new RedirectResponse(route('frontend.index'), ['flash_success' => 'Maaf anda tidak memasukkan inputan apapun.']);

      }

    }
}
