<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Agenda\Agenda;
use App\Models\Banner\Banner;
/**
 * Class FrontendController.
 */
class AgendaController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $settingData = Setting::first();
        $google_analytics = $settingData->google_analytics;
        $items_internal = [];
        $items_eksternal = [];
        foreach(Agenda::where('status', 'Published')->orderBy('created_at', 'DESC')->get() as $key => $value) {
          if($value->user->hasRole(4) || $value->user->hasRole(5)){
            $items_internal[] = $value;
          }else if($value->user->hasRole(3)){
            $items_eksternal[] = $value;
          }
        }
        $total_internal   = count($items_internal);
        $total_eksternal = count($items_eksternal);
        $banner = Banner::where('name', 'agenda')->first();

        return view('frontend.agenda.index', ['banner' => $banner, 'items_internal' => $items_internal, 'items_eksternal' => $items_eksternal, 'google_analytics' => $google_analytics, 'setting' => $settingData]);
    }

    /**
     * show page by $page_slug.
     */
    public function show($slug)
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
      $popular_article = Agenda::inRandomOrder()->limit(5)->get();
      $result = Agenda::find($slug);
      $result->addView();

        return view('frontend.layout.articles', ['agenda_terkini' => $popular_article, 'google_analytics' => $google_analytics, 'setting' => $settingData, 'item' => $result])
            ->withpage($result);
    }
}
