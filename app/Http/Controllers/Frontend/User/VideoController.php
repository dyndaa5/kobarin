<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Video\VideoRepository;
use App\Models\Video\Video;
use App\Models\Banner\Banner;
/**
 * Class FrontendController.
 */
class VideoController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $settingData = Setting::first();
        $google_analytics = $settingData->google_analytics;
        $items = Video::where('created_by', \Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        $total = $items->count();
        $active_posts = Video::where('created_by', \Auth::user()->id)->where('status', 'Published')->count();
        $pending_approval_posts = Video::where('created_by', \Auth::user()->id)->where('status', 'Waiting')->count();
        $draft_posts = Video::where('created_by', \Auth::user()->id)->where('status', 'Draft')->count();
        $banner = Banner::where('name', 'video')->first();

        return view('frontend.user.video.index', ['active_posts' => $active_posts, 'pending_posts' => $pending_approval_posts, 'draft_posts' => $draft_posts, 'banner' => $banner, 'google_analytics' => $google_analytics, 'items' => $items, 'setting' => $settingData]);
    }

    /**
     * show page by $page_slug.
     */
    public function show($slug)
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
      $popular_article = Video::inRandomOrder()->limit(5)->get();
      $result = Video::find($slug);
      $result->addView();

        return view('frontend.layout.videos', ['popular_video' => $popular_article, 'google_analytics' => $google_analytics, 'setting' => $settingData, 'item' => $result])
            ->withpage($result);
    }
}
