<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Http\Requests\Frontend\User\DashboardViewRequest;
use App\Models\Selfdevelopment\Selfdevelopment;
use App\Models\Selfenhancement\Selfenhancement;
use App\Models\Agenda\Agenda;
use App\Models\Video\Video;


/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(DashboardViewRequest $request)
    {
        $settingData = Setting::first();
        $google_analytics = $settingData->google_analytics;
        $user = \Auth::user()->id;
        $sd_posts = Selfdevelopment::where('created_by', $user)->count();
        $se_posts = Selfenhancement::where('created_by', $user)->count();
        $agenda_posts = Agenda::where('created_by', $user)->count();
        $video_posts = Video::where('created_by', $user)->count();

        return view('frontend.user.dashboard', ['sd_posts' => $sd_posts, 'se_posts' => $se_posts, 'agenda_posts' => $agenda_posts, 'video_posts' => $video_posts, 'google_analytics' => $google_analytics, 'setting' => $settingData]);
    }
}
