<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Agenda\Agenda;
use App\Models\Banner\Banner;
/**
 * Class FrontendController.
 */
class AgendaController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $settingData = Setting::first();
        $google_analytics = $settingData->google_analytics;
        $items  = Agenda::where('created_by', \Auth::user()->id)->get();
        $total = $items->count();
        $active_posts = Agenda::where('created_by', \Auth::user()->id)->where('status', 'Published')->count();
        $pending_approval_posts = Agenda::where('created_by', \Auth::user()->id)->where('status', 'Waiting')->count();
        $draft_posts = Agenda::where('created_by', \Auth::user()->id)->where('status', 'Draft')->count();
        $banner = Banner::where('name', 'agenda')->first();

        return view('frontend.user.agenda.index', ['active_posts' => $active_posts, 'pending_posts' => $pending_approval_posts, 'draft_posts' => $draft_posts, 'banner' => $banner, 'items' => $items, 'google_analytics' => $google_analytics, 'setting' => $settingData]);
    }

    /**
     * show page by $page_slug.
     */
    public function show($slug)
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
      $popular_article = Agenda::inRandomOrder()->limit(5)->get();
      $result = Agenda::find($slug);
      $result->addView();

        return view('frontend.layout.articles', ['agenda_terkini' => $popular_article, 'google_analytics' => $google_analytics, 'setting' => $settingData, 'item' => $result])
            ->withpage($result);
    }
}
