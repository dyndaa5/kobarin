<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Repositories\Backend\Selfenhancement\SelfenhancementRepository;
use App\Models\Selfenhancement\Selfenhancement;
use App\Models\Banner\Banner;
use App\Models\Video\Video;


/**
 * Class FrontendController.
 */
class SEController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
      $items = Selfenhancement::where('created_by', \Auth::user()->id)->get();
      $total= $items->count();
      $active_posts = Selfenhancement::where('created_by', \Auth::user()->id)->where('status', 'Published')->count();
      $pending_approval_posts = Selfenhancement::where('created_by', \Auth::user()->id)->where('status', 'Waiting')->count();
      $draft_posts = Selfenhancement::where('created_by', \Auth::user()->id)->where('status', 'Draft')->count();
      $banner = Banner::where('name', 'self-enhancement')->first();

      return view('frontend.user.se.index', ['active_posts' => $active_posts, 'pending_posts' => $pending_approval_posts, 'draft_posts' => $draft_posts, 'banner' => $banner, 'google_analytics' => $google_analytics, 'items' => $items, 'setting' => $settingData]);
    }

    /**
     * show page by $page_slug.
     */
    public function show($slug, SelfenhancementRepository $pages)
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
      $popular_article = Selfenhancement::inRandomOrder()->limit(5)->get();
      $popular_video = Video::inRandomOrder()->limit(5)->get();
      $result = Selfenhancement::find($slug);
      $result->addView();

        return view('frontend.layout.articles', ['popular_video' => $popular_video, 'popular_article' => $popular_article, 'google_analytics' => $google_analytics, 'setting' => $settingData, 'item' => $result])
            ->withpage($result);
    }
}
