<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
/**
 * Class AccountController.
 */
class AccountController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
        return view('frontend.user.account', ['google_analytics' => $google_analytics, 'setting' => $settingData]);
    }
}
