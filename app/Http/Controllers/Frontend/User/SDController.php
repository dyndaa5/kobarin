<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Selfdevelopment\Selfdevelopment;
use App\Models\Banner\Banner;
use App\Models\Video\Video;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class FrontendController.
 */
class SDController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $settingData = Setting::first();
        $google_analytics = $settingData->google_analytics;
        $items = Selfdevelopment::where('created_by', \Auth::user()->id)->get();
        $active_posts = Selfdevelopment::where('created_by', \Auth::user()->id)->where('status', 'Published')->count();
        $pending_approval_posts = Selfdevelopment::where('created_by', \Auth::user()->id)->where('status', 'Waiting')->count();
        $draft_posts = Selfdevelopment::where('created_by', \Auth::user()->id)->where('status', 'Draft')->count();
        $totals = $items->count();
        $banner = Banner::where('name', 'self-development')->first();

        return view('frontend.user.sd.index', ['active_posts' => $active_posts, 'pending_posts' => $pending_approval_posts, 'draft_posts' => $draft_posts, 'banner' => $banner, 'google_analytics' => $google_analytics, 'items' => $items, 'setting' => $settingData]);
    }

    /**
     * show page by $page_slug.
     */
    public function show($slug, PagesRepository $pages)
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
      $popular_article = Selfdevelopment::inRandomOrder()->limit(5)->get();
      $popular_video = Video::inRandomOrder()->limit(5)->get();
      $result = Selfdevelopment::find($slug);
      $result->addView();

        return view('frontend.layout.articles', ['popular_video' => $popular_video, 'popular_article' => $popular_article, 'google_analytics' => $google_analytics, 'setting' => $settingData, 'item' => $result])
            ->withpage($result);
    }

    public function datatable()
    {
      return Datatables::of(Selfdevelopment::select([
                      config('module.selfdevelopments.table').'.id',
                      config('module.selfdevelopments.table').'.name',
                      config('module.selfdevelopments.table').'.slug',
                      config('module.selfdevelopments.table').'.status',
                      config('module.selfdevelopments.table').'.created_at',
                      config('module.selfdevelopments.table').'.updated_at',
                  ])->where('selfdevelopments.created_by', \Auth::user()->id))
          ->escapeColumns(['id'])
          ->addColumn('created_at', function ($selfdevelopment) {
              return Carbon::parse($selfdevelopment->created_at)->toDateString();
          })
          ->addColumn('actions', function ($selfdevelopment) {
            return '<div class="btn-group action-btn">
                    <a class="btn btn-default" data-toggle="modal" data-target="#myModal'.$selfdevelopment->id.'">
                      <i class="fa fa-2x fa-eye" aria-hidden="true "></i>
                    </a>
                    <a class="btn btn-warning" data-toggle="modal" data-target="#myModal'.$selfdevelopment->id.'">
                      <i class="fa fa-2x fa-pencil" aria-hidden="true "></i>
                    </a>
                    <a class="btn btn-danger" data-toggle="modal" data-target="#myModal'.$selfdevelopment->id.'">
                      <i class="fa fa-2x fa-trash" aria-hidden="true "></i>
                    </a>
                    </div>';
          })
          ->make(true);
    }
}
