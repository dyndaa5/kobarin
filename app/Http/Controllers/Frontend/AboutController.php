<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Page\Page;

/**
 * Class FrontendController.
 */
class AboutController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $settingData = Setting::first();
        $activePage = Page::where('page_slug', 'tentang-kami')->first();
        $google_analytics = $settingData->google_analytics;

        return view('frontend.about.index', ['activePage' => $activePage, 'google_analytics' => $google_analytics, 'setting' => $settingData]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function panduan()
    {
        $settingData = Setting::first();
        $activePage = Page::where('page_slug', 'panduan-logo')->first();
        $google_analytics = $settingData->google_analytics;

        return view('frontend.about.panduan', ['activePage' => $activePage, 'google_analytics' => $google_analytics, 'setting' => $settingData]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function sejarah()
    {
        $settingData = Setting::first();
        $activePage = Page::where('page_slug', 'sejarah-kobarin')->first();
        $google_analytics = $settingData->google_analytics;

        return view('frontend.about.sejarah', ['activePage' => $activePage, 'google_analytics' => $google_analytics, 'setting' => $settingData]);
    }

}
