<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Repositories\Backend\Selfenhancement\SelfenhancementRepository;
use App\Models\Selfenhancement\Selfenhancement;
use App\Models\Banner\Banner;
use App\Models\Video\Video;

/**
 * Class FrontendController.
 */
class SEController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
      $items_ahli = [];
      $items_publik = [];
      foreach(Selfenhancement::where('status', 'Published')->orderBy('created_at', 'DESC')->get() as $key => $value) {
        if($value->user->hasRole(4) || $value->user->hasRole(5)){
          $items_ahli[] = $value;
        }else if($value->user->hasRole(3)){
          $items_publik[] = $value;
        }
      }

      $total_ahli   = count($items_ahli);
      $total_publik = count($items_publik);
      $banner = Banner::where('name', 'self-enhancement')->first();

      return view('frontend.se.index', ['banner' => $banner, 'google_analytics' => $google_analytics, 'items_ahli' => $items_ahli, 'items_publik' => $items_publik, 'setting' => $settingData]);
    }

    /**
     * show page by $page_slug.
     */
    public function show($slug, SelfenhancementRepository $pages)
    {
      $settingData = Setting::first();
      $result = Selfenhancement::find($slug);
      $google_analytics = $settingData->google_analytics;
      $popular_article = Selfenhancement::where('status', 'Published')->where('id', '!=', $result->id)->inRandomOrder()->limit(5)->get();
      $popular_video = Video::where('status', 'Published')->inRandomOrder()->limit(5)->get();
      $result->addView();

        return view('frontend.layout.articles', ['popular_video' => $popular_video, 'popular_article' => $popular_article, 'google_analytics' => $google_analytics, 'setting' => $settingData, 'item' => $result])
            ->withpage($result);
    }
}
