<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Access\User\User;
use App\Models\Agenda\Agenda;
use App\Models\Access\Role\Role;
use App\Models\Homepage\Homepage;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
      $agenda_internal = [];
      $agenda_eksternal = [];
      $items_sd = [];
      $items_se = [];
      $items_videos = [];
      $items_agenda = Homepage::where('model_name', 'Agenda')->orderBy('sort', 'ASC')->limit(5)->get();
      if(count($items_agenda) > 0){
        $post_ids = $items_agenda->pluck('post_id');
        $items_agendas = \App\Models\Agenda\Agenda::where('status', 'Published')->whereIn('id', $post_ids)->get();

        foreach ($items_agendas as $key => $value) {
          if($value->user->hasRole('Admin')){
            $agenda_internal[] = $value;
          }else{
            $agenda_eksternal[] = $value;
          }
        }
      }

      $sd_rows = Homepage::where('model_name', 'SelfDevelopment')->orderBy('sort', 'DESC')->limit(9)->get();
      if(count($sd_rows) > 0){
        $post_ids = $sd_rows->pluck('post_id');
        $items_sds = \App\Models\Selfdevelopment\Selfdevelopment::where('status', 'Published')->whereIn('id', $post_ids)->get();

        foreach ($items_sds as $key => $value) {
          if($value->user->hasRole('Admin')){
            $items_sd[] = $value;
          }else{
            $items_sd[] = $value;
          }
        }
      }

      $se_rows = Homepage::where('model_name', 'SelfEnhancement')->orderBy('sort', 'DESC')->limit(9)->get();
      if(count($se_rows) > 0){
        $post_ids = $se_rows->pluck('post_id');
        $items_sds = \App\Models\Selfenhancement\Selfenhancement::where('status', 'Published')->whereIn('id', $post_ids)->get();

        foreach ($items_sds as $key => $value) {
          if($value->user->hasRole('Admin')){
            $items_se[] = $value;
          }else{
            $items_se[] = $value;
          }
        }
      }

      $video_rows = Homepage::where('model_name', 'Video')->orderBy('sort', 'ASC')->limit(3)->get();
      if(count($video_rows) > 0){
        $post_ids = $video_rows->pluck('post_id');
        $items_sds = \App\Models\Video\Video::where('status', 'Published')->whereIn('id', $post_ids)->get();

        foreach ($items_sds as $key => $value) {
          if($value->user->hasRole('Admin')){
            $items_videos[] = $value;
          }else{
            $items_videos[] = $value;
          }
        }
      }

      $items_edukasi   = Role::where('name', 'Ahli Edukasi')->first()->users()->limit(3)->get();
      $items_konseling = Role::where('name', 'Ahli Konseling')->first()->users()->limit(3)->get();

      return view('frontend.index', ['ahli_edukasi' => $items_edukasi, 'items_sd' => $items_sd, 'items_se' => $items_se, 'items_videos' => $items_videos, 'ahli_konseling' => $items_konseling, 'agenda_internal' => $agenda_internal,'items_videos' => $items_videos, 'agenda_eksternal' => $agenda_eksternal,'google_analytics' => $google_analytics, 'setting' => $settingData]);
    }

    /**
     * show page by $page_slug.
     */
    public function showPage($slug, PagesRepository $pages)
    {
        $result = $pages->findBySlug($slug);
        $settingData = Setting::first();
        $google_analytics = $settingData->google_analytics;

        return view('frontend.pages.index', ['google_analytics' => $google_analytics, 'setting' => $settingData])
            ->withpage($result);
    }
}
