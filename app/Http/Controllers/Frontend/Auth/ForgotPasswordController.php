<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Models\Settings\Setting;

/**
 * Class ForgotPasswordController.
 */
class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;

        return view('frontend.auth.passwords.email', ['google_analytics' => $google_analytics, 'setting' => $settingData]);
    }
}
