<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Access\User\User;
use App\Models\Banner\Banner;
use App\Models\Selfdevelopment\Selfdevelopment;
use App\Models\Selfenhancement\Selfenhancement;
use App\Models\Access\Role\Role;

/**
 * Class FrontendController.
 */
class AuthorController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $settingData = Setting::first();
        $google_analytics = $settingData->google_analytics;
        $items_edukasi   = Role::where('name', 'Ahli Edukasi')->first()->users()->get();
        $items_konseling = Role::where('name', 'Ahli Konseling')->first()->users()->get();
        $total_edukasi   = $items_edukasi->count();
        $total_konseling = $items_konseling->count();
        $banner = Banner::where('name', 'daftar-ahli')->first();

        return view('frontend.author.index', ['banner' => $banner, 'items_edukasi' => $items_edukasi, 'items_konseling' => $items_konseling, 'google_analytics' => $google_analytics, 'setting' => $settingData]);
    }

    /**
     * show page by $page_slug.
     */
    public function show($slug, PagesRepository $pages)
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
      $result = User::find($slug);

      $sd_posts = Selfdevelopment::where('created_by', $result->id)->where('status', 'Published')->get();
      $se_posts = Selfenhancement::where('created_by', $result->id)->where('status', 'Published')->get();

      // $articles = array_merge($sd_posts, $se_posts);
      return view('frontend.author.show', ['sd_posts' => $sd_posts, 'se_posts' => $se_posts, 'google_analytics' => $google_analytics, 'setting' => $settingData, 'user' => $result])
          ->withpage($result);
    }

}
