<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Video\VideoRepository;
use App\Models\Video\Video;
use App\Models\Banner\Banner;
/**
 * Class FrontendController.
 */
class VideoController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $settingData = Setting::first();
        $google_analytics = $settingData->google_analytics;
        $items_populer = [];
        $items_terbaru = [];
        foreach(Video::where('status', 'Published')->orderBy('created_at', 'DESC')->get() as $key => $value) {
          if($value->user->hasRole(4) || $value->user->hasRole(5)){
            $items_populer[] = $value;
          }else if($value->user->hasRole(3)){
            $items_terbaru[] = $value;
          }
        }
        $total_populer   = count($items_populer);
        $total_terbaru = count($items_terbaru);
        $banner = Banner::where('name', 'video')->first();

        return view('frontend.video.index', ['banner' => $banner, 'google_analytics' => $google_analytics, 'items_populer' => $items_populer, 'items_terbaru' => $items_terbaru, 'setting' => $settingData]);
    }

    /**
     * show page by $page_slug.
     */
    public function show($slug)
    {
      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;
      $popular_article = Video::inRandomOrder()->limit(5)->get();
      $result = Video::find($slug);
      $result->addView();

        return view('frontend.layout.videos', ['popular_video' => $popular_article, 'google_analytics' => $google_analytics, 'setting' => $settingData, 'item' => $result])
            ->withpage($result);
    }
}
