<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use App\Models\Selfdevelopment\Selfdevelopment;
use App\Models\Selfenhancement\Selfenhancement;
use App\Models\Access\User\User;
use Illuminate\Http\Request;

/**
 * Class FrontendController.
 */
class SearchController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $input)
    {
      $sd_results = [];
      $se_results = [];
      $ahli_results = [];
      if($input->keywords != '')
      {
        $sd_results = Selfdevelopment::where('name', 'like', '%' . $input->keywords . '%')->get();
        $se_results = Selfenhancement::where('name', 'like', '%' . $input->keywords . '%')->get();
        $ahli_results = User::where('first_name', 'like', '%' . $input->keywords . '%')->orWhere('first_name', 'like', '%' . $input->keywords . '%')->get();
      }

      $settingData = Setting::first();
      $google_analytics = $settingData->google_analytics;

      return view('frontend.search.index', ['sd_results' => $sd_results, 'se_results' => $se_results, 'ahli_results' => $ahli_results, 'setting' => $settingData, 'google_analytics' => $google_analytics]);
    }
}
