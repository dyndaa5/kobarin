<?php

namespace App\Http\Controllers\Backend\Selfenhancement;

use App\Models\Selfenhancement\Selfenhancement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Selfenhancement\CreateResponse;
use App\Http\Responses\Backend\Selfenhancement\EditResponse;
use App\Repositories\Backend\Selfenhancement\SelfenhancementRepository;
use App\Http\Requests\Backend\Selfenhancement\ManageSelfenhancementRequest;
use App\Http\Requests\Backend\Selfenhancement\CreateSelfenhancementRequest;
use App\Http\Requests\Backend\Selfenhancement\StoreSelfenhancementRequest;
use App\Http\Requests\Backend\Selfenhancement\EditSelfenhancementRequest;
use App\Http\Requests\Backend\Selfenhancement\UpdateSelfenhancementRequest;
use App\Http\Requests\Backend\Selfenhancement\DeleteSelfenhancementRequest;

/**
 * SelfenhancementsController
 */
class SelfenhancementsController extends Controller
{
    /**
     * variable to store the repository object
     * @var SelfenhancementRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param SelfenhancementRepository $repository;
     */
    public function __construct(SelfenhancementRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Selfenhancement\ManageSelfenhancementRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageSelfenhancementRequest $request)
    {
        return new ViewResponse('backend.selfenhancements.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateSelfenhancementRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Selfenhancement\CreateResponse
     */
    public function create(CreateSelfenhancementRequest $request)
    {
        return new CreateResponse('backend.selfenhancements.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreSelfenhancementRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreSelfenhancementRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.selfenhancements.index'), ['flash_success' => trans('alerts.backend.selfenhancements.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Selfenhancement\Selfenhancement  $selfenhancement
     * @param  EditSelfenhancementRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Selfenhancement\EditResponse
     */
    public function edit(Selfenhancement $selfenhancement, EditSelfenhancementRequest $request)
    {
        return new EditResponse($selfenhancement);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateSelfenhancementRequestNamespace  $request
     * @param  App\Models\Selfenhancement\Selfenhancement  $selfenhancement
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateSelfenhancementRequest $request, Selfenhancement $selfenhancement)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $selfenhancement, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.selfenhancements.index'), ['flash_success' => trans('alerts.backend.selfenhancements.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteSelfenhancementRequestNamespace  $request
     * @param  App\Models\Selfenhancement\Selfenhancement  $selfenhancement
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Selfenhancement $selfenhancement, DeleteSelfenhancementRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($selfenhancement);
        //returning with successfull message
        return new RedirectResponse(route('admin.selfenhancements.index'), ['flash_success' => trans('alerts.backend.selfenhancements.deleted')]);
    }

}
