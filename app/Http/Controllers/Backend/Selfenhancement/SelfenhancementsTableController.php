<?php

namespace App\Http\Controllers\Backend\Selfenhancement;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Selfenhancement\SelfenhancementRepository;
use App\Http\Requests\Backend\Selfenhancement\ManageSelfenhancementRequest;

/**
 * Class SelfenhancementsTableController.
 */
class SelfenhancementsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var SelfenhancementRepository
     */
    protected $selfenhancement;

    /**
     * contructor to initialize repository object
     * @param SelfenhancementRepository $selfenhancement;
     */
    public function __construct(SelfenhancementRepository $selfenhancement)
    {
        $this->selfenhancement = $selfenhancement;
    }

    /**
     * This method return the data of the model
     * @param ManageSelfenhancementRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageSelfenhancementRequest $request)
    {
        return Datatables::of($this->selfenhancement->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($selfenhancement) {
                return Carbon::parse($selfenhancement->created_at)->toDateString();
            })
            ->addColumn('actions', function ($selfenhancement) {
                return $selfenhancement->action_buttons;
            })
            ->make(true);
    }
}
