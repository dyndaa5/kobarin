<?php

namespace App\Http\Controllers\Backend\Homepage;

use App\Models\Homepage\Homepage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Homepage\CreateResponse;
use App\Http\Responses\Backend\Homepage\EditResponse;
use App\Repositories\Backend\Homepage\HomepageRepository;
use App\Http\Requests\Backend\Homepage\ManageHomepageRequest;
use App\Http\Requests\Backend\Homepage\CreateHomepageRequest;
use App\Http\Requests\Backend\Homepage\StoreHomepageRequest;
use App\Http\Requests\Backend\Homepage\EditHomepageRequest;
use App\Http\Requests\Backend\Homepage\UpdateHomepageRequest;
use App\Http\Requests\Backend\Homepage\DeleteHomepageRequest;

/**
 * HomepagesController
 */
class HomepagesController extends Controller
{
    /**
     * variable to store the repository object
     * @var HomepageRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param HomepageRepository $repository;
     */
    public function __construct(HomepageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Homepage\ManageHomepageRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageHomepageRequest $request)
    {
        return new ViewResponse('backend.homepages.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateHomepageRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Homepage\CreateResponse
     */
    public function create(CreateHomepageRequest $request)
    {
        return new CreateResponse('backend.homepages.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreHomepageRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreHomepageRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.homepages.index'), ['flash_success' => trans('alerts.backend.homepages.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Homepage\Homepage  $homepage
     * @param  EditHomepageRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Homepage\EditResponse
     */
    public function edit(Homepage $homepage, EditHomepageRequest $request)
    {
        return new EditResponse($homepage);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateHomepageRequestNamespace  $request
     * @param  App\Models\Homepage\Homepage  $homepage
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateHomepageRequest $request, Homepage $homepage)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $homepage, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.homepages.index'), ['flash_success' => trans('alerts.backend.homepages.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteHomepageRequestNamespace  $request
     * @param  App\Models\Homepage\Homepage  $homepage
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Homepage $homepage, DeleteHomepageRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($homepage);
        //returning with successfull message
        return new RedirectResponse(route('admin.homepages.index'), ['flash_success' => trans('alerts.backend.homepages.deleted')]);
    }

    public function getLists($model)
    {
      switch($model){
        case 'Agenda':
          return \App\Models\Agenda\Agenda::where('status', 'Published')->get();
        case 'SelfDevelopment':
          return \App\Models\Selfdevelopment\Selfdevelopment::where('status', 'Published')->get();
        case 'SelfEnhancement':
          return \App\Models\Selfenhancement\Selfenhancement::where('status', 'Published')->get();
        case 'Video':
          return \App\Models\Video\Video::where('status', 'Published')->get();
      }
    }

}
