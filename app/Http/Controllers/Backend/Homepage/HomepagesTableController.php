<?php

namespace App\Http\Controllers\Backend\Homepage;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Homepage\HomepageRepository;
use App\Http\Requests\Backend\Homepage\ManageHomepageRequest;

/**
 * Class HomepagesTableController.
 */
class HomepagesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var HomepageRepository
     */
    protected $homepage;

    /**
     * contructor to initialize repository object
     * @param HomepageRepository $homepage;
     */
    public function __construct(HomepageRepository $homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * This method return the data of the model
     * @param ManageHomepageRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageHomepageRequest $request)
    {
        return Datatables::of($this->homepage->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($homepage) {
                return Carbon::parse($homepage->created_at)->toDateString();
            })
            ->addColumn('actions', function ($homepage) {
                return $homepage->action_buttons;
            })
            ->make(true);
    }
}
