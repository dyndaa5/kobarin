<?php

namespace App\Http\Controllers\Backend\Video;

use App\Models\Video\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Video\CreateResponse;
use App\Http\Responses\Backend\Video\EditResponse;
use App\Repositories\Backend\Video\VideoRepository;
use App\Http\Requests\Backend\Video\ManageVideoRequest;
use App\Http\Requests\Backend\Video\CreateVideoRequest;
use App\Http\Requests\Backend\Video\StoreVideoRequest;
use App\Http\Requests\Backend\Video\EditVideoRequest;
use App\Http\Requests\Backend\Video\UpdateVideoRequest;
use App\Http\Requests\Backend\Video\DeleteVideoRequest;

/**
 * VideosController
 */
class VideosController extends Controller
{
    /**
     * variable to store the repository object
     * @var VideoRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param VideoRepository $repository;
     */
    public function __construct(VideoRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Video\ManageVideoRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageVideoRequest $request)
    {
        return new ViewResponse('backend.videos.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateVideoRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Video\CreateResponse
     */
    public function create(CreateVideoRequest $request)
    {
        return new CreateResponse('backend.videos.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreVideoRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreVideoRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.videos.index'), ['flash_success' => trans('alerts.backend.videos.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Video\Video  $video
     * @param  EditVideoRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Video\EditResponse
     */
    public function edit(Video $video, EditVideoRequest $request)
    {
        return new EditResponse($video);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateVideoRequestNamespace  $request
     * @param  App\Models\Video\Video  $video
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateVideoRequest $request, Video $video)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $video, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.videos.index'), ['flash_success' => trans('alerts.backend.videos.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteVideoRequestNamespace  $request
     * @param  App\Models\Video\Video  $video
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Video $video, DeleteVideoRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($video);
        //returning with successfull message
        return new RedirectResponse(route('admin.videos.index'), ['flash_success' => trans('alerts.backend.videos.deleted')]);
    }
    
}
