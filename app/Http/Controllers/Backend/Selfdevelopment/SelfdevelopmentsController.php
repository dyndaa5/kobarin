<?php

namespace App\Http\Controllers\Backend\Selfdevelopment;

use App\Models\Selfdevelopment\Selfdevelopment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Selfdevelopment\CreateResponse;
use App\Http\Responses\Backend\Selfdevelopment\EditResponse;
use App\Repositories\Backend\Selfdevelopment\SelfdevelopmentRepository;
use App\Http\Requests\Backend\Selfdevelopment\ManageSelfdevelopmentRequest;
use App\Http\Requests\Backend\Selfdevelopment\CreateSelfdevelopmentRequest;
use App\Http\Requests\Backend\Selfdevelopment\StoreSelfdevelopmentRequest;
use App\Http\Requests\Backend\Selfdevelopment\EditSelfdevelopmentRequest;
use App\Http\Requests\Backend\Selfdevelopment\UpdateSelfdevelopmentRequest;
use App\Http\Requests\Backend\Selfdevelopment\DeleteSelfdevelopmentRequest;
use App\Models\Category\Category;
use Carbon\Carbon;

/**
 * SelfdevelopmentsController
 */
class SelfdevelopmentsController extends Controller
{
    /**
     * Blog Status.
     */
    protected $status = [
        'Published' => 'Published',
        'Draft'     => 'Draft',
        'InActive'  => 'InActive',
        'Scheduled' => 'Scheduled',
    ];

    /**
     * variable to store the repository object
     * @var SelfdevelopmentRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param SelfdevelopmentRepository $repository;
     */
    public function __construct(SelfdevelopmentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Selfdevelopment\ManageSelfdevelopmentRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageSelfdevelopmentRequest $request)
    {
        return new ViewResponse('backend.selfdevelopments.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateSelfdevelopmentRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Selfdevelopment\CreateResponse
     */
    public function create(CreateSelfdevelopmentRequest $request)
    {
      $blogCategories = Category::getSelectData();
      return new CreateResponse($this->status, $blogCategories);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreSelfdevelopmentRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreSelfdevelopmentRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.selfdevelopments.index'), ['flash_success' => trans('alerts.backend.selfdevelopments.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Selfdevelopment\Selfdevelopment  $selfdevelopment
     * @param  EditSelfdevelopmentRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Selfdevelopment\EditResponse
     */
    public function edit(Selfdevelopment $selfdevelopment, EditSelfdevelopmentRequest $request)
    {
        return new EditResponse($selfdevelopment);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateSelfdevelopmentRequestNamespace  $request
     * @param  App\Models\Selfdevelopment\Selfdevelopment  $selfdevelopment
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateSelfdevelopmentRequest $request, Selfdevelopment $selfdevelopment)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        $input['slug'] = str_slug($input['name']);
        $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
        //Update the model using repository update method
        $this->repository->update( $selfdevelopment, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.selfdevelopments.index'), ['flash_success' => trans('alerts.backend.selfdevelopments.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteSelfdevelopmentRequestNamespace  $request
     * @param  App\Models\Selfdevelopment\Selfdevelopment  $selfdevelopment
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Selfdevelopment $selfdevelopment, DeleteSelfdevelopmentRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($selfdevelopment);
        //returning with successfull message
        return new RedirectResponse(route('admin.selfdevelopments.index'), ['flash_success' => trans('alerts.backend.selfdevelopments.deleted')]);
    }

}
