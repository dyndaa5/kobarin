<?php

namespace App\Http\Controllers\Backend\Selfdevelopment;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Selfdevelopment\SelfdevelopmentRepository;
use App\Http\Requests\Backend\Selfdevelopment\ManageSelfdevelopmentRequest;

/**
 * Class SelfdevelopmentsTableController.
 */
class SelfdevelopmentsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var SelfdevelopmentRepository
     */
    protected $selfdevelopment;

    /**
     * contructor to initialize repository object
     * @param SelfdevelopmentRepository $selfdevelopment;
     */
    public function __construct(SelfdevelopmentRepository $selfdevelopment)
    {
        $this->selfdevelopment = $selfdevelopment;
    }

    /**
     * This method return the data of the model
     * @param ManageSelfdevelopmentRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageSelfdevelopmentRequest $request)
    {
        return Datatables::of($this->selfdevelopment->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($selfdevelopment) {
                return Carbon::parse($selfdevelopment->created_at)->toDateString();
            })
            ->addColumn('actions', function ($selfdevelopment) {
                return $selfdevelopment->action_buttons;
            })
            ->make(true);
    }
}
