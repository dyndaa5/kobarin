<?php

namespace App\Http\Controllers\Backend\Newsletter;

use App\Models\Newsletter\Newsletter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Newsletter\CreateResponse;
use App\Http\Responses\Backend\Newsletter\EditResponse;
use App\Repositories\Backend\Newsletter\NewsletterRepository;
use App\Http\Requests\Backend\Newsletter\ManageNewsletterRequest;
use App\Http\Requests\Backend\Newsletter\CreateNewsletterRequest;
use App\Http\Requests\Backend\Newsletter\StoreNewsletterRequest;
use App\Http\Requests\Backend\Newsletter\EditNewsletterRequest;
use App\Http\Requests\Backend\Newsletter\UpdateNewsletterRequest;
use App\Http\Requests\Backend\Newsletter\DeleteNewsletterRequest;

/**
 * NewslettersController
 */
class NewslettersController extends Controller
{
    /**
     * variable to store the repository object
     * @var NewsletterRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param NewsletterRepository $repository;
     */
    public function __construct(NewsletterRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Newsletter\ManageNewsletterRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageNewsletterRequest $request)
    {
        return new ViewResponse('backend.newsletters.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateNewsletterRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Newsletter\CreateResponse
     */
    public function create(CreateNewsletterRequest $request)
    {
        return new CreateResponse('backend.newsletters.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreNewsletterRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreNewsletterRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.newsletters.index'), ['flash_success' => trans('alerts.backend.newsletters.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Newsletter\Newsletter  $newsletter
     * @param  EditNewsletterRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Newsletter\EditResponse
     */
    public function edit(Newsletter $newsletter, EditNewsletterRequest $request)
    {
        return new EditResponse($newsletter);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNewsletterRequestNamespace  $request
     * @param  App\Models\Newsletter\Newsletter  $newsletter
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateNewsletterRequest $request, Newsletter $newsletter)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $newsletter, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.newsletters.index'), ['flash_success' => trans('alerts.backend.newsletters.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteNewsletterRequestNamespace  $request
     * @param  App\Models\Newsletter\Newsletter  $newsletter
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Newsletter $newsletter, DeleteNewsletterRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($newsletter);
        //returning with successfull message
        return new RedirectResponse(route('admin.newsletters.index'), ['flash_success' => trans('alerts.backend.newsletters.deleted')]);
    }
    
}
