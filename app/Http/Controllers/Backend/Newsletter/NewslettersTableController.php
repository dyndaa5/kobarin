<?php

namespace App\Http\Controllers\Backend\Newsletter;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Newsletter\NewsletterRepository;
use App\Http\Requests\Backend\Newsletter\ManageNewsletterRequest;

/**
 * Class NewslettersTableController.
 */
class NewslettersTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var NewsletterRepository
     */
    protected $newsletter;

    /**
     * contructor to initialize repository object
     * @param NewsletterRepository $newsletter;
     */
    public function __construct(NewsletterRepository $newsletter)
    {
        $this->newsletter = $newsletter;
    }

    /**
     * This method return the data of the model
     * @param ManageNewsletterRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageNewsletterRequest $request)
    {
        return Datatables::of($this->newsletter->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($newsletter) {
                return Carbon::parse($newsletter->created_at)->toDateString();
            })
            ->addColumn('actions', function ($newsletter) {
                return $newsletter->action_buttons;
            })
            ->make(true);
    }
}
