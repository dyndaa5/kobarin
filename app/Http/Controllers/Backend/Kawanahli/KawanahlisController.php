<?php

namespace App\Http\Controllers\Backend\Kawanahli;

use App\Models\Kawanahli\Kawanahli;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Kawanahli\CreateResponse;
use App\Http\Responses\Backend\Kawanahli\EditResponse;
use App\Repositories\Backend\Kawanahli\KawanahliRepository;
use App\Http\Requests\Backend\Kawanahli\ManageKawanahliRequest;
use App\Http\Requests\Backend\Kawanahli\CreateKawanahliRequest;
use App\Http\Requests\Backend\Kawanahli\StoreKawanahliRequest;
use App\Http\Requests\Backend\Kawanahli\EditKawanahliRequest;
use App\Http\Requests\Backend\Kawanahli\UpdateKawanahliRequest;
use App\Http\Requests\Backend\Kawanahli\DeleteKawanahliRequest;

/**
 * KawanahlisController
 */
class KawanahlisController extends Controller
{
    /**
     * variable to store the repository object
     * @var KawanahliRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param KawanahliRepository $repository;
     */
    public function __construct(KawanahliRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Kawanahli\ManageKawanahliRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageKawanahliRequest $request)
    {
        return new ViewResponse('backend.kawanahlis.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateKawanahliRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Kawanahli\CreateResponse
     */
    public function create(CreateKawanahliRequest $request)
    {
        return new CreateResponse('backend.kawanahlis.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreKawanahliRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreKawanahliRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.kawanahlis.index'), ['flash_success' => trans('alerts.backend.kawanahlis.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Kawanahli\Kawanahli  $kawanahli
     * @param  EditKawanahliRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Kawanahli\EditResponse
     */
    public function edit(Kawanahli $kawanahli, EditKawanahliRequest $request)
    {
        return new EditResponse($kawanahli);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateKawanahliRequestNamespace  $request
     * @param  App\Models\Kawanahli\Kawanahli  $kawanahli
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateKawanahliRequest $request, Kawanahli $kawanahli)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $kawanahli, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.kawanahlis.index'), ['flash_success' => trans('alerts.backend.kawanahlis.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteKawanahliRequestNamespace  $request
     * @param  App\Models\Kawanahli\Kawanahli  $kawanahli
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Kawanahli $kawanahli, DeleteKawanahliRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($kawanahli);
        //returning with successfull message
        return new RedirectResponse(route('admin.kawanahlis.index'), ['flash_success' => trans('alerts.backend.kawanahlis.deleted')]);
    }
    
}
