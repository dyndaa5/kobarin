<?php

namespace App\Http\Controllers\Backend\Kawanahli;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Kawanahli\KawanahliRepository;
use App\Http\Requests\Backend\Kawanahli\ManageKawanahliRequest;

/**
 * Class KawanahlisTableController.
 */
class KawanahlisTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var KawanahliRepository
     */
    protected $kawanahli;

    /**
     * contructor to initialize repository object
     * @param KawanahliRepository $kawanahli;
     */
    public function __construct(KawanahliRepository $kawanahli)
    {
        $this->kawanahli = $kawanahli;
    }

    /**
     * This method return the data of the model
     * @param ManageKawanahliRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageKawanahliRequest $request)
    {
        return Datatables::of($this->kawanahli->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($kawanahli) {
                return Carbon::parse($kawanahli->created_at)->toDateString();
            })
            ->addColumn('actions', function ($kawanahli) {
                return $kawanahli->action_buttons;
            })
            ->make(true);
    }
}
