<?php

namespace App\Http\Controllers\Backend\Agenda;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Agenda\AgendaRepository;
use App\Http\Requests\Backend\Agenda\ManageAgendaRequest;

/**
 * Class AgendasTableController.
 */
class AgendasTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var AgendaRepository
     */
    protected $agenda;

    /**
     * contructor to initialize repository object
     * @param AgendaRepository $agenda;
     */
    public function __construct(AgendaRepository $agenda)
    {
        $this->agenda = $agenda;
    }

    /**
     * This method return the data of the model
     * @param ManageAgendaRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageAgendaRequest $request)
    {
        return Datatables::of($this->agenda->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($agenda) {
                return Carbon::parse($agenda->created_at)->toDateString();
            })
            ->addColumn('actions', function ($agenda) {
                return $agenda->action_buttons;
            })
            ->make(true);
    }
}
