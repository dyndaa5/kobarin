<?php

namespace App\Http\Controllers\Backend\Agenda;

use App\Models\Agenda\Agenda;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Agenda\CreateResponse;
use App\Http\Responses\Backend\Agenda\EditResponse;
use App\Repositories\Backend\Agenda\AgendaRepository;
use App\Http\Requests\Backend\Agenda\ManageAgendaRequest;
use App\Http\Requests\Backend\Agenda\CreateAgendaRequest;
use App\Http\Requests\Backend\Agenda\StoreAgendaRequest;
use App\Http\Requests\Backend\Agenda\EditAgendaRequest;
use App\Http\Requests\Backend\Agenda\UpdateAgendaRequest;
use App\Http\Requests\Backend\Agenda\DeleteAgendaRequest;

/**
 * AgendasController
 */
class AgendasController extends Controller
{
    /**
     * variable to store the repository object
     * @var AgendaRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param AgendaRepository $repository;
     */
    public function __construct(AgendaRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Agenda\ManageAgendaRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageAgendaRequest $request)
    {
        return new ViewResponse('backend.agendas.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateAgendaRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Agenda\CreateResponse
     */
    public function create(CreateAgendaRequest $request)
    {
        return new CreateResponse('backend.agendas.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreAgendaRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreAgendaRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.agendas.index'), ['flash_success' => trans('alerts.backend.agendas.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Agenda\Agenda  $agenda
     * @param  EditAgendaRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Agenda\EditResponse
     */
    public function edit(Agenda $agenda, EditAgendaRequest $request)
    {
        return new EditResponse($agenda);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateAgendaRequestNamespace  $request
     * @param  App\Models\Agenda\Agenda  $agenda
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateAgendaRequest $request, Agenda $agenda)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $agenda, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.agendas.index'), ['flash_success' => trans('alerts.backend.agendas.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteAgendaRequestNamespace  $request
     * @param  App\Models\Agenda\Agenda  $agenda
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Agenda $agenda, DeleteAgendaRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($agenda);
        //returning with successfull message
        return new RedirectResponse(route('admin.agendas.index'), ['flash_success' => trans('alerts.backend.agendas.deleted')]);
    }
    
}
