<?php

namespace App\Http\Controllers\Backend\Network;

use App\Models\Network\Network;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Network\CreateResponse;
use App\Http\Responses\Backend\Network\EditResponse;
use App\Repositories\Backend\Network\NetworkRepository;
use App\Http\Requests\Backend\Network\ManageNetworkRequest;
use App\Http\Requests\Backend\Network\CreateNetworkRequest;
use App\Http\Requests\Backend\Network\StoreNetworkRequest;
use App\Http\Requests\Backend\Network\EditNetworkRequest;
use App\Http\Requests\Backend\Network\UpdateNetworkRequest;
use App\Http\Requests\Backend\Network\DeleteNetworkRequest;

/**
 * NetworksController
 */
class NetworksController extends Controller
{
    /**
     * variable to store the repository object
     * @var NetworkRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param NetworkRepository $repository;
     */
    public function __construct(NetworkRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Network\ManageNetworkRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageNetworkRequest $request)
    {
        return new ViewResponse('backend.networks.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateNetworkRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Network\CreateResponse
     */
    public function create(CreateNetworkRequest $request)
    {
        return new CreateResponse('backend.networks.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreNetworkRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreNetworkRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.networks.index'), ['flash_success' => trans('alerts.backend.networks.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Network\Network  $network
     * @param  EditNetworkRequestNamespace  $request
     * @return \App\Http\Responses\Backend\Network\EditResponse
     */
    public function edit(Network $network, EditNetworkRequest $request)
    {
        return new EditResponse($network);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNetworkRequestNamespace  $request
     * @param  App\Models\Network\Network  $network
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateNetworkRequest $request, Network $network)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $network, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.networks.index'), ['flash_success' => trans('alerts.backend.networks.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteNetworkRequestNamespace  $request
     * @param  App\Models\Network\Network  $network
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Network $network, DeleteNetworkRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($network);
        //returning with successfull message
        return new RedirectResponse(route('admin.networks.index'), ['flash_success' => trans('alerts.backend.networks.deleted')]);
    }
    
}
