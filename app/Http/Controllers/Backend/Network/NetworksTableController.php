<?php

namespace App\Http\Controllers\Backend\Network;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Network\NetworkRepository;
use App\Http\Requests\Backend\Network\ManageNetworkRequest;

/**
 * Class NetworksTableController.
 */
class NetworksTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var NetworkRepository
     */
    protected $network;

    /**
     * contructor to initialize repository object
     * @param NetworkRepository $network;
     */
    public function __construct(NetworkRepository $network)
    {
        $this->network = $network;
    }

    /**
     * This method return the data of the model
     * @param ManageNetworkRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageNetworkRequest $request)
    {
        return Datatables::of($this->network->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($network) {
                return Carbon::parse($network->created_at)->toDateString();
            })
            ->addColumn('actions', function ($network) {
                return $network->action_buttons;
            })
            ->make(true);
    }
}
