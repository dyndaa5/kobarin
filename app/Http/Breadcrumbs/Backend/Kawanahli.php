<?php

Breadcrumbs::register('admin.kawanahlis.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.kawanahlis.management'), route('admin.kawanahlis.index'));
});

Breadcrumbs::register('admin.kawanahlis.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.kawanahlis.index');
    $breadcrumbs->push(trans('menus.backend.kawanahlis.create'), route('admin.kawanahlis.create'));
});

Breadcrumbs::register('admin.kawanahlis.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.kawanahlis.index');
    $breadcrumbs->push(trans('menus.backend.kawanahlis.edit'), route('admin.kawanahlis.edit', $id));
});
