<?php

Breadcrumbs::register('admin.agendas.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.agendas.management'), route('admin.agendas.index'));
});

Breadcrumbs::register('admin.agendas.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.agendas.index');
    $breadcrumbs->push(trans('menus.backend.agendas.create'), route('admin.agendas.create'));
});

Breadcrumbs::register('admin.agendas.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.agendas.index');
    $breadcrumbs->push(trans('menus.backend.agendas.edit'), route('admin.agendas.edit', $id));
});
