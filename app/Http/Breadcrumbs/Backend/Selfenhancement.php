<?php

Breadcrumbs::register('admin.selfenhancements.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.selfenhancements.management'), route('admin.selfenhancements.index'));
});

Breadcrumbs::register('admin.selfenhancements.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.selfenhancements.index');
    $breadcrumbs->push(trans('menus.backend.selfenhancements.create'), route('admin.selfenhancements.create'));
});

Breadcrumbs::register('admin.selfenhancements.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.selfenhancements.index');
    $breadcrumbs->push(trans('menus.backend.selfenhancements.edit'), route('admin.selfenhancements.edit', $id));
});
