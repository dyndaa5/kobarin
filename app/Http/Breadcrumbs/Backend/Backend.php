<?php

Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
});

require __DIR__.'/Search.php';
require __DIR__.'/Access/User.php';
require __DIR__.'/Access/Role.php';
require __DIR__.'/Access/Permission.php';
require __DIR__.'/Page.php';
require __DIR__.'/Setting.php';
require __DIR__.'/Blog_Category.php';
require __DIR__.'/Blog_Tag.php';
require __DIR__.'/Blog_Management.php';
require __DIR__.'/Faqs.php';
require __DIR__.'/Menu.php';
require __DIR__.'/LogViewer.php';

require __DIR__.'/Selfdevelopment.php';
require __DIR__.'/Selfenhancement.php';
require __DIR__.'/Kawanahli.php';
require __DIR__.'/Agenda.php';
require __DIR__.'/Banner.php';
require __DIR__.'/Network.php';
require __DIR__.'/Category.php';
require __DIR__.'/Newsletter.php';
require __DIR__.'/Video.php';
require __DIR__.'/Homepage.php';