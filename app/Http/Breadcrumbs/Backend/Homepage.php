<?php

Breadcrumbs::register('admin.homepages.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.homepages.management'), route('admin.homepages.index'));
});

Breadcrumbs::register('admin.homepages.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.homepages.index');
    $breadcrumbs->push(trans('menus.backend.homepages.create'), route('admin.homepages.create'));
});

Breadcrumbs::register('admin.homepages.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.homepages.index');
    $breadcrumbs->push(trans('menus.backend.homepages.edit'), route('admin.homepages.edit', $id));
});
