<?php

Breadcrumbs::register('admin.networks.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.networks.management'), route('admin.networks.index'));
});

Breadcrumbs::register('admin.networks.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.networks.index');
    $breadcrumbs->push(trans('menus.backend.networks.create'), route('admin.networks.create'));
});

Breadcrumbs::register('admin.networks.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.networks.index');
    $breadcrumbs->push(trans('menus.backend.networks.edit'), route('admin.networks.edit', $id));
});
