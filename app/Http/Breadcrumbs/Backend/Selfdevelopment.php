<?php

Breadcrumbs::register('admin.selfdevelopments.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.selfdevelopments.management'), route('admin.selfdevelopments.index'));
});

Breadcrumbs::register('admin.selfdevelopments.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.selfdevelopments.index');
    $breadcrumbs->push(trans('menus.backend.selfdevelopments.create'), route('admin.selfdevelopments.create'));
});

Breadcrumbs::register('admin.selfdevelopments.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.selfdevelopments.index');
    $breadcrumbs->push(trans('menus.backend.selfdevelopments.edit'), route('admin.selfdevelopments.edit', $id));
});
