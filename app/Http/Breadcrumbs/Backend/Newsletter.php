<?php

Breadcrumbs::register('admin.newsletters.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.newsletters.management'), route('admin.newsletters.index'));
});

Breadcrumbs::register('admin.newsletters.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.newsletters.index');
    $breadcrumbs->push(trans('menus.backend.newsletters.create'), route('admin.newsletters.create'));
});

Breadcrumbs::register('admin.newsletters.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.newsletters.index');
    $breadcrumbs->push(trans('menus.backend.newsletters.edit'), route('admin.newsletters.edit', $id));
});
