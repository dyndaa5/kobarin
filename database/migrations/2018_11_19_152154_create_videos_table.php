<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name', 191);
          $table->dateTime('publish_datetime');
          $table->string('featured_image', 191);
          $table->text('content');
          $table->string('meta_title', 191)->nullable();
          $table->string('cannonical_link', 191)->nullable();
          $table->string('slug', 191)->nullable();
          $table->text('meta_description', 65535)->nullable();
          $table->text('meta_keywords', 65535)->nullable();
          $table->enum('status', ['Published', 'Draft', 'InActive', 'Scheduled']);
          $table->integer('created_by')->unsigned();
          $table->integer('updated_by')->unsigned()->nullable();
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
