<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKawanahlisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kawanahlis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index('kawan_ahli_user_id_foreign');
            $table->integer('user_type')->default(0);
            $table->string('institusi')->nullable();
            $table->string('researchgate')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('twitter')->nullable();
            $table->string('facebook')->nullable();
            $table->string('website')->nullable();
            $table->string('youtube')->nullable();
            $table->string('lisensi_professional')->nullable();
            $table->string('about')->nullable();
            $table->string('featured_image', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kawanahlis');
    }
}
